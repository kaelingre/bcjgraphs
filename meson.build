project(
	'gGraph',
	'cpp',
	version : '1.0',
	default_options : ['warning_level=3', 'cpp_std=c++17'])

conf_data = configuration_data()

# check for libraries
permGroup_dep = dependency('PermGroup', version : '>=0.5', method : 'cmake',
							modules : ['PermGroup::perm_group', 'PermGroup::graph_canon'])
graphCanon_dep = dependency('GraphCanon', version : '>=0.5', method : 'cmake',
							modules : ['GraphCanon::perm_group', 'GraphCanon::graph_canon'])
boost_dep = dependency('boost', version : '>=1.64')
openmp_dep = dependency('openmp', required : false)

incdirs = []
incdirs += include_directories('src/cereal/include/')

# check for mathematica
if get_option('mma')
	if host_machine.system() == 'linux'
		mma_cmd = find_program('math', required : false)
	elif host_machine.system() == 'darwin'
		mma_cmd = find_program('MathKernel', required : false, dirs : ['/Applications/Mathematica.app/Contents/MacOS/'])
	else
		error('Your operating system is not supported, sorry.')
	endif
	
	if mma_cmd.found()
		conf_data.set('GGRAPH_USE_MMA', true)
		incDirMMA = run_command(mma_cmd, '-script', 'includeDir.m')
		sysID = run_command(mma_cmd, '-script', 'systemID.m')

		if incDirMMA.returncode() != 0
			error('Mathematica could not be run properly.')
		endif
		if sysID.returncode() != 0
			error('Mathematica could not be run properly.')
		endif
		incdirs += include_directories(incDirMMA.stdout().strip())
	else
		conf_data.set('GGRAPH_USE_MMA', false)
	endif
else
	conf_data.set('GGRAPH_USE_MMA', false)
endif

configure_file(output : 'ggraph_config.h',
			   configuration : conf_data,
			   install : true,
			   install_dir : 'include/ggraph')

incdirs += include_directories('.')

subdir('src')
