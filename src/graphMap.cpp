#include "graphMap.hpp"

#include "graph.hpp"
#include "util.hpp"

namespace ggraph {
	graphMap::graphMap(const std::size_t& nVerts_, const std::size_t& nEdges_)
		: m_sign(false) {
		m_verts.reserve(nVerts_);
		for (size_t i = 0; i != nVerts_; ++i) {
			m_verts.push_back(i);
		}
		m_edges.reserve(nEdges_);
		for (size_t i = 0; i != nEdges_; ++i) {
			m_edges.push_back(std::make_pair(i, false));
		}
	}
	void graphMap::invert() {
		//invert vertex map
		std::vector<std::size_t> newVerts(m_verts.size());
		auto size = m_verts.size();
		for (std::size_t i = 0; i != size; ++i) {
			newVerts[m_verts[i]] = i;
		}
		m_verts = std::move(newVerts);

		//invert edge map
		std::vector<std::pair<std::size_t, bool> > newEdges(m_edges.size());
		size = m_edges.size();
		for (std::size_t i = 0; i != size; ++i) {
			newEdges[m_edges[i].first] = std::make_pair(i, m_edges[i].second);
		}
		m_edges = std::move(newEdges);
	}

	void graphMap::invertCare() {
		//invert vertex map
		std::vector<std::size_t> newVerts(
			m_verts.size() - std::count(m_verts.begin(), m_verts.end(), -1));
		auto size = m_verts.size();
		for (std::size_t i = 0; i != size; ++i) {
			auto& mapped = m_verts[i];
			if (mapped 	!= -1) {
				newVerts[mapped] = i;
			}
		}
		m_verts = std::move(newVerts);

		//invert edge map
		std::vector<std::pair<std::size_t, bool> > newEdges(
			std::count_if(m_edges.begin(), m_edges.end(),
						  [] (const std::pair<std::size_t, bool>& e_) {
							  return e_.first != -1;
						  }));
		size = m_edges.size();
		for (std::size_t i = 0; i != size; ++i) {
			auto& mapped = m_edges[i];
			if (mapped.first != -1) {
				newEdges[mapped.first] = std::make_pair(i, mapped.second);
			}
		}
		m_edges = std::move(newEdges);
	}

	std::ostream& operator<<(std::ostream& ostream_, const graphMap& map_) {
		ostream_ << "vertex map: " << std::endl;
		std::size_t counter = 1;
		for (auto& v : map_.m_verts) {
			ostream_ << "  " << counter++ << "->" << v + 1 << std::endl;
		}

		ostream_ << "edge map: " << std::endl;
		counter = 1;
		for (auto& e : map_.m_edges) {
			ostream_ << "  " << counter++ << "->" << e.first + 1
					 << '(' << e.second << ')' << std::endl;
		}
		ostream_ << "sign: " << map_.sign() << std::endl;

		return ostream_;
	}

	bool graphMap::computeSign(const graph& from_, const graph& to_) {
		bool ret = false;
		const std::size_t size = m_verts.size();
		for (std::size_t i = 0 ; i != size; ++i) {
			const auto& otherV = *to_[m_verts[i]];
			if (otherV.size() == 1) {
				continue;
			}

			//make a permutation vector for the legs of i'th vertex
			std::vector<std::size_t> perm;
			for (auto& e : *from_[i]) {
				//find edge corresponding to e in otherV
				const auto otherEID = m_edges[e->m_ID].first;
				const std::size_t size2 = otherV.size();
#ifndef NDEBUG
				bool found = false;
#endif
				for (std::size_t j = 0; j != size2; ++j) {
					//we should not yet have added the other edge and they
					//should be of the same type
					if (otherV[j]->m_ID == otherEID
						&& std::find(perm.begin(), perm.end(), j) == perm.end()
						&& graph::edge_t::compare(*e, *otherV[j])==0) {
						perm.push_back(j);
#ifndef NDEBUG
						found = true;
#endif 
						break;
					}
				}
#ifndef NDEBUG
				assert(found);
#endif
			}
			if (parity(perm)) {
				ret = !ret;
			}
		}
		m_sign = ret;
		return ret;
	}

	void graphMap::concat(const graphMap& map_) {
		//map verts
		for (auto& i : m_verts) {
			if (i != -1) {
				assert(map_.m_verts.size() > i);
				i = map_.m_verts[i];
			}
		}

		//map edges
		for (auto& i : m_edges) {
			if (i.first != -1) {
				assert(map_.m_edges.size() > i.first);
				auto lookup = map_.m_edges[i.first];
				lookup.second = (lookup.second != i.second);
				i = std::move(lookup);
			}
		}
		//sign
		m_sign = (m_sign != map_.m_sign);
	}
}
