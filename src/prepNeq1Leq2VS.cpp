#include "ggraph_config.h"

#include <iostream>
#include <fstream>

#include "graphLabelling.hpp"
#include "cut.hpp"
#include "eqns.hpp"

using namespace ggraph;


int main(int argc, char* argv[]) {
	std::vector<flExt> exts;
	auto gluon =std::make_pair(ext_t::GLUON,-1);
	exts.push_back(gluon);
	exts.push_back(gluon);
	exts.push_back(gluon);
	exts.push_back(gluon);

	std::fstream fs("/tmp/test.m", std::fstream::out);

	labelledGraphs gsNeq2(exts, 2, {coupling::THREEGLUON, coupling::QUARKGLUON});
	labelledGraphs gs(exts, 2, {coupling::THREEGLUON, coupling::SCALARGLUON,
								coupling::SCALARGLUON1, coupling::SCALARGLUON2,
								coupling::THREESCALAR});

	auto es = eqns::makeFlavorSymEqns(gs, {0,1,2},"n", true);
	//es.add(eqns::makeScalarTwoTermEqns(gs));
	//es.add(eqns::makeScalarTwoTermFlEqns(gs, {0,1,2}));
	es.add(eqns::makeJacobiEqns(gs));

	std::vector<const graph*> knowns;
	for (auto& g : gsNeq2) {
		es.push_back(eqns::makeNeq2DecompIDS(g, gsNeq2, gs));
		knowns.push_back(&g.getGraph());
	}

	std::vector<const graph*> masters;
	masters.push_back(&gs[294].getGraph());
	masters.push_back(&gs[295].getGraph());
	/*
	masters.push_back(&gs[27].getGraph());
	masters.push_back(&gs[389].getGraph());
	masters.push_back(&gs[32].getGraph());
	*/
	/*
	masters.push_back(&gs[94].getGraph());
	masters.push_back(&gs[24].getGraph());
	masters.push_back(&gs[601].getGraph());
	masters.push_back(&gs[37].getGraph());
	masters.push_back(&gs[301].getGraph());
	masters.push_back(&gs[14].getGraph());
	masters.push_back(&gs[25].getGraph());
	masters.push_back(&gs[29].getGraph());
	*/

	auto sol = es.reduce(masters, knowns, {0,1,2});
	replaceRepeated(sol);

	fs << "solFunctional=" << sol << ';' << std::endl
	   << "consistencyEqns=" << es << ';' << std::endl;

	return 0;
}
