#ifndef COUPLINGSRUNNER_HPP
#define COUPLINGSRUNNER_HPP

#include "ggraph_config.h"
#include "utility/runner.hpp"
#include "utility/rangeRunner.hpp"
#include "tuplesRunner.hpp"
#include "graphs.hpp"

namespace ggraph {
	/**
	 * a runner that runs over all possible combinations of couplings picked from
	 * given set of couplings that can appear in a graph with nLoops and nExts.
	 * the current choice is represented by a vector that maps the k'th coupling
	 * to the number of times it appears in the current combination
	 * %%%%%%
	 * IMPORTANT: the couplings need to be sorted by size! (smallest first)
	 * %%%%%%
	 * idea: use a range runner to find solutions to the equation:
	 *  2L + N == 2 + Sum_{internal verts v} (|v| - 2)
	 * for given number of loops L and number of exts N (|v| = size of vertex).
	 * then use tupleRunners to choose corresponding couplings of given sizes
	 *
	 * if exactNV is set (!=0) then only configurations with exactly that number of
	 * vertices (couplings) are considered. Can be used to include configurations
	 * with 1-pt internal vertices without producing infinitely many vertices
	 */
	class couplingsRunner : public runner<std::vector<std::size_t> > {
	public:
		//**************************************************
		// constructor
		//**************************************************
		couplingsRunner(const std::vector<coupling>& couplings_,
						const std::size_t& nLoops_, const std::size_t& nExts_,
						const std::size_t& exactNV_ = 0);

		//**************************************************
		// methods
		//**************************************************
		bool next() override;

		const std::vector<std::size_t>& getCurrent() const override {
			return m_current;
		}

	private:
		//**************************************************
		// members
		//**************************************************
		std::vector<std::size_t> m_current;
		//couplings represented by a map from sizeOfCoupling
		//to (number of couplings with that size)
		std::map<std::size_t, std::size_t> m_couplings;
		const std::size_t m_sum;
		rangeRunner<std::size_t> m_rR;
		std::vector<tuplesRunner> m_tuplesRunners;
		const std::size_t m_exactNV;

		//**************************************************
		// methods
		//**************************************************
		/**
		 * goes to next solution of equation, resets tuplesRunners and reconstructs
		 * current in one go
		 */
		bool nextRangeRunner();

		/**
		 * goes to next picked tuples and reconstructs current
		 */
		bool nextTuplesRunner();
		
		void resetTuplesRunners();
		void constructCurrent();
	};
}


#endif /*COUPLINGSRUNNER_HPP*/
