#ifndef GKA_DUALGRAPHS_HPP
#define GKA_DUALGRAPHS_HPP

#include "ggraph_config.h"

#include "dualGraph.hpp"
#include "graph.hpp"

namespace ggraph {
	class dualGraphs : public std::set<dualGraph> {
	public:
		//**************************************************
		// constructors
		//**************************************************
		/**
		 * constructs all dual graphs that the given graph is a part of
		 */
		dualGraphs(const graph& g_);
		dualGraphs(graph&& g_);

	private:
		//**************************************************
		// methods
		//**************************************************
		//construction helper (construct dualGraph for a fixed permutation
		//of legs at each vertex)
		void construct(const graph& g_);

		//helper of construct
		//given an edge and a side (side=true->left, side=false->right),
		//determine the "region", i.e. planarely collect all edges and sides
		static std::vector<graph::edge>
		findRegion(const graph::edge& e_, const bool& side_,
				   std::map<std::size_t, std::pair<bool, bool> >& visited_);
	};
}

#endif /*GKA_DUALGRAPHS_HPP*/
