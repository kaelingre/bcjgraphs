#include "dualGraphs.hpp"

#include "utility/permItRunner.hpp"

namespace ggraph {
	dualGraphs::dualGraphs(const graph& g_) {
		//go through all (cyclically-inequivalent) permutations of all vertices
		//TODO: can we skip the first vertex?

		//make a copy
		auto g = g_;

		std::vector<permItRunner<std::vector<graph::edge>::iterator> >
			runners;
		runners.reserve(g.nVerts());

		for (auto& v : g.getVerts()) {
			if (v->size() > 1) {
				runners.push_back(permItRunner(std::make_pair(v->begin() + 1, v->end())));
			}
		}

		do {
			construct(g);
			//step
			bool found = false;
			for (auto it = runners.begin(), end = runners.end(); it != end; ++it) {
				if (it->next()) {
					found = true;
					break;
				} else {
					it->reset();
				}
			}
			if (!found) {
				//end of recursion
				return;
			}
		} while (true);
	}

	dualGraphs::dualGraphs(graph&& g_) {
		//go through all (cyclically-inequivalent) permutations of all vertices
		//TODO: can we skip the first vertex?

		std::vector<permItRunner<std::vector<graph::edge>::iterator> >
			runners;
		runners.reserve(g_.nVerts());

		for (auto& v : g_.getVerts()) {
			if (v->size() > 1) {
				runners.push_back(permItRunner(std::make_pair(v->begin() + 1, v->end())));
			}
		}

		do {
			construct(g_);
			//step
			bool found = true;
			for (auto it = runners.begin(), end = runners.end(); it != end; ++it) {
				if (it->next()) {
					found = true;
					break;
				} else {
					it->reset();
				}
			}
			if (!found) {
				//end of recursion
				return;
			}
		} while (true);
	}

	void dualGraphs::construct(const graph& g_) {
		//TODO again
	}

	std::vector<graph::edge>
	dualGraphs::findRegion(const graph::edge& e_, const bool& side_,
						   std::map<std::size_t, std::pair<bool, bool> >& visited_) {
		std::vector<graph::edge> ret;

		graph::edge currentEdge = e_;
		currentEdge.m_dir = side_;

		do {
			//add to return vector
			ret.push_back(currentEdge);
			if (currentEdge.m_dir) {
				//add to visited
				visited_[currentEdge->m_ID].first = true;
				//get next edge
				auto nextV = currentEdge->second;
				currentEdge = nextV->next(currentEdge);
				currentEdge.m_dir = (currentEdge->first == nextV);
			} else {
				//add to visited
				visited_[currentEdge->m_ID].second = true;
				//get next edge
				auto nextV = currentEdge->first;
				currentEdge = nextV->next(currentEdge);
				currentEdge.m_dir = (currentEdge->first == nextV);
			}
		} while (!(currentEdge == e_ && currentEdge.m_dir == e_.m_dir));

		return ret;
	}
}
