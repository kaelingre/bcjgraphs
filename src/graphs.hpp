#ifndef GKA_GRAPHS_HPP
#define GKA_GRAPHS_HPP

#include "ggraph_config.h"
#include "graph.hpp"

namespace ggraph {
	enum class ext_t : int {GLUON, QUARK, ANTIQUARK, SCALAR};
	typedef std::pair<ext_t, flavor_t> flExt;

	std::ostream& operator<<(std::ostream& stream_, const ext_t& type_);

	ext_t stringToExtT(const std::string& str_);

	line_t extToLine(const ext_t& t_);
	/**
	 * returns a pair of the line type corresponding to a particle type and a
	 * boolean specifying the line direction (i.e. if true it is an  outgoing line,
	 * otherwise incoming)
	 * (always true for undirected line types)
	 */
	std::pair<line_t, bool> extToLineDir(const ext_t& p_);

	/**
	 * opposite of above function, dir_==true -> outgoing
	 * (meaning outgoing from the graph, incoming into the external vertex)
	 */
	ext_t lineDirToExt(const line_t& t_, const bool& dir_ = true);

	ext_t antiParticle(const ext_t& ext_);

	struct coupling {
		//**************************************************
		// constructors
		//**************************************************
		coupling(const int& number_ = 0) : m_number(number_) {}
		//for mathematica
		coupling(const std::vector<flExt>& exts_, const int& number_ = 0);
		coupling(const std::map<flExt, unsigned int>& particles_, const int& number_ = 0)
			: m_particles(particles_), m_number(number_) {}
		coupling(std::map<flExt, unsigned int>&& particles_, const int& number_ = 0)
			: m_particles(std::move(particles_)), m_number(number_) {}
		
		//**************************************************
		// operators
		//**************************************************
		friend bool operator==(const coupling& left_, const coupling& right_);
		friend bool operator!=(const coupling& left_, const coupling& right_) {
			return !(left_ == right_);
		}

		/**
		 * absolute ordering on couplings
		 */
		friend bool operator<(const coupling& left_, const coupling& right_) {
			return compare(left_, right_) < 1;
		}

		/**
		 * returns negative number if left_ < right_
		 * 0 if left_ == right_
		 * 1 if left_ > right_
		 * checks different properties than operator< above!!!
		 */
		friend int compare(const coupling& left_, const coupling& right_);

		//**************************************************
		// methods
		//**************************************************
		std::size_t size() const;
		
		//maps particle types to how many times it appears in the coupling
		std::map<flExt, unsigned int> m_particles;
		//number that becomes the "ext" m_number of the vertex, defaults to 0
		int m_number;

		const static coupling THREEGLUON;
		const static coupling QUARKGLUON;//flavor 0
		const static coupling QUARKGLUON1;//flavor 1
		const static coupling QUARKGLUON2;//flavor 2
		const static coupling SCALARGLUON;//flavor 0
		const static coupling SCALARGLUON1;//flavor 1
		const static coupling SCALARGLUON2;//flavor 2
		const static coupling THREEQUARK;//flavors 0,1,2
		const static coupling THREEAQUARK;//flavors 0,1,2
		const static coupling THREESCALAR;//flavors 0,1,2
	};

	/**
	 * a set of graphs constructed from given couplings and with a given number of loops
	 * and given external partons
	 */
	class graphs : public std::set<graph> {
	public:
		//**************************************************
		// constructors
		//**************************************************
		graphs() {}
		/**
		 * exactNV_ (!=0) specifies that only diagrams with this exact number of vertices
		 * should be constructed
		 */
		graphs(const std::vector<flExt>& exts_, const unsigned int& nLoops_,
			   std::vector<coupling> couplings_, const std::size_t& exactNV_ = 0);

		/**
		 */
		graphs(std::set<graph>&& gs_) : std::set<graph>(std::move(gs_)) {
		}

		//**************************************************
		// factories
	    //**************************************************
		/**
		 * Makes all PM EFT graphs with undirected gravitons (GLUON lines) for the in-out formalism.
		 * @param order_ PM order
		 * @param nBH_ Number of black holes
		 * @param higherWLCoupling_ If higher order couplings of gravitons to WL should be taken into account
		 */
		static graphs makePMEFTGraphs(std::size_t order_, const std::size_t& nBH_ = 2,
									  const bool& higherWLCoupling_ = false);

		/**
		 * Makes all PM EFT graphs with directed gravitons (QUARK lines) for the VARIATION of the action in
		 * the in-in formalism.
		 * All graphs will be directed such that all arrows point towards a single external vertex (of particle 1)
		 * This "sink" vertex is chosen to have the lowest possible external number
		 * higherWLCoupling_: if there should be higher point WL couplings
		 */
		static graphs makePMEFTGraphsInIn(std::size_t order_, const std::size_t& nBH_ = 2,
										  const bool& higherWLCoupling_ = false);

		/**
		 * makes all PM EFT graphs with directed gravitons and a single external radiating graviton.
		 * Gravitons are "quark" lines with flavor 0
		 * all graphs will be directed such that all arrows point towards a single external vertex=source
		 * this "out" vertex is chosen to have the lowest possible external number
		 * higherWLCoupling_: if there should be higher point WL couplings
		 * the graviton has external number nBH_+1
		 */
		static graphs makePMEFTRadGraphs(std::size_t order_, const std::size_t& nBH_ = 2,
										 const bool& higherWLCoupling_ = false);

		/**
		 * makes all tree graphs to compute the classical impulse using the GR+WL theory
		 */
		static graphs makeGRWLImpulseGraphs(std::size_t orderG_);

		/**
		 * makes all tree graphs to compute the classical action using the GR+WL theory
		 */
		static graphs makeGRWLSGraphs(std::size_t orderG_);

		//**************************************************
		// operators
		//**************************************************
		const graph& operator[](const std::size_t& n_) const;

		//**************************************************
		// methods
		//**************************************************
		friend std::ostream& operator<<(std::ostream& ostream_, const graphs& graphs_);

		void printVerbose(std::ostream& ostream_ = std::cout) const;

		/**
		 * puts explicit external m_numbers to exts
		 * in all permutations, then canonicalizes again and obtains a new graph set
		 * (assumes that graphs are canonicalized with externals first)
		 */
		void diffExts();

	private:
		//**************************************************
		// construction helpers
		//**************************************************
		struct partialGraph {
			//**************************************************
			// constructors
			//**************************************************
			partialGraph(const coupling& coupling_);
			
			//**************************************************
			// methods
			//**************************************************
			/**
			 * joins the m_graphs and the map, relabelling everything accordingly
			 */
			void join(partialGraph&& other_);

			/**
			 * canonicalizes the m_graph and renames the vertices in m_map accordingly
			 */
			void canonicalize();
			
			//**************************************************
			// members
			//**************************************************
			graph m_graph;
			//map from vertex id to available couplings
			std::map<std::size_t, coupling> m_map;
		};
		friend bool operator==(const partialGraph& left_, const partialGraph& right_);
		friend bool operator!=(const partialGraph& left_, const partialGraph& right_) {
			return !(left_ == right_);
		}
		friend bool operator<(const partialGraph& left_, const partialGraph& right_);
		friend int compare(const partialGraph& left_, const partialGraph& right_);
		struct unfinishedGraph {
			//**************************************************
			// constructors
			//**************************************************
			unfinishedGraph(const std::vector<coupling>& couplings_,
							const std::vector<std::size_t>& multiplicities_);

			//**************************************************
			// factory
			//**************************************************
			static std::set<unfinishedGraph> makeUnfinishedGraphs(
				const std::vector<flExt>& exts_, const std::vector<coupling>& couplings_,
				const std::vector<std::size_t>& multiplicities_);
			
			//**************************************************
			// methods
			//**************************************************
			std::vector<unfinishedGraph> glueNext() const;
			unfinishedGraph glueThis(const std::size_t& partialGraphCounter2_,
									 const std::size_t& couplingCounter2_,
									 const std::size_t& particleCounter2_) const;

			unfinishedGraph glueThis(const std::size_t& couplingCounter2_,
									 const std::size_t& particleCounter2_) const;
			
			unfinishedGraph glueThis(const std::size_t& particleCounter2_) const;

			bool hasDisconnectedComponent() const;

			void printVerbose(std::ostream& ostream_ = std::cout) const;

			//**************************************************
			// members
			//**************************************************
			//map counting multiplicity of each partial graph
			std::map<partialGraph, std::size_t> m_partGraphs;
		};
		friend bool operator==(const unfinishedGraph& left_,
							   const unfinishedGraph& right_);
		friend bool operator!=(const unfinishedGraph& left_,
							   const unfinishedGraph& right_) {
			return !(left_ == right_);
		}
		friend bool operator<(const unfinishedGraph& left_,
							  const unfinishedGraph& right_);
	};
}

#endif /*GKA_GRAPHS_HPP*/
