#include "graph.hpp"

#include <boost/graph/connected_components.hpp>
#include <graph_canon/edge_handler/all_equal.hpp>
#include <cassert>
#include <cereal/archives/xml.hpp>
#include <perm_group/group/io.hpp>
#include <algorithm>

#include "util.hpp"
#include "graphRunner.hpp"
#include "graphLoopRunner.hpp"
#include "canonGraphRunner.hpp"
#include "utility/boolRunner.hpp"

namespace ggraph {
	std::ostream& operator<<(std::ostream& ostream_, const line_t& type_) {
		switch(type_) {
		case line_t::GLUON:
			return ostream_ << "\"gluon\"";
		case line_t::QUARK:
			return ostream_ << "\"quark\"";
		case line_t::SCALAR:
			return ostream_ << "\"scalar\"";
		}
		return ostream_;
	}

	line_t stringToLineT(const std::string& str_) {
		if (str_ == "gluon") {
			return line_t::GLUON;
		}
		if (str_ == "quark") {
			return line_t::QUARK;
		}
		if (str_ == "scalar") {
			return line_t::SCALAR;
		}
		assert (false);
		return line_t::GLUON;
	}

	void printLineFl(const line_t& lT_, const flavor_t& fl_, const bool& outgoing_,
					 std::ostream& ostream_) {
		ostream_ << '{';
		switch (lT_) {
		case line_t::GLUON:
			ostream_  <<0;
			break;
		case line_t::QUARK:
			if (outgoing_) {
				ostream_ << 1;
			} else {
				ostream_ << -1;
			}
			break;
		case line_t::SCALAR:
			ostream_ << 2;
		}
		ostream_ << ',' << (fl_ + 1) << '}';
	}

	bool operator==(const graph::edge_t& left_, const graph::edge_t& right_) {
		//compare properties
		if (left_.m_ID != right_.m_ID || left_.m_flavor != right_.m_flavor
			|| left_.m_type != right_.m_type) {
			return false;
		}

		//check direction
		return (left_.first->m_ID == right_.first->m_ID)
			&& (left_.second->m_ID == right_.second->m_ID);
	}

	bool operator<(const graph::edge_t& left_, const graph::edge_t& right_) {
		//compare properties
		if (left_.m_ID < right_.m_ID) {
			return true;
		} else if (left_.m_ID > right_.m_ID) {
			return false;
		}
		if (left_.m_flavor < right_.m_flavor) {
			return true;
		} else if (left_.m_flavor > right_.m_flavor) {
			return false;
		}
		if (left_.m_type < right_.m_type) {
			return true;
		} else if (left_.m_type > right_.m_type) {
			return false;
		}

		//compare connected vertices
		auto& l1 = left_.first->m_ID;
		auto& r1 = right_.first->m_ID;
		if (l1 < r1) {
			return true;
		} else if (l1 > r1) {
			return false;
		}

		auto& l2 = left_.second->m_ID;
		auto& r2 = right_.second->m_ID;
		if (l2 < r2) {
			return true;
		}// else if (l2 > r2) {
		//	return false;
		//}

		return false;
	}
	
	int graph::edge_t::compare(const graph::edge_t& e1_, const graph::edge_t& e2_) {
		if (e1_.m_type < e2_.m_type) {
			return -1;
		} else if (e1_.m_type > e2_.m_type) {
			return 1;
		}

		if (e1_.m_flavor < e2_.m_flavor) {
			return -1;
		} else if (e1_.m_flavor > e2_.m_flavor) {
			return 1;
		}

		return 0;
	}

	graph::vert graph::edge_t::otherActual(const vert v_) {
		auto vs = getActualVerts();
		if (v_ == vs.first) {
			return vs.second;
		}
		//else
		assert(v_ == vs.second);
		return vs.first;
	}

	bool graph::edge_t::isActualEdge() const {
		auto fromNumber = first->m_number;
		auto toNumber = second->m_number;
		if ((fromNumber >= 0 && toNumber >= 0) || (fromNumber < 0 && toNumber < 0)) {
			return true;
		}
		
		return false;
	}

	graph::edge graph::edge_t::getActualEdge() {
		if (!isDirected(m_type)) {
			return this;
		}
		
		const auto fromNumber = first->m_number;
		const auto toNumber = second->m_number;
		if (fromNumber < 0 && toNumber < 0) {
			//we are on the middle edge
			return this;
		}

		if (fromNumber < 0) {
			return first->other(this);
		} 
		return second->other(this);
	}

	std::pair<graph::vert, graph::vert> graph::edge_t::getActualVerts() {
		if (!isDirected(m_type)) {
			return std::make_pair(first, second);
		}

		//determine the middle edge
		auto fromNumber = first->m_number;
		auto toNumber = second->m_number;
		edge middleEdge;
		if (fromNumber < 0 && toNumber < 0) {
			middleEdge = this;
		} else if (fromNumber < 0) {
			middleEdge = first->other(this);
		} else {
			middleEdge = second->other(this);
		}

		return std::make_pair(
			middleEdge->first->other(middleEdge)->first,
			middleEdge->second->other(middleEdge)->second);
	}

	void graph::edge_t::replaceVertex(const vert& vOld_, const vert& vNew_) {
		if (first == vOld_) {
			first = vNew_;
			return;
		}
		if (second == vOld_) {
			second = vNew_;
			return;
		}
		assert(false);
	}

	bool graph::edge_t::isClosedLoop() {
		//tadpole check
		if (first == second) {
			return true;
		}

		const edge thisE(this);
		auto currentV = first;
	    auto currentE = thisE;

		do {
			if (currentV->m_number > 0) {
				//external vertex
				return false;
			}
			//step
#ifndef NDEBUG
			bool found = false;
#endif
			for (auto& e : *currentV) {
				if (e == thisE) {
					continue;
				}
				if (e->m_type == m_type && e->m_flavor == m_flavor) {
					//found a line continuing the path
#ifndef NDEBUG					
					found = true;
#endif					
					currentE = e.m_edge;
					currentV = (currentE->first == currentV) ?
						currentE->second : currentE->first;
				}
			}
#ifndef NDEBUG
			if (!found) {
				assert(false);
			}
#endif
		} while (currentE != thisE);
		//it terminated, i.e. we are back to this edge and have a closed loop
		return true;
	}

	bool graph::edge_t::isLoop() {
		graphRunner gR(first, edge(this));
		do {
			if (gR.getCurrent() == second) {
				return true;
			}
		} while (gR.next());

		return false;
	}

	bool graph::edge_t::isNull() {
		//idea: use graphRunner and count external legs
		//check first direction
		graphRunner gR(first, this);
		unsigned int counter = 0;
		do {
			if (gR.getCurrent()->m_number > 0) {
				if ((++counter) > 1) {
					break;
				}
			}
		} while (gR.next());

		if (counter <= 1) {
			return true;
		}

		//check second direction
		gR = graphRunner(second, this);
		counter = 0;
		do {
			if (gR.getCurrent()->m_number > 0) {
				if ((++counter) > 1) {
					return false;
				}
			}
		} while (gR.next());

		assert(counter <= 1);
		return true;
	}

	void graph::edge_t::permuteFlavors(const std::map<flavor_t, flavor_t>& map_) {
		auto it = map_.find(m_flavor);
		if (it != map_.end()) {
			m_flavor = it->second;
		}
	}

	std::pair<graph, graphMap> graph::toTopo() const {
		//copy
		auto g = *this;

		//fix edges
		std::size_t fakeCounter = 0;
		std::vector<std::pair<size_t, bool> > edgeMap;
		for (std::size_t i = 0; i != g.nEdges(); ++i) {
			auto& e = g.m_edges[i];
			auto& origE = m_edges[i+fakeCounter];
			if (origE->isActualEdge()) {
				edgeMap.push_back(std::make_pair(i, false));
				e->m_flavor = -1;
				e->m_type = line_t::GLUON;
			} else {
				bool isFromFake = (e->first->m_number < 0);
				auto vFake = isFromFake ? e->first : e->second;
				auto v = e->other(vFake);
				auto actualE = vFake->other(e.get());
				//reconnect
				if (vFake->m_number == -1) {
					actualE->first = v;
				} else {
					assert(vFake->m_number == -2);
					actualE->second = v;
				}
				v->replaceEdge(edge(e.get(),!isFromFake), actualE);
				edgeMap.push_back(std::make_pair(-1, false));
				//remove edge
				g.removeEdge(e.get());

				//fix counters
				++fakeCounter;
				--i;
			}
		}

		//fix vertices
		std::vector<std::size_t> vertMap;
		vertMap.reserve(nVerts());
		for (std::size_t i = 0; i != g.nVerts(); ++i) {
			if (m_verts[i]->m_number < 0) {
				vertMap.push_back(-1);
				g.removeVertex(i);
				//fix counter
				--i;
			} else {
				vertMap.push_back(i);
			}
		}
		graphMap map(std::move(vertMap), std::move(edgeMap), false);

		return std::make_pair(std::move(g), std::move(map));
	}

	std::vector<std::vector<std::size_t> > graph::getQuarkLines() const {
		std::vector<std::vector<std::size_t>> ret;
		std::set<edge> alreadyVisited;
		for (auto& e : m_edges) {
			const edge ee = e.get();
			if (!ee->isActualEdge() || ee->m_type != line_t::QUARK ||
				alreadyVisited.find(ee) != alreadyVisited.end()) {
				continue;
			}
			std::vector<std::size_t> line;
			
			//search towards front
			edge current = ee;
			auto nextV = current->second;
			do {
				if (current->isActualEdge()) {
					alreadyVisited.insert(current);
					line.push_back(current->m_ID);
				}
				//step
#ifndef NDEBUG
				bool found = false;
#endif
				for (auto& e2 : *nextV) {
					if (e2 == current) {
						continue;
					}
					if (e2->m_type == line_t::QUARK && e2->m_flavor == ee->m_flavor) {
#ifndef NDEBUG						
						found = true;
#endif						
						current = e2;
						break;
					}
				}
#ifndef NDEBUG				
				assert(found);
#endif
				nextV = current->second;
			} while (current != ee && nextV->m_number < 1);

			//search towards back only if not a loop
			if (current != ee) {
				current = ee;
				nextV = current->first;
				do {
					if (current->isActualEdge()) {
						alreadyVisited.insert(current);
						line.push_back(current->m_ID);
					}
					//step
#ifndef NDEBUG
					bool found = false;
#endif
					for (auto& e2 : *nextV) {
						if (e2 == current) {
							continue;
						}
						if (e2->m_type == line_t::QUARK && e2->m_flavor == ee->m_flavor) {
#ifndef NDEBUG							
							found = true;
#endif							
							current = e2;
							break;
						}
					}
#ifndef NDEBUG					
					assert(found);
#endif				
					nextV = current->first;
				} while (current != ee && nextV->m_number < 1);
			}

			ret.push_back(std::move(line));
		}

		return ret;
	}

	std::size_t graph::countQuarkLoops() const {
		std::size_t ret = 0;
		std::set<edge> alreadyVisited;
		for (auto& e : m_edges) {
			const edge ee = e.get();
			if (!ee->isActualEdge() || ee->m_type != line_t::QUARK ||
				alreadyVisited.find(ee) != alreadyVisited.end()) {
				continue;
			}
			//search towards front
			edge current = ee;
			auto nextV = current->second;
			do {
				if (current->isActualEdge()) {
					alreadyVisited.insert(current);
				}
				//step
#ifndef NDEBUG
				bool found = false;
#endif
				for (auto& e2 : *nextV) {
					if (e2 == current) {
						continue;
					}
					if (e2->m_type == line_t::QUARK && e2->m_flavor == ee->m_flavor) {
#ifndef NDEBUG						
						found = true;
#endif						
						current = e2;
						break;
					}
				}
#ifndef NDEBUG				
				assert(found);
#endif
				nextV = current->second;
			} while (current != ee && nextV->m_number < 1);

			++ret;
			//search towards back only if not a loop
			if (current != ee) {
				--ret;//it was not a loop
				current = ee;
				nextV = current->first;
				do {
					if (current->isActualEdge()) {
						alreadyVisited.insert(current);
					}
					//step
#ifndef NDEBUG
					bool found = false;
#endif
					for (auto& e2 : *nextV) {
						if (e2 == current) {
							continue;
						}
						if (e2->m_type == line_t::QUARK && e2->m_flavor == ee->m_flavor) {
#ifndef NDEBUG							
							found = true;
#endif							
							current = e2;
							break;
						}
					}
#ifndef NDEBUG					
					assert(found);
#endif				
					nextV = current->first;
				} while (current != ee && nextV->m_number < 1);
			}
		}

		return ret;
	}

	std::vector<std::vector<std::size_t> > graph::getScalarLines() const {
		std::vector<std::vector<std::size_t>> ret;
		std::set<edge> alreadyVisited;
		for (auto& e : m_edges) {
			const edge ee = e.get();
			if (ee->m_type != line_t::SCALAR ||
				alreadyVisited.find(e.get()) != alreadyVisited.end()) {
				continue;
			}
			//tadpoles
			if (ee->first == ee->second) {
				alreadyVisited.insert(ee);
				ret.push_back(std::vector<std::size_t>(1,ee->m_ID));
				continue;
			}
			std::vector<std::size_t> line;
			
			//search towards front
			edge current = ee;
			auto nextV = current->second;
			do {
				alreadyVisited.insert(current);
				line.push_back(current->m_ID);
				//step
#ifndef NDEBUG
				bool found = false;
#endif
				for (auto& e2 : *nextV) {
					if (e2 == current) {
						continue;
					}
					if (e2->m_type == line_t::SCALAR && e2->m_flavor == ee->m_flavor) {
#ifndef NDEBUG						
						found = true;
#endif						
						current = e2;
						break;
					}
				}
#ifndef NDEBUG				
				assert(found);
#endif				
				nextV = current->other(nextV);
			} while (current != ee && nextV->m_number != 0);

			//search towards back only if not a loop
			if (current != ee) {
				current = ee;
				nextV = current->first;
				do {
					alreadyVisited.insert(current);
					line.push_back(current->m_ID);
					//step
#ifndef NDEBUG
					bool found = false;
#endif
					for (auto& e2 : *nextV) {
						if (e2 == current) {
							continue;
						}
						if (e2->m_type == line_t::SCALAR && e2->m_flavor == ee->m_flavor) {
#ifndef NDEBUG							
							found = true;
#endif							
							current = e2;
							break;
						}
					}
#ifndef NDEBUG					
					assert(found);
#endif					
					nextV = current->other(nextV);
				} while (current != ee && nextV->m_number != 0);
			}

			ret.push_back(std::move(line));
		}

		return ret;
	}

	bool operator==(const graph::vert_t& left_, const graph::vert_t& right_) {
		if (left_.m_ID != right_.m_ID || left_.m_number != right_.m_number
			|| left_.size() != right_.size()) {
			return false;
		}

		for (auto i = 0; i != left_.size(); ++i) {
			auto& l = left_[i];
			auto& r = right_[i];
			if (l->m_ID != r->m_ID || l.m_dir != r.m_dir) {
				return false;
			}
		}

		return true;
	}

	bool operator<(const graph::vert_t& left_, const graph::vert_t& right_) {
		//compare properties
		if (left_.m_ID < right_.m_ID) {
			return true;
		} else if (left_.m_ID > right_.m_ID) {
			return false;
		}
		//inverse ordering
		if (left_.m_number > right_.m_number) {
			return true;
		} else if (left_.m_number < right_.m_number) {
			return false;
		}
		//compare vertex size
		if (left_.size() < right_.size()) {
			return true;
		} else if (left_.size() > right_.size()) {
			return false;
		}

		//compare edges
		for (auto i = 0; i != left_.size(); ++i) {
			auto& l = left_[i];
			auto& r = right_[i];
			if (l->m_ID < r->m_ID) {
				return true;
			} else if (l->m_ID > r->m_ID) {
				return false;
			}
			if (l.m_dir < r.m_dir) {
				return true;
			}
			if (l.m_dir > r.m_dir) {
				return false;
			}
		}

		return false;
	}

	bool lessSize(const graph::vert_t& left_, const graph::vert_t& right_) {
		//inverse ordering (external before internal vertices)
		if (left_.m_number > right_.m_number) {
			return true;
		} else if (left_.m_number < right_.m_number) {
			return false;
		}
		return left_.size() < right_.size();
	}

	graph::edge graph::vert_t::other(const edge e_) {
		assert(size() == 2);
		if (front() == e_) {
			return back();
		}
		assert(back() == e_);
		return front();
	}

	const graph::edge graph::vert_t::other(const edge e_) const {
		assert(size() == 2);
		if (front() == e_) {
			return back();
		}
		assert(back() == e_);
		return front();
	}

	bool graph::vert_t::removeEdge(const edge& e_) {
		auto it = std::find_if(begin(), end(),
							   [&] (const edge& ee_) {
								   return e_ == ee_ && e_.m_dir == ee_.m_dir;
							   });
		if (it == end()) {
			return false;
		}
		//else
		erase(it);
		return true;
	}

	bool graph::vert_t::removeEdge(const std::unique_ptr<edge_t>& e_) {
		auto it = std::find(begin(), end(), e_.get());
		if (it == end()) {
			return false;
		}
		//else
		erase(it);
		return true;
	}

	void graph::vert_t::replaceEdge(const edge& eOld_, const edge& eNew_) {
		for (auto& e : *this) {
			if (e == eOld_ && e.m_dir == eOld_.m_dir) {
				e = eNew_;
				return;
			}
		}
		assert(false);
	}

	void graph::vert_t::replaceEdge(const std::unique_ptr<edge_t>& eOld_,
									const edge& eNew_) {
		for (auto& e : *this) {
			if (e.m_edge == eOld_.get()) {
				e = eNew_;
				return;
			}
		}
		assert(false);
	}

	void graph::vert_t::replaceEdges(const edge& eOld1_, const edge& eNew1_,
									 const edge& eOld2_, const edge& eNew2_) {
		bool found = false;
		for (auto& e : *this) {
			if (e == eOld1_ && e.m_dir == eOld1_.m_dir) {
				e = eNew1_;
				if (found == true) {
					return;
				} else {
					found = true;
				}
			} else if (e == eOld2_ && e.m_dir == eOld2_.m_dir) {
				e = eNew2_;
				if (found == true) {
					return;
				} else {
					found = true;
				}
			}
		}
		assert(false);
	}

	void graph::vert_t::changeEdgeDir(const edge& e_) {
		for (auto& e : *this) {
			if (e == e_ && e.m_dir == e_.m_dir) {
				e.m_dir = !e.m_dir;
				return;
			}
		}
		assert(false);
	}

	void graph::vert_t::changeEdgeDir(const std::unique_ptr<edge_t>& e_) {
		for (auto& e : *this) {
			if (e.m_edge == e_.get()) {
				e.m_dir = !e.m_dir;
				return;
			}
		}
		assert(false);
	}

	graph::edge graph::vert_t::next(const edge& e_) {
		auto it = std::find_if(begin(), end(),
							   [&] (const edge& ee_) {
								   return e_==ee_ && e_.m_dir == ee_.m_dir;
							   });
		if (it == end()) {
			return nullptr;
		}
		//else
		if (++it == end()) {
			it = begin();
		}
		return *it;
	}

	const graph::edge graph::vert_t::next(const edge& e_) const {
		auto it = std::find_if(begin(), end(),
							   [&] (const edge& ee_) {
								   return e_==ee_ && e_.m_dir == ee_.m_dir;
							   });
		if (it == end()) {
			return nullptr;
		}
		//else
		if (++it == end()) {
			it = begin();
		}
		return *it;
	}

	std::pair<graph::edge, graph::edge> graph::vert_t::nextTwo(const edge& e_) {
		auto it = std::find_if(begin(), end(),
							   [&] (const edge& ee_) {
								   return e_==ee_ && e_.m_dir == ee_.m_dir;
							   });
		std::pair<edge, edge> ret(nullptr, nullptr);
		if (it == end()) {
			return ret;
		}
		//else
		if (++it == end()) {
			it = begin();
		}
		ret.first = *it;
		if (++it == end()) {
			it = begin();
		}
		ret.second = *it;
		return ret;
	}

	std::pair<const graph::edge, const graph::edge> graph::vert_t::nextTwo(
		const edge& e_) const {
		auto it = std::find_if(begin(), end(),
							   [&] (const edge& ee_) {
								   return e_==ee_ && e_.m_dir == ee_.m_dir;
							   });
		std::pair<edge, edge> ret(nullptr, nullptr);
		if (it == end()) {
			return ret;
		}
		//else
		if (++it == end()) {
			it = begin();
		}
		ret.first = *it;
		if (++it == end()) {
			it = begin();
		}
		ret.second = *it;
		return ret;
	}

	void graph::vert_t::swapEdges(const edge& e1_, const edge& e2_) {
		auto it1 = std::find(begin(), end(), e1_);
		auto it2 = std::find(begin(), end(), e2_);
		assert(it1 != end() && it2 != end());
		std::swap(*it1, *it2);
	}

	graph::graph(std::istream& istream_) {
		cereal::XMLInputArchive ar(istream_);
		ar(*this);
	}

	graph::graph(const graph& other_) {
		m_verts.reserve(other_.m_verts.size());
		m_edges.reserve(other_.m_edges.size());

		//copy vert and edge proporties
		for (const auto& v : other_.m_verts) {
			m_verts.emplace_back(new vert_t(*v));
		}

		for (const auto& e : other_.m_edges) {
			m_edges.emplace_back(new edge_t(*e));
			//relink vertices
			m_edges.back()->first = m_verts[e->first->m_ID].get();
			m_edges.back()->second = m_verts[e->second->m_ID].get();
		}

		//relink edges
		for (std::size_t i = 0; i != m_verts.size(); ++i) {
			auto& vFrom = *other_.m_verts[i];
			auto& vTo = *m_verts[i];
			for (auto& e : vFrom) {
				vTo.push_back(edge(m_edges[e->m_ID].get(), e.m_dir));
			}
		}
	}

	graph& graph::operator=(const graph& other_) {
		m_verts.clear();
		m_edges.clear();
		m_verts.reserve(other_.m_verts.size());
		m_edges.reserve(other_.m_edges.size());

		//copy vert and edge proporties
		for (const auto& v : other_.m_verts) {
			m_verts.emplace_back(new vert_t(*v));
		}

		for (const auto& e : other_.m_edges) {
			m_edges.emplace_back(new edge_t(*e));
			//relink vertices
			m_edges.back()->first = m_verts[e->first->m_ID].get();
			m_edges.back()->second = m_verts[e->second->m_ID].get();
		}
		//relink edges
		for (std::size_t i = 0; i != m_verts.size(); ++i) {
			auto& vFrom = *other_.m_verts[i];
			auto& vTo = *m_verts[i];
			vTo.reserve(vFrom.size());
			for (auto& e : vFrom) {
				vTo.push_back(edge(m_edges[e->m_ID].get(), e.m_dir));
			}
		}

		return *this;
	}
 
	bool operator==(const graph& left_, const graph& right_) {
		//compare sizes
		if (left_.m_verts.size() != right_.m_verts.size()
			|| left_.m_edges.size() != right_.m_edges.size()) {
			return false;
		}

		//compare vertices
		for (auto i = 0; i != left_.m_verts.size(); ++i) {
			if (*left_.m_verts[i] != *right_.m_verts[i]) {
				return false;
			}
		}

		//compare edges
		for (auto i = 0; i != left_.m_edges.size(); ++i) {
			if (*left_.m_edges[i] != *right_.m_edges[i]) {
				return false;
			}
		}

		return true;
	}

	bool operator<(const graph& left_, const graph& right_) {
		//compare sizes
		if (left_.m_verts.size() < right_.m_verts.size()) {
			return true;
		} else if (left_.m_verts.size() > right_.m_verts.size()) {
			return false;
		}
		if (left_.m_edges.size() < right_.m_edges.size()) {
			return true;
		} else if (left_.m_edges.size() > right_.m_edges.size()) {
			return false;
		}

		//compare vertices
		for (auto i = 0; i != left_.m_verts.size(); ++i) {
			auto& l = *left_.m_verts[i];
			auto& r = *right_.m_verts[i];
			if (l < r) {
				return true;
			} else if (r < l) {
				return false;
			}
		}

		for (auto i = 0; i != left_.m_edges.size(); ++i) {
			auto& l = *left_.m_edges[i];
			auto& r = *right_.m_edges[i];
			if (l < r) {
				return true;
			} else if (r < l) {
				return false;
			}
		}

		return false;
	}

	int compare(const graph& left_, const graph& right_) {
		//compare sizes
		if (left_.m_verts.size() < right_.m_verts.size()) {
			return -1;
		} else if (left_.m_verts.size() > right_.m_verts.size()) {
			return 1;
		}
		if (left_.m_edges.size() < right_.m_edges.size()) {
			return -1;
		} else if (left_.m_edges.size() > right_.m_edges.size()) {
			return 1;
		}

		//compare vertices
		for (auto i = 0; i != left_.m_verts.size(); ++i) {
			auto& l = *left_.m_verts[i];
			auto& r = *right_.m_verts[i];
			if (l < r) {
				return -1;
			} else if (r < l) {
				return 1;
			}
		}

		for (auto i = 0; i != left_.m_edges.size(); ++i) {
			auto& l = *left_.m_edges[i];
			auto& r = *right_.m_edges[i];
			if (l < r) {
				return -1;
			} else if (r < l) {
				return 1;
			}
		}

		return 0;
	}

	std::size_t graph::nExts() const {
		std::size_t n = 0;
		for (auto it = vBegin(); it != vEnd(); ++it) {
			if ((*it)->m_number <= 0) {
				return n;
			}
			++n;
		}
		return n;
	}
	
	std::vector<std::unique_ptr<graph::vert_t> >::iterator graph::extEnd() {
		auto it = vBegin();
		for (const auto end = vEnd(); it != end; ++it) {
			if ((*it)->m_number <= 0) {
				return it;
			}
		}
		return it;
	}
	
	std::vector<std::unique_ptr<graph::vert_t> >::const_iterator graph::extEnd() const {
		auto it = vBegin();
		for (const auto end = vEnd(); it != end; ++it) {
			if ((*it)->m_number <= 0) {
				return it;
			}
		}
		return it;
	}

	std::vector<size_t> graph::canonicalize() {
		//ret.first is a permutation from the input IDs to canonical IDs
		graph_canon::canonicalizer<std::size_t, graph::edgeHandlerCreator_t, true, true>
			canonicalizer(edgeHandlerCreator);
		auto ret = canonicalizer(*this, m_indexMap, m_vertexLess, defaultVisitor);

		//move verts to their correction position
		//invert to get a map from canonical IDs to input IDs
		const auto perm = perm_group::make_inverse(ret.first);

		auto vertsOld = std::move(m_verts);
		m_verts.clear();
		m_verts.reserve(vertsOld.size());

		for (const auto& i : perm) {
			m_verts.push_back(std::move(vertsOld[i]));
			m_verts.back()->m_ID = m_verts.size() - 1;
		}

		//change direction of undirected edges
		for (auto& e : m_edges) {
			if (!isDirected(e->m_type) && (e->first->m_ID > e->second->m_ID)) {
				//first change m_dirs
				edge ee(e.get(), true);
				e->first->changeEdgeDir(ee);
				ee.m_dir = false;
				e->second->changeEdgeDir(ee);
				std::swap(e->first, e->second);
			}
		}

		//sort edges globally
		std::sort(m_edges.begin(), m_edges.end(), m_edgeLess);
		std::size_t counter = 0;
		for (auto& e : m_edges) {
			e->m_ID = counter++;
		}

		//sort edges for each vertex
		for (auto& v : m_verts) {
			if (v->size() == 1) {
				continue;
			}
			std::sort(v->begin(), v->end(),
					  [](const edge& left_, const edge& right_) {
						  if (left_->m_ID == right_->m_ID) {
							  //sort tadpole legs!
							  assert(left_.m_dir != right_.m_dir);
							  return left_.m_dir < right_.m_dir;
						  }
						  //else
						  return left_->m_ID < right_->m_ID;
					  });
		}

		return ret.first;
	}
 
	graphMap graph::canonMap() {
		//ret.first is a permutation from the input IDs to canonical IDs
		graph_canon::canonicalizer<std::size_t, graph::edgeHandlerCreator_t, true, true>
			canonicalizer(edgeHandlerCreator);
		auto ret = canonicalizer(*this, m_indexMap, m_vertexLess, defaultVisitor);

		//move verts to their correction position
		//invert to get a map from canonical IDs to input IDs
		const auto perm = perm_group::make_inverse(ret.first);

		auto vertsOld = std::move(m_verts);
		m_verts.clear();
		m_verts.reserve(vertsOld.size());

		for (const auto& i : perm) {
			m_verts.push_back(std::move(vertsOld[i]));
			m_verts.back()->m_ID = m_verts.size() - 1;
		}

		//change direction of undirected edges
		std::vector<bool> dirMap;
		for (auto& e : m_edges) {
			if (!isDirected(e->m_type) && (e->first->m_ID > e->second->m_ID)) {
				dirMap.push_back(true);
				//first change m_dirs
				edge ee(e.get(), true);
				e->first->changeEdgeDir(ee);
				ee.m_dir = false;
				e->second->changeEdgeDir(ee);
				std::swap(e->first, e->second);
			} else {
				dirMap.push_back(false);
			}
		}

		//sort edges globally
		auto ePerm = ordering(m_edges, m_edgeLess);
		std::vector<std::pair<std::size_t, bool> > ePermDir(m_edges.size(),
															std::make_pair(0,false));
		ePermDir.reserve(ePerm.size());
		auto oldEdges = std::move(m_edges);
		m_edges.clear();
		m_edges.reserve(oldEdges.size());
		for (auto& e : ePerm) {
			ePermDir[e] = std::make_pair(m_edges.size(), dirMap[e]);
			m_edges.push_back(std::move(oldEdges[e]));
			m_edges.back()->m_ID = m_edges.size() - 1;
		}

		//sort edges for each vertex and get the sign
		bool sign = false;
		for (auto& v : m_verts) {
			auto order = ordering(*v,
								  [](const edge& left_, const edge& right_) {
									  if (left_->m_ID == right_->m_ID) {
										  //sort tadpole legs!
										  assert(left_.m_dir != right_.m_dir);
										  return left_.m_dir < right_.m_dir;
									  }
									  //else
									  return left_->m_ID < right_->m_ID;
								  });
			//apply the ordering
			std::vector<edge> oldV = std::move(*v);
			v->clear();
			v->reserve(oldV.size());
			for (auto& newPos : order) {
				v->push_back(std::move(oldV[newPos]));
			}

			//compute the sign
			if (parity(order)) {
				sign = !sign;
			}
		}

		return graphMap(std::move(ret.first), std::move(ePermDir), sign);
	}

	std::vector<size_t> graph::canonicalizeOrdered(vert min_) {
		if (!min_) {
			//idea: first find "smallest" external vertex by also using its position
			//in the graph
			//i.e. start from each external and canonically go through the graph
		    //(using canonGraphRunner) to compare each vertex in order
			//then sort the vertices globally by starting from the smallest external
			//and then follow the graphRunner
			std::vector<vert> exts;
			for (auto& v : m_verts) {
				if (v->m_number > 0) {
					exts.push_back(v.get());
				}
			}

			//get the new vertex (and global edge) ordering
			min_ = *std::min_element(exts.begin(), exts.end(), m_extLessOrdered);
		}
		std::vector<size_t> ret(nVerts(), 0);
		std::vector<size_t> edgeOrdering(nEdges(), 0);
		std::size_t counterV = 0;
		std::size_t counterE = 0;
		std::set<std::size_t> alreadyVisitedE;
		canonGraphRunner gR(min_,
							[&] (const graph::edge& e_) {
								if (alreadyVisitedE.find(e_->m_ID)
									==alreadyVisitedE.end()) {
									edgeOrdering[e_->m_ID] = counterE++;
									alreadyVisitedE.insert(e_->m_ID);
								}
							});
		do {
			ret[gR.getCurrent()->m_ID] = counterV++;
			auto eID = gR.nextEdge()->m_ID;
			if (alreadyVisitedE.find(eID)==alreadyVisitedE.end()) {
				edgeOrdering[eID] = counterE++;
				alreadyVisitedE.insert(eID);
			}
		} while (gR.next());

		//move verts to their correction position
		//invert to get a map from canonical IDs to input IDs
		const auto perm = perm_group::make_inverse(ret);

		auto vertsOld = std::move(m_verts);
		m_verts.clear();
		m_verts.reserve(vertsOld.size());

		for (const auto& i : perm) {
			m_verts.push_back(std::move(vertsOld[i]));
			m_verts.back()->m_ID = m_verts.size() - 1;
		}

		//change direction of undirected edges
		for (auto& e : m_edges) {
			if (!isDirected(e->m_type) && (e->first->m_ID > e->second->m_ID)) {
				//first change m_dirs
				edge ee(e.get(), true);
				e->first->changeEdgeDir(ee);
				ee.m_dir = false;
				e->second->changeEdgeDir(ee);
				std::swap(e->first, e->second);
			}
		}

		//sort edges globally
		//invert to get a map from canonical IDs to input IDs
		edgeOrdering = perm_group::make_inverse(edgeOrdering);
		auto edgesOld = std::move(m_edges);
		m_edges.clear();
		m_edges.reserve(edgesOld.size());
		for (const auto& i : edgeOrdering) {
			m_edges.push_back(std::move(edgesOld[i]));
			m_edges.back()->m_ID = m_edges.size() - 1;
		}

		//canonically rotate edges for each vertex
		for (auto& v : m_verts) {
			if (v->size() == 1) {
				continue;
			}
			std::rotate(v->begin(),
						std::min_element(v->begin(), v->end(),
										 [](const edge& left_, const edge& right_) {
											 return (left_->m_ID < right_->m_ID);
										 }),
						v->end());
		}

		return ret;
	}

	graphMap graph::canonMapOrdered(vert min_) {
		if (!min_) {
			//idea: first find "smallest" external vertices by also using its position
			//in the graph
			//i.e. start from each external and canonically go through the graph
		    //(using graphRunner) to compare each vertex in order
			//then sort the vertices globally by starting from the smallest external
			//and then follow the graphRunner
			std::vector<vert> exts;
			for (auto& v : m_verts) {
				if (v->m_number > 0) {
					exts.push_back(v.get());
				}
			}

			min_ = *std::min_element(exts.begin(), exts.end(), m_extLessOrdered);
		}
		//get the new vertex ordering
		std::vector<size_t> ret(nVerts(), 0);
		std::vector<size_t> edgeOrdering(nEdges(), 0);
		std::size_t counterV = 0;
		std::size_t counterE = 0;
		std::set<std::size_t> alreadyVisitedE;
		canonGraphRunner gR(min_,
							[&] (const graph::edge& e_) {
								if (alreadyVisitedE.find(e_->m_ID)
									==alreadyVisitedE.end()) {
									edgeOrdering[e_->m_ID] = counterE++;
									alreadyVisitedE.insert(e_->m_ID);
								}
							});
		do {
			ret[gR.getCurrent()->m_ID] = counterV++;
			auto eID = gR.nextEdge()->m_ID;
			if (alreadyVisitedE.find(eID)==alreadyVisitedE.end()) {
				edgeOrdering[eID] = counterE++;
				alreadyVisitedE.insert(eID);
			}
		} while (gR.next());

		//move verts to their correction position
		//invert to get a map from canonical IDs to input IDs
		const auto perm = perm_group::make_inverse(ret);

		auto vertsOld = std::move(m_verts);
		m_verts.clear();
		m_verts.reserve(vertsOld.size());

		for (const auto& i : perm) {
			m_verts.push_back(std::move(vertsOld[i]));
			m_verts.back()->m_ID = m_verts.size() - 1;
		}

		//change direction of undirected edges
		std::vector<bool> dirMap;
		for (auto& e : m_edges) {
			if (!isDirected(e->m_type) && (e->first->m_ID > e->second->m_ID)) {
				dirMap.push_back(true);
				//first change m_dirs
				edge ee(e.get(), true);
				e->first->changeEdgeDir(ee);
				ee.m_dir = false;
				e->second->changeEdgeDir(ee);
				std::swap(e->first, e->second);
			} else {
				dirMap.push_back(false);
			}
		}

		//sort edges globally
		edgeOrdering = perm_group::make_inverse(edgeOrdering);
		std::vector<std::pair<std::size_t, bool> > ePermDir(m_edges.size(),
															std::make_pair(0,false));
		auto oldEdges = std::move(m_edges);
		m_edges.clear();
		m_edges.reserve(oldEdges.size());
		for (auto& e : edgeOrdering) {
			ePermDir[e] = std::make_pair(m_edges.size(), dirMap[e]);
			m_edges.push_back(std::move(oldEdges[e]));
			m_edges.back()->m_ID = m_edges.size() - 1;
		}

		bool sign = false;
		//canonically rotate the edges at each vertex
		for (auto& v : m_verts) {
			if (v->size() == 1) {
				continue;
			}
			std::rotate(v->begin(),
						std::min_element(v->begin(), v->end(),
										 [](const edge& left_, const edge& right_) {
											 return (left_->m_ID < right_->m_ID);
										 }),
						v->end());
		}

		return graphMap(std::move(ret), std::move(ePermDir), sign);
	}

	graphMap graph::findMap() const {
		//perm.first is a permutation from the input IDs to canonical IDs
		graph_canon::canonicalizer<std::size_t, graph::edgeHandlerCreator_t, true, true>
			canonicalizer(edgeHandlerCreator);
		auto perm = canonicalizer(*this, m_indexMap, m_vertexLess, defaultVisitor);
		//auto perm = canonicalizer(*this, m_indexMap, m_vertexLess, defaultVisitor);

		//make a copy to canonicalize the vertices
		auto g = *this;
		//move verts to their correction position
		//invert to get a map from canonical IDs to input IDs
		const auto invPerm = perm_group::make_inverse(perm.first);

		auto vertsOld = std::move(g.m_verts);
		g.m_verts.clear();
		g.m_verts.reserve(vertsOld.size());

		for (const auto& i : invPerm) {
			g.m_verts.push_back(std::move(vertsOld[i]));
			g.m_verts.back()->m_ID = g.m_verts.size() - 1;
		}

		//change direction of undirected edges
		std::vector<bool> dirMap;
		for (auto& e : g.m_edges) {
			if (!isDirected(e->m_type) && (e->first->m_ID > e->second->m_ID)) {
				dirMap.push_back(true);
				//first change m_dirs
				edge ee(e.get(), true);
				e->first->changeEdgeDir(ee);
				ee.m_dir = false;
				e->second->changeEdgeDir(ee);
				std::swap(e->first, e->second);
			} else {
				dirMap.push_back(false);
			}
		}

		//sort edges globally
		auto ePerm = ordering(g.m_edges, m_edgeLess);
		std::vector<std::pair<std::size_t, bool> > ePermDir(m_edges.size(),
															std::make_pair(0,false));
		auto oldEdges = std::move(g.m_edges);
		g.m_edges.clear();
		g.m_edges.reserve(oldEdges.size());
		for (auto& e : ePerm) {
			ePermDir[e] = std::make_pair(m_edges.size(), dirMap[e]);
			g.m_edges.push_back(std::move(oldEdges[e]));
			g.m_edges.back()->m_ID = g.m_edges.size() - 1;
		}

		//get edge ordering for each vertex to compute the sign
		bool sign = false;
		for (auto& v : m_verts) {
			auto order = ordering(*v,
								  [](const edge& left_, const edge& right_) {
									  if (left_->m_ID == right_->m_ID) {
										  //sort tadpole legs!
										  assert(left_.m_dir != right_.m_dir);
										  return left_.m_dir < right_.m_dir;
									  }
									  //else
									  return left_->m_ID < right_->m_ID;
								  });
			//compute the sign
			if (parity(order)) {
				sign = !sign;
			}
		}

		return graphMap(std::move(perm.first), std::move(ePermDir), sign);
	}

	std::ostream& operator<<(std::ostream& ostream_, const graph& g_) {
		//vertices
		ostream_ << "{{";
		bool first = true;
		for (const auto& v : g_.m_verts) {
			if (v->m_number < 0) {
				continue;
			}
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << '{' << v->m_ID + 1 << ',';
			//check if external
			if (v->m_number > 0) {
				ostream_ << v->m_number << ',';
			} 
			//connected edges
			ostream_ << '{';
			bool firstE = true;
			for (const auto& e : *v) {
				if (firstE) {
					firstE = false;
				} else {
					ostream_ << ',';
				}
				if (!e.m_dir) {
					ostream_ << '-';
				}
				ostream_ << (e->getActualEdge()->m_ID + 1);
			}
			ostream_ << "}}";
		}
		ostream_ << "},{";
		//edges
		first = true;
		for (const auto& e : g_.m_edges) {
			if (!e->isActualEdge()) {
				continue;
			}
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			auto verts = e->getActualVerts();
			ostream_ << '{' << e->m_type << ',' << e->m_ID + 1 << ',' << verts.first->m_ID + 1
					 << "->" << verts.second->m_ID + 1 << ',' << '{' << e->m_flavor << "}}";
			
		}
		ostream_ << "}}";

		return ostream_;
	}

	void graph::join(graph&& other_) {
		//re-ID vertices and edges
		auto size = m_verts.size();
		for (auto& v : other_.m_verts) {
			v->m_ID += size;
		}

		size = m_edges.size();
		for (auto& e : other_.m_edges) {
			e->m_ID += size;
		}

		//move vertices and edges
		m_verts.insert(m_verts.end(), std::make_move_iterator(other_.m_verts.begin()),
					   std::make_move_iterator(other_.m_verts.end()));
		m_edges.insert(m_edges.end(), std::make_move_iterator(other_.m_edges.begin()),
					   std::make_move_iterator(other_.m_edges.end()));
	}

	void graph::printVerbose(std::ostream& ostream_) const {
		//vertices
		ostream_ << "{{";
		bool first = true;
		for (const auto& v : m_verts) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << '{' << v->m_ID + 1 << ',';
			ostream_ << v->m_number << ',';
			//connected edges
			ostream_ << '{';
			bool firstE = true;
			for (const auto& e : *v) {
				if (firstE) {
					firstE = false;
				} else {
					ostream_ << ',';
				}
				if (!e.m_dir) {
					ostream_ << '-';
				}
				ostream_ << (e->m_ID + 1);
			}
			ostream_ << "}}";
		}
		ostream_ << "},{";
		//edges
		first = true;
		for (const auto& e : m_edges) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << '{' << e->m_type << ',' << e->m_ID + 1 << ',' << e->first->m_ID + 1
					 << "->" << e->second->m_ID + 1 << ',' << '{' << e->m_flavor << "}}";
			
		}
		ostream_ << "}}";
	}

	void graph::saveToStream(std::ostream& ostream_) const {
		cereal::XMLOutputArchive ar(ostream_);
		ar(cereal::make_nvp("graph", *this));
	}

	std::vector<std::vector<std::size_t> > graph::makeSymmetryGrp() const {
		auto g = *this;

		graph_canon::canonicalizer<std::size_t, graph::edgeHandlerCreator_t, true, true>
			canonicalizer(edgeHandlerCreator);
		auto can = canonicalizer(*this, m_indexMap, m_vertexLess, defaultVisitor);

		auto gen = graph_canon::get(graph_canon::aut_pruner_basic::result_t(), can.second)->generators();

		std::vector<std::vector<std::size_t> > ret;

		for (auto it = gen.begin(), end = gen.end(); it != end; ++it) {
			ret.push_back(std::move(*it));
		}

		return ret;
   }

	std::ostream& graph::printSymmetryGroup(std::ostream& ostream_) const {
		auto grp = makeSymmetryGrp();
		ostream_ << grp;
		return ostream_;
	}

	std::size_t graph::countMultiEdgeTadpoleSyms() const {
		std::size_t ret = 1;
		//multi-edges: they come after each other if canonicalized
		auto it = eBegin();
		const auto ee = eEnd();
		while (it != ee) {
			std::size_t counter = 1;
			auto it2 = it+1;
			for (; it2 != ee; ++it2) {
				if (graph::edge_t::compare(**it,**it2)==0 && (*it)->first == (*it2)->first
					&& (*it)->second == (*it2)->second) {
					//we don't want double tadpoles (for now, TODO?)
					assert((*it)->first != (*it)->second);
					++counter;
				} else {
					break;
				}
			}
			//multiply by counter! (factorial)
			while (counter != 1) {
				ret *= counter--;
			}
			//step
			it = it2;
		}

		//simple tadpoles
		it = eBegin();
		while (it != ee) {
			if ((*it)->first == (*it)->second) {
				ret *= 2;
			}
			//step
			++it;
		}

		return ret;
	}

	std::size_t graph::symmetryFactor() const {
		auto symGrp = makeSymmetryGrp();
		std::set<std::vector<std::size_t> > grpElements(symGrp.begin(), symGrp.end());
		//begin() is always the identity permutation
		symGrp.erase(symGrp.begin());

		//apply symGrp to all elements and check if more elements have been found
		auto currentSize = grpElements.size();
		while (true) {
			//make a copy
			auto grpElementsNew = grpElements;
			for (const auto& e : grpElements) {
				for (const auto& perm : symGrp) {
					grpElementsNew.insert(permuteHelper(e, perm));
				}
			}
			if (grpElements.size() == grpElementsNew.size()) {
				//don't forget the multi-edge symmetries
				return grpElements.size() * countMultiEdgeTadpoleSyms();
			}
			//else
			grpElements = std::move(grpElementsNew);
		}

		assert(false);
		return 0;
	}

	std::map<std::size_t, bool> graph::loopEdges() const {
		//start from external legs and recursively set all legs
		//to be non-loop legs if they are connected only to non-loop legs on one side
		std::map<std::size_t, bool> ret;
		for (auto& e : m_edges) {
			//set to true if not an external leg
			ret.insert(std::make_pair(e->m_ID, !(e->first->m_number >0 ||
												 e->second->m_number > 0)));
		}

		bool change = true;
		while (change) {
			change = false;
			for (auto& v : m_verts) {
				edge loopEdge = nullptr;
				bool tooMany = false;
				for (auto& e : *v) {
					if (ret[e->m_ID]) {
						if (loopEdge) {
							tooMany = true;
							break;
						}
						//else
						loopEdge = e;
					}
				}

				if (!tooMany && loopEdge) {
					ret[loopEdge->m_ID] = false;
					change = true;
				}
			}
		}

		return ret;
	}

	graphMap graph::symGrpGenToMap(const std::vector<std::size_t>& gen_) const {
		graphMap ret(gen_);
		//we want map from graph produced by the generator to *this
		ret.invert();

		//keep track of which edges we have already mapped onto
		std::vector<bool> alreadyMapped(m_edges.size(), false);
		for (auto& e : m_edges) {
			//find corresponding after applying the map
			auto v1 = (*this)[ret[e->first->m_ID]];
			auto v2ID = ret[e->second->m_ID];

#ifndef NDEBUG
			bool found = false;
#endif
			for (auto& ee : *v1) {
				//otherID and properties have to match
				if (ee->other(v1)->m_ID == v2ID && !alreadyMapped[ee->m_ID]
				    && graph::edge_t::compare(*e,*ee)==0) {
					ret.m_edges.push_back(std::make_pair(ee->m_ID,ee->first!=v1));
					alreadyMapped[ee->m_ID] = true;
#ifndef NDEBUG
					found = true;
#endif
					break;
				}
			}
#ifndef NDEBUG
			assert(found);
#endif			
		}

		ret.computeSign(*this, *this);

		return ret;
	}

	void graph::setExtUnity() {
		for (auto& v : m_verts) {
			if (v->m_number > 0) {
				v->m_number = 1;
			}
		}
	}

	void graph::permute(const permutation& perm_) {
		for (auto& v : m_verts) {
			if (v->m_number > 0) {
				auto it = perm_.find(v->m_number);
				if (it != perm_.end()) {
					v->m_number = it->second;
				}
				//else: don't change it
			}
		}
	}

	bool graph::containsSimpleTadpole() const {
		for (auto& e : m_edges) {
			auto vs = e->getActualVerts();
			if (vs.first == vs.second) {
				return true;
			}
		}
		return false;
	}

	bool graph::containsBubble() const {
		//TODO: more efficient: instead of checking all it2's,
		//      only check the connected edges?
		const auto end = m_edges.end();
		for (auto it1 = m_edges.begin(); it1 != end; ++it1) {
			if (!(*it1)->isActualEdge()) {
				continue;
			}
			for (auto it2 = it1+1; it2 != end; ++it2) {
				if (!(*it2)->isActualEdge()) {
					continue;
				}
				auto v1s = (*it1)->getActualVerts();
				auto v2s = (*it2)->getActualVerts();
				if (v1s == v2s || (v1s.first == v2s.second && v1s.second == v2s.first)) {
					return true;
				}
			}
		}
		return false;
	}

	bool graph::containsNull() const {
		for (auto& e : m_edges) {
			if (e->isActualEdge()) {
				auto vs = e->getActualVerts();
				//the first two conditions make sure that it's not an external edge
				if (vs.first->m_number <= 0 && vs.second->m_number <= 0 && e->isNull()) {
					return true;
				}
			}
		}
		return false;
	}

	bool graph::containsPureGluonLoop() const {
		for (auto& v : m_verts) {
			if (v->m_number != 0) {
				continue;
			}
			//TODO: check for bubbles
			//TODO: more efficient if we just run over gluon legs
			graphLoopRunner gLR(v.get());
			while (gLR.next()) {
				auto& stack = gLR.getEdgeStack();
				if (stack.size() <= 1) {
					continue;
				}
				//check if the current vertex connects back to starting vertex
				bool found = false;
				auto& vv = gLR.getCurrent();
				for (auto& e : *vv) {
					if (e->other(vv) == v.get() && e->m_type == line_t::GLUON) {
						found = true;
						break;
					}
				}
				if (found) {
					//check if every edge in the stack is a gluon
					found = false;
					for (auto& e : stack) {
						if (e->m_type != line_t::GLUON) {
							found = true;
							break;
						}
					}
					if (!found) {
						return true;
					}
				}
			}
		}
		
		return false;
	}

	bool graph::containsFlavor(const flavor_t& fl_) const {
		for (auto& e : m_edges) {
			if (e->m_flavor == fl_) {
				return true;
			}
		}
		return false;
	}

	bool graph::containsFlavor(const flLine_t& fl_) const {
		for (auto& e : m_edges) {
			if (e->m_type == fl_.first && e->m_flavor == fl_.second) {
				return true;
			}
		}
		return false;
	}

	void graph::permuteFlavors(const std::map<flavor_t, flavor_t>& map_) {
		for (auto& e : m_edges) {
			e->permuteFlavors(map_);
		}
	}

	std::vector<graph> graph::reverseQuarkLines() const {
		auto lines = getQuarkLines();
		
		std::vector<graph> ret;
		const auto size = lines.size();
		boolRunner r(size);
		//skip over identity
		while (r.next()) {
			auto current = r.getCurrent();
			//make a copy
			auto g = *this;
			for (std::size_t i = 0; i != size; ++i) {
				if (current[i]) {
					g.reverseQuarkLine(lines[i]);
				}
			}
			ret.push_back(std::move(g));
		}

		return ret;
	}

	std::vector<std::pair<graph, graphMap> > graph::reverseQuarkLines(
		const graphMap& map_) const {
		auto lines = getQuarkLines();

		std::vector<std::pair<graph, graphMap> > ret;
		const auto size = lines.size();
		boolRunner r(size);
		//skip over identity
		while (r.next()) {
			auto current = r.getCurrent();
			//make copies
			auto g = *this;
			auto map = map_;
			for (std::size_t i = 0; i != size; ++i) {
				if (current[i]) {
					g.reverseQuarkLine(lines[i], map);
				}
			}
			ret.push_back(std::make_pair(std::move(g), std::move(map)));
		}

		return ret;
	}

	graph graph::reverseAllQuarks() const {
		//make a copy
		graph g = *this;
		for (auto& ee : g.getEdges()) {
			edge e(ee.get(), true);
			if (e->m_type != line_t::QUARK || !e->isActualEdge()) {
				continue;
			}
			//swap line directions and edge m_dirs
			auto other = e->first->other(e);
			other->second->changeEdgeDir(other);
			other.m_dir = !other.m_dir;
			other->first->changeEdgeDir(other);
			std::swap(other->first, other->second);
			
			other = e->second->other(e);
			other->first->changeEdgeDir(other);
			other.m_dir = !other.m_dir;
			other->second->changeEdgeDir(other);
			std::swap(other->first, other->second);
			
			e->first->changeEdgeDir(e);
			e.m_dir = false;
			e->second->changeEdgeDir(e);
			std::swap(e->first, e->second);
			//swap fake vertex numbers
			e->first->m_number = -1;
			e->second->m_number = -2;
		}
		return g;
	}

	void graph::reverseQuarkLine(const std::vector<std::size_t>& edges_) {
		for (auto& id : edges_) {
			edge e(m_edges[id].get(),true);
			assert(e->isActualEdge());
			assert(e->m_type == line_t::QUARK);
			//swap line directions and edge m_dirs
			auto other = e->first->other(e);
			other->second->changeEdgeDir(other);
			other.m_dir = !other.m_dir;
			other->first->changeEdgeDir(other);
			std::swap(other->first, other->second);
			
			other = e->second->other(e);
			other->first->changeEdgeDir(other);
			other.m_dir = !other.m_dir;
			other->second->changeEdgeDir(other);
			std::swap(other->first, other->second);
			
			e->first->changeEdgeDir(e);
			e.m_dir = false;
			e->second->changeEdgeDir(e);
			std::swap(e->first, e->second);
			//swap fake vertex numbers
			e->first->m_number = -1;
			e->second->m_number = -2;
		}
	}

	void graph::reverseQuarkLine(const std::vector<std::size_t>& edges_,
								 graphMap& map_) {
		for (auto& id : edges_) {
			edge e(m_edges[id].get(), true);
			assert(e->isActualEdge());
			assert(e->m_type == line_t::QUARK);
			//swap line directions
			auto other = e->first->other(e);
			other->second->changeEdgeDir(other);
			other.m_dir = !other.m_dir;
			other->first->changeEdgeDir(other);
			std::swap(other->first, other->second);
			auto& lookup1 = map_(other->m_ID).second;
			lookup1 = !lookup1;

			other = e->second->other(e);
			other->first->changeEdgeDir(other);
			other.m_dir = !other.m_dir;
			other->second->changeEdgeDir(other);
			std::swap(other->first, other->second);
			auto& lookup2 = map_(other->m_ID).second;
			lookup2 = !lookup2;

			e->first->changeEdgeDir(e);
			e.m_dir = false;
			e->second->changeEdgeDir(e);
			std::swap(e->first, e->second);
			auto& lookup3 = map_(e->m_ID).second;
			lookup3 = !lookup3;
			//swap fake vertex numbers
			e->first->m_number = -1;
			e->second->m_number = -2;
		}
	}

	void graph::swapEdges(const std::size_t& vertID_, const std::size_t& edgeID1_, const bool& edgeDir1_,
						  const std::size_t& edgeID2_, const bool& edgeDir2_) {
		assert(vertID_ < nVerts());
		assert(edgeID1_ < nEdges() && edgeID2_ < nEdges());
		auto v = (*this)[vertID_];
		const edge e1(getEdges()[edgeID1_].get(), edgeDir1_);
		const edge e2(getEdges()[edgeID2_].get(), edgeDir2_);
		v->swapEdges(e1, e2);
	}

	graph::vert graph::addVertex(const vert_t& vert_) {
		m_verts.emplace_back(new vert_t(vert_));
		auto& ret = m_verts.back();
		ret->m_ID = m_verts.size() - 1;
		return ret.get();
	}

	graph::edge graph::addEdge(const edge_t& edge_, vert& from_, vert& to_) {
		m_edges.emplace_back(new edge_t(edge_));
		auto ret = m_edges.back().get();
		ret->m_ID = m_edges.size() - 1;

		//connect
		if (isDirected(edge_.m_type)) {
			//introduce helping vertices and edges
			m_edges.emplace_back(new edge_t(edge_));
			auto e1 = m_edges.back().get();
			e1->m_ID = m_edges.size() - 1;
			m_edges.emplace_back(new edge_t(edge_));
			auto e2 = m_edges.back().get();
			e2->m_ID = m_edges.size() - 1;
			auto v1 = addVertex(vert_t(-1));
			auto v2 = addVertex(vert_t(-2));

			e1->first = from_;
			from_->push_back(edge(e1, true));
			e1->second = v1;
			v1->push_back(edge(e1, false));
			ret->first = v1;
			v1->push_back(edge(ret, true));
			ret->second = v2;
			v2->push_back(edge(ret, false));
			e2->first = v2;
			v2->push_back(edge(e2, true));
			e2->second = to_;
			to_->push_back(edge(e2, false));
		} else {
			ret->first = from_;
			from_->push_back(edge(ret, true));
			ret->second = to_;
			to_->push_back(edge(ret, false));
		}

		return ret;
	}

	graph::edge graph::addEdge(const edge_t& edge_, vert& from_, vert& to_,
							   vert_t::iterator& itFrom_, vert_t::iterator& itTo_) {
		m_edges.emplace_back(new edge_t(edge_));
		auto ret = m_edges.back().get();
		ret->m_ID = m_edges.size() - 1;

		//connect
		if (isDirected(edge_.m_type)) {
			//introduce helping vertices and edges
			m_edges.emplace_back(new edge_t(edge_));
			auto e1 = m_edges.back().get();
			e1->m_ID = m_edges.size() - 1;
			m_edges.emplace_back(new edge_t(edge_));
			auto e2 = m_edges.back().get();
			e2->m_ID = m_edges.size() - 1;
			auto v1 = addVertex(vert_t(-1));
			auto v2 = addVertex(vert_t(-2));

			e1->first = from_;
			//from_->push_back(e1);
			itFrom_ = from_->insert(itFrom_, edge(e1, true));
			e1->second = v1;
			v1->push_back(edge(e1, false));
			ret->first = v1;
			v1->push_back(edge(ret, true));
			ret->second = v2;
			v2->push_back(edge(ret, false));
			e2->first = v2;
			v2->push_back(edge(e2, true));
			e2->second = to_;
			//to_->push_back(e2);
			itTo_ = to_->insert(itTo_, edge(e2, false));
		} else {
			ret->first = from_;
			//from_->push_back(ret);
			itFrom_ = from_->insert(itFrom_, edge(ret, true));
			ret->second = to_;
			//to_->push_back(ret);
			itTo_ = to_->insert(itTo_, edge(ret, false));
		}

		return ret;
	}

	graph::edge graph::replaceEdge(const edge_t& edge_, vert& from_, vert& to_,
								   edge repl_) {
		if (isDirected(repl_->m_type)) {
			//disconnect old edges
			assert(repl_->isActualEdge());
			auto eMiddle = repl_;
			auto v1 = eMiddle->first;
			auto v2 = eMiddle->second;
			auto e1 = v1->other(eMiddle);
			auto e2 = v2->other(eMiddle);

			bool as;
			e1.m_dir = true;
			as = e1->first->removeEdge(e1);
			assert(as);
			e2.m_dir = false;
			as = e2->second->removeEdge(e2);
			assert(as);

			//assign new properties
			eMiddle->m_type = edge_.m_type;
			eMiddle->m_flavor = edge_.m_flavor;
			if (isDirected(edge_.m_type)) {
				//assign new properties also for helpers
				e1->m_type = edge_.m_type;
				e1->m_flavor = edge_.m_flavor;
				e2->m_type = edge_.m_type;
				e2->m_flavor = edge_.m_flavor;

				//connect
				e1->first = from_;
				from_->push_back(e1);//still has correct m_dir
				e2->second = to_;
				to_->push_back(e2);//still has correct m_dir
			} else {
				//completely erase helpers
				//vertices
				auto idSmaller = (v1->m_ID < v2->m_ID) ? v1->m_ID : v2->m_ID;
				auto idLarger = (v1->m_ID < v2->m_ID) ? v2->m_ID : v1->m_ID;
				m_verts.erase(m_verts.begin() + idLarger);
				m_verts.erase(m_verts.begin() + idSmaller);
				//reset the ids
				for (auto size = m_verts.size(); idSmaller != size; ++idSmaller) {
					m_verts[idSmaller]->m_ID = idSmaller;
				}

				//edges
				idSmaller = (e1->m_ID < e2->m_ID) ? e1->m_ID : e2->m_ID;
				idLarger = (e1->m_ID < e2->m_ID) ? e2->m_ID : e1->m_ID;
				m_edges.erase(m_edges.begin() + idLarger);
				m_edges.erase(m_edges.begin() + idSmaller);
				//reset the ids
				for (auto size = m_edges.size(); idSmaller != size; ++idSmaller) {
					m_edges[idSmaller]->m_ID = idSmaller;
				}

				//finally connect
				eMiddle->first = from_;
				eMiddle.m_dir = true;
				from_->push_back(eMiddle);
				eMiddle->second = to_;
				eMiddle.m_dir = false;
				to_->push_back(eMiddle);
			}
			return eMiddle;
		}
		//else
		//disconnect old edge
		bool as;
		repl_.m_dir = true;
		as = repl_->first->removeEdge(repl_);
		assert(as);
		repl_.m_dir = false;
		as = repl_->second->removeEdge(repl_);
		assert(as);

		//assign new properties
		repl_->m_type = edge_.m_type;
		repl_->m_flavor = edge_.m_flavor;
		//connect
		if (isDirected(edge_.m_type)) {
			//introduce helping vertices and edges
			m_edges.emplace_back(new edge_t(edge_));
			auto e1 = m_edges.back().get();
			e1->m_ID = m_edges.size() - 1;
			m_edges.emplace_back(new edge_t(edge_));
			auto e2 = m_edges.back().get();
			e2->m_ID = m_edges.size() - 1;
			auto v1 = addVertex(vert_t(-1));
			auto v2 = addVertex(vert_t(-2));

			e1->first = from_;
			from_->push_back(edge(e1, true));
			e1->second = v1;
			v1->push_back(edge(e1, false));
			repl_->first = v1;
			repl_.m_dir = true;
			v1->push_back(repl_);
			repl_->second = v2;
			repl_.m_dir = false;
			v2->push_back(repl_);
			e2->first = v2;
			v2->push_back(edge(e2, true));
			e2->second = to_;
			to_->push_back(edge(e2, false));
		} else {
			repl_->first = from_;
			repl_.m_dir = true;
			from_->push_back(repl_);

			repl_->second = to_;
			repl_.m_dir = false;
			to_->push_back(repl_);
		}

		return repl_;
	}

	graph::edge graph::replaceEdge(const edge_t& edge_, edge repl_, const bool& invert_) {
		if (isDirected(repl_->m_type)) {
			//get stuff
			assert(repl_->isActualEdge());
			auto eMiddle = repl_;
			auto v1 = eMiddle->first;
			auto v2 = eMiddle->second;
			auto e1 = v1->other(eMiddle);
			auto e2 = v2->other(eMiddle);
			auto from = e1->other(v1);
			auto to = e2->other(v2);
			e1.m_dir = true;
			e2.m_dir = false;

			//assign new properties
			eMiddle->m_type = edge_.m_type;
			eMiddle->m_flavor = edge_.m_flavor;
			if (isDirected(edge_.m_type)) {
				//assign new properties also for helpers
				e1->m_type = edge_.m_type;
				e1->m_flavor = edge_.m_flavor;
				e2->m_type = edge_.m_type;
				e2->m_flavor = edge_.m_flavor;

				//reconnect
				if (invert_) {
					if (to == from) {
						//special tadpoles case
						from->replaceEdges(e1, e2, e2, e1);
					} else {
						e1->first = to;
						from->replaceEdge(e1,e2);
			   			e2->second = from;
						to->replaceEdge(e2, e1);
					}
				}
			} else {
				//reconnect
				if (!invert_) {
					eMiddle->first = from;
					eMiddle.m_dir = true;
					from->replaceEdge(e1, eMiddle);
					eMiddle->second = to;
					eMiddle.m_dir = false;
					to->replaceEdge(e2, eMiddle);
				} else {
					eMiddle->first = to;
					eMiddle.m_dir = true;
					to->replaceEdge(e2, eMiddle);
					eMiddle->second = from;
					eMiddle.m_dir = false;
					from->replaceEdge(e1, eMiddle);
				}
				
				//completely erase helpers
				//vertices
				auto idSmaller = (v1->m_ID < v2->m_ID) ? v1->m_ID : v2->m_ID;
				auto idLarger = (v1->m_ID < v2->m_ID) ? v2->m_ID : v1->m_ID;
				m_verts.erase(m_verts.begin() + idLarger);
				m_verts.erase(m_verts.begin() + idSmaller);
				//reset the ids
				for (auto size = m_verts.size(); idSmaller != size; ++idSmaller) {
					m_verts[idSmaller]->m_ID = idSmaller;
				}

				//edges
				idSmaller = (e1->m_ID < e2->m_ID) ? e1->m_ID : e2->m_ID;
				idLarger = (e1->m_ID < e2->m_ID) ? e2->m_ID : e1->m_ID;
				m_edges.erase(m_edges.begin() + idLarger);
				m_edges.erase(m_edges.begin() + idSmaller);
				//reset the ids
				for (auto size = m_edges.size(); idSmaller != size; ++idSmaller) {
					m_edges[idSmaller]->m_ID = idSmaller;
				}
			}
			return eMiddle;
		}
		//else
		//disconnect old edge
		auto from = repl_->first;
		auto to = repl_->second;

		//assign new properties
		repl_->m_type = edge_.m_type;
		repl_->m_flavor = edge_.m_flavor;
		//connect
		if (isDirected(edge_.m_type)) {
			//introduce helping vertices and edges
			m_edges.emplace_back(new edge_t(edge_));
			auto e1 = m_edges.back().get();
			e1->m_ID = m_edges.size() - 1;
			m_edges.emplace_back(new edge_t(edge_));
			auto e2 = m_edges.back().get();
			e2->m_ID = m_edges.size() - 1;
			auto v1 = addVertex(vert_t(-1));
			auto v2 = addVertex(vert_t(-2));

			//reconnect
			if (!invert_) {
				e1->first = from;
				repl_.m_dir = true;
				from->replaceEdge(repl_, edge(e1, true));
			} else {
				e1->first = to;
				repl_.m_dir = false;
				to->replaceEdge(repl_, edge(e1, true));
			}
			e1->second = v1;
			v1->push_back(edge(e1, false));
			repl_->first = v1;
			repl_.m_dir = true;
			v1->push_back(repl_);
			repl_->second = v2;
			repl_.m_dir = false;
			v2->push_back(repl_);
			e2->first = v2;
			v2->push_back(edge(e2, true));
			if (!invert_) {
				e2->second = to;
				repl_.m_dir = false;
				to->replaceEdge(repl_, edge(e2, false));
			} else {
				e2->second = from;
				repl_.m_dir = true;
				from->replaceEdge(repl_, edge(e2, false));
			}
		} else {
			if (invert_ && from == to) {
				//special case of a tadpole
#ifndef NDEBUG
				unsigned int counter = 0;
#endif
				for (auto& e : *from) {
					if (e == repl_) {
						e.m_dir = !e.m_dir;
#ifndef NDEBUG
						++counter;
#endif
					}
				}
				assert(counter == 2);
			} else if (invert_) {
				repl_->first = to;
				repl_.m_dir = true;
				auto copy = repl_;
				copy.m_dir = false;
				to->replaceEdge(copy, repl_);

				repl_->second = from;
				repl_.m_dir = false;
				copy.m_dir = true;
				from->replaceEdge(copy, repl_);
			}
		}

		return repl_;
	}

	void graph::removeVertex(const vert& v_) {
		auto id = v_->m_ID;
		//remove from vector
		m_verts.erase(m_verts.begin() + id);
		//reset IDs
		for (const auto size = m_verts.size(); id != size; ++id) {
			m_verts[id]->m_ID = id;
		}
	}

	void graph::removeVertex(std::size_t id_) {
		//remove from vector
		m_verts.erase(m_verts.begin() + id_);
		//reset IDs
		for (const auto size = m_verts.size(); id_ != size; ++id_) {
			m_verts[id_]->m_ID = id_;
		}
	}

	std::vector<std::unique_ptr<graph::vert_t> >::iterator graph::removeVertex(
		const std::vector<std::unique_ptr<vert_t> >::const_iterator& v_) {
		auto id = (*v_)->m_ID;
		//remove from vector
		auto ret = m_verts.erase(v_);
		//reset IDs
		for (const auto size = m_verts.size(); id != size; ++id) {
			m_verts[id]->m_ID = id;
		}
		return ret;
	}

	void graph::removeEdge(const edge& e_) {
		auto id = e_->m_ID;
		//remove from vector
		m_edges.erase(m_edges.begin() + id);
		//reset IDs
		for (const auto size = m_edges.size(); id != size; ++id) {
			m_edges[id]->m_ID = id;
		}
	}

	void graph::removeEdge(std::size_t id_) {
		//remove from vector
		m_edges.erase(m_edges.begin() + id_);
		//reset IDs
		for (const auto size = m_edges.size(); id_ != size; ++id_) {
			m_edges[id_]->m_ID = id_;
		}
	}

	std::set<graph> graph::addEFTGraviton() const {
		std::set<graph> ret;
		//find scalar leg
		for (auto& e1 : m_edges) {
			if (e1->m_type == line_t::SCALAR) {
				//find other edge
				for (auto& e2 : m_edges) {
					if (e2->m_type != line_t::SCALAR) {
						auto tmp = addEFTGraviton(e1.get(), e2.get());
						ret.insert(std::move(tmp));
					}
				}
				//find other vertex
				for (auto& v : m_verts) {
					if (v->m_number != 0 || e1->first == v.get()
						|| e1->second == v.get()) {
						continue;
					}
					//check if connected to the same scalar line
					bool found = false;
					for (auto& ee : *v) {
						if (ee->m_type == line_t::SCALAR &&
							ee->m_flavor == e1->m_flavor) {
							found = true;
							break;
						}
					}
					if (!found) {
						auto tmp = addEFTGraviton(e1.get(), v.get());
						ret.insert(std::move(tmp));
					}
				}
			}
		}
		return ret;
	}

	graph graph::addEFTGraviton(const edge& e1_, const edge& e2_) const {
		assert(!isDirected(e1_->m_type) && !isDirected(e2_->m_type));
		//make a copy
		auto g = *this;
	    edge e1(g.m_edges[e1_->m_ID].get(), false);
		auto v1Target = e1->second;
		//disconnect
		v1Target->removeEdge(e1);
		//new edge and vertex, and recconnect
		auto v1Middle = g.addVertex(vert_t(0));
		v1Middle->push_back(e1);
		e1->second = v1Middle;
		g.addEdge(edge_t(e1->m_flavor, e1->m_type), v1Middle, v1Target);
		
		
		edge e2(g.m_edges[e2_->m_ID].get(), false);
		auto v2Target = e2->second;
		//disconnect
		v2Target->removeEdge(e2);
		//new edge and vertex, and reconnect
		auto v2Middle = g.addVertex(vert_t(0));
		v2Middle->push_back(e2);
		e2->second = v2Middle;
		g.addEdge(edge_t(e2->m_flavor, e2->m_type), v2Middle, v2Target);

		//new graviton edge
		g.addEdge(edge_t(-1, line_t::GLUON), v1Middle, v2Middle);
		g.canonicalize();
		return g;
	}
	
	graph graph::addEFTGraviton(const edge& e_, const vert& v_) const {
		assert(!isDirected(e_->m_type));
		//make a copy
		auto g = *this;
	    edge e(g.m_edges[e_->m_ID].get(), false);
		auto vTarget = e->second;
		//disconnect
		vTarget->removeEdge(e);
		//new edge and vertex, and recconnect
		auto vMiddle = g.addVertex(vert_t(0));
		vMiddle->push_back(e);
		e->second = vMiddle;
		g.addEdge(edge_t(e->m_flavor, e->m_type), vMiddle, vTarget);

		//new graviton edge
		auto v = g[v_->m_ID];
		g.addEdge(edge_t(-1, line_t::GLUON), vMiddle, v);
		g.canonicalize();
		return g;
	}

	std::set<graph> graph::addPMEFTBH(const std::size_t& number_, const bool& higherWLCoupling_) const {
		std::set<graph> gs;
		//from all vertices to BH
		for (const auto& v : getVerts()) {
			if (!higherWLCoupling_ && v->m_number > 0 && !v->empty()) {
				continue; 
			}
			//make a copy
			auto g = *this;
			auto v1 = g[v->m_ID];
			auto v2 = g.addVertex(number_);
			g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v2, v1);
			g.canonicalize();
			gs.insert(std::move(g));
		}

	    //from all edges to BH
		for (const auto& e : getEdges()) {
			//make a copy
			auto g = *this;
			auto e1 = g.m_edges[e->m_ID].get();
			//add new middle vertex
			auto v0= e1->first;
			v0->removeEdge(e1);
			auto v1 = g.addVertex(0);
			e1->first = v1;
			v1->push_back(e1);
			g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v0, v1);
			//add new out BH vertex
			auto v2 = g.addVertex(number_);
			g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v2, v1);
			g.canonicalize();
			gs.insert(std::move(g));
		}

		return gs;
	}

	std::set<graph> graph::addPMEFTGrav(const std::size_t& number_, const bool& higherWLCoupling_) const {
		std::set<graph> gs;
		//from all vertices to ext grav
		for (const auto& v : getVerts()) {
			if ((!higherWLCoupling_ && v->m_number > 0 && !v->empty()) || v->m_number < 0) {
				continue; 
			}
			//make a copy
			auto g = *this;
			auto v1 = g[v->m_ID];
			auto v2 = g.addVertex(number_);
			g.addEdge(edge_t(flavor_t(0), line_t::QUARK), v2, v1);
			g.canonicalize();
			gs.insert(std::move(g));
		}

	    //from all non-fake edges to ext grav
		for (const auto& e : getEdges()) {
			if (!e->isActualEdge()) {
				continue;
			}
			//make a copy
			auto g = *this;
			edge e1 = g.m_edges[e->m_ID].get();
			assert(e1->m_type == line_t::QUARK);
			e1 = e1->first->other(e1);
			e1.m_dir = true;
			//add new middle vertex
			auto v0= e1->first;
			v0->removeEdge(e1);
			auto v1 = g.addVertex(0);
			e1->first = v1;
			v1->push_back(e1);
			g.addEdge(edge_t(flavor_t(0), line_t::QUARK), v0, v1);
			//add new out grav vertex
			auto v2 = g.addVertex(number_);
			g.addEdge(edge_t(flavor_t(0), line_t::QUARK), v2, v1);
			g.canonicalize();
			gs.insert(std::move(g));
		}

		return gs;
	}

	std::set<graph> graph::addGRWLGraviton() const {
		std::set<graph> gs;

		//insert in all vertices
		for (const auto& v : getVerts()) {
			if (v->m_number == 0) {
				//it's a purely gluonic vertex
				//make a copy and add gluon leg towards first BH
				auto g = *this;
				auto v1 = g[v->m_ID];
				auto v2 = g.addVertex(1);
				g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v2, v1);
				g.canonicalize();
				gs.insert(std::move(g));

				//make a copy and add gluon leg towards second BH
				g = *this;
				v1 = g[v->m_ID];
				v2 = g.addVertex(2);
				g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v2, v1);
				g.canonicalize();
				gs.insert(std::move(g));
			}
		}

		//insert in all gluon edges
		for (const auto& e : getEdges()) {
			if (e->m_type == line_t::GLUON) {
				//it's a gluon edge
				//make a copy and add gluon leg towards first BH
				auto g = *this;
				edge e1 = g.m_edges[e->m_ID].get();
				e1.m_dir = true;
				auto v0 = e1->first;
				v0->removeEdge(e1);
				auto v1 = g.addVertex(0);
				e1->first = v1;
				v1->push_back(e1);

				g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v0, v1);
				auto v2 = g.addVertex(1);
				g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v2, v1);
				g.canonicalize();
				gs.insert(std::move(g));

				//make a copy and add gluon leg towards first BH
				g = *this;
				e1 = g.m_edges[e->m_ID].get();
				e1.m_dir = true;
				v0 = e1->first;
				v0->removeEdge(e1);
				v1 = g.addVertex(0);
				e1->first = v1;
				v1->push_back(e1);

				g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v0, v1);
				v2 = g.addVertex(2);
				g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v2, v1);
				g.canonicalize();
				gs.insert(std::move(g));
			}
		}

		return gs;
	}

	std::set<graph> graph::addGRWLTree() const {
		std::set<graph> gs;

		//insert in all vertices
		for (const auto& v : getVerts()) {
			if (v->m_number == 1) {
				//make a copy and insert the new objects
				auto g = *this;
				auto v1 = g[v->m_ID];
				auto v2 = g.addVertex(1);
				auto v3 = g.addVertex(2);

				g.addEdge(edge_t(flavor_t(0), line_t::QUARK), v2, v1);
				g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v3, v2);
				g.canonicalize();
				gs.insert(g);
			}

			if (v->m_number == 2) {
				//make a copy and insert the new objects
				auto g = *this;
				auto v1 = g[v->m_ID];
				auto v2 = g.addVertex(2);
				auto v3 = g.addVertex(1);

				g.addEdge(edge_t(flavor_t(0), line_t::QUARK), v2, v1);
				g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v3, v2);
				g.canonicalize();
				gs.insert(g);
			}
		}

		return gs;
	}

	std::set<graph> graph::addGRWLTreeUndir() const {
		std::set<graph> gs;

		//insert in all vertices
		for (const auto& v : getVerts()) {
			if (v->m_number == 1) {
				//make a copy and insert the new objects
				auto g = *this;
				auto v1 = g[v->m_ID];
				auto v2 = g.addVertex(1);
				auto v3 = g.addVertex(2);

				g.addEdge(edge_t(flavor_t(0), line_t::SCALAR), v2, v1);
				g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v3, v2);
				g.canonicalize();
				gs.insert(g);
			}

			if (v->m_number == 2) {
				//make a copy and insert the new objects
				auto g = *this;
				auto v1 = g[v->m_ID];
				auto v2 = g.addVertex(2);
				auto v3 = g.addVertex(1);

				g.addEdge(edge_t(flavor_t(0), line_t::SCALAR), v2, v1);
				g.addEdge(edge_t(flavor_t(-1), line_t::GLUON), v3, v2);
				g.canonicalize();
				gs.insert(g);
			}
		}

		return gs;
	}

	std::vector<std::size_t> graph::permuteHelper(const std::vector<std::size_t>& vec_,
												  const std::vector<std::size_t>& perm_) {
		assert(vec_.size() == perm_.size());
		std::vector<std::size_t> ret;
		ret.reserve(vec_.size());
		for (const auto& e : perm_) {
			ret.push_back(vec_[e]);
		}
		return ret;
	}

	std::vector<std::unique_ptr<graph::edge_t> >::iterator graph::removeEdge(
		const std::vector<std::unique_ptr<edge_t> >::const_iterator& e_) {
		auto id = (*e_)->m_ID;
		//remove from vector
		auto ret = m_edges.erase(e_);
		//reset IDs
		for (const auto size = m_edges.size(); id != size; ++id) {
			m_edges[id]->m_ID = id;
		}
		return ret;
	}

	bool graph::edgeLess::operator()(const edge& left_, const edge& right_) {
		//compare properties
		auto compProp = edge_t::compare(*left_, *right_);
		if (compProp < 0) {
			return true;
		} else if (compProp > 0) {
			return false;
		}

		//compare connected vertices
		auto& l1 = left_->first->m_ID;
		auto& r1 = right_->first->m_ID;
		if (l1 < r1) {
			return true;
		} else if (r1 < l1) {
			return false;
		}
		auto& l2 = left_->second->m_ID;
		auto& r2 = right_->second->m_ID;
		if (l2 < r2) {
			return true;
		}
		return false;
	}

    bool graph::extLessOrdered::operator()(const vert& left_, const vert& right_) {
		canonGraphRunner gR1(left_);
		canonGraphRunner gR2(right_);

		do {
			assert(gR1.isRunning() && gR2.isRunning());

			//compare vertex properties (and size)
			if (lessSize(*gR1.getCurrent(), *gR2.getCurrent())) {
				return true;
			} else if (lessSize(*gR2.getCurrent(), *gR1.getCurrent())) {
				return false;
			}

			//compare next edge properties
			const auto comp = graph::edge_t::compare(*gR1.nextEdge(), *gR1.nextEdge());
			if (comp == -1) {
				return true;
			}
			if (comp == 1) {
				return false;
			}
			assert(comp == 0);

			//step
			gR2.next();
		} while (gR1.next());

		//they are equal in every sense (we actually discovered an external symmetry)
		return false;
	}

	graph::edgeHandlerCreator_t graph::edgeHandlerCreator;

	graph_canon::compound_visitor<
		graph_canon::target_cell_flm, graph_canon::traversal_bfs_exp, graph_canon::refine_WL_1,
		graph_canon::aut_pruner_basic, graph_canon::invariant_cell_split,
		graph_canon::invariant_partial_leaf, graph_canon::invariant_quotient>
	graph::defaultVisitor = graph_canon::make_visitor(
		graph_canon::target_cell_flm(), graph_canon::traversal_bfs_exp(),
		graph_canon::refine_WL_1(),	graph_canon::aut_pruner_basic(),
		//graph_canon::aut_implicit_size_2(),
		graph_canon::invariant_cell_split(), graph_canon::invariant_partial_leaf(),
		graph_canon::invariant_quotient());
}
