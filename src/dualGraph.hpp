#ifndef GKA_DUALGRAPH_HPP
#define GKA_DUALGRAPH_HPP

#include "ggraph_config.h"

#include <iostream>
#include <vector>
#include <memory>
#include <cereal/access.hpp>
#include <cereal/types/vector.hpp>

#include "graphs.hpp"

namespace ggraph {
	enum class dualLine_t : int {GLUON,QUARK,ANTIQUARK,UQUARK,SCALAR,ANTISCALAR,USCALAR};
	std::ostream& operator<<(std::ostream& stream_, const dualLine_t& type_);

	dualLine_t conjugate(const dualLine_t& dualLine_t);

	class dualGraph {
		friend class dualGraphs;
	public:
		//forward declarations
		struct vert_t;
		typedef vert_t* vert;
		
		struct edge_t : public std::pair<vert, vert> {
			//**************************************************
			// constructors
			//**************************************************
			edge_t() {}
			edge_t(const flavor_t& flavor_, const dualLine_t& type_)
				: m_flavor(flavor_), m_type(type_) {}
			//does not copy the verts
			edge_t(const edge_t& other_)
				: m_ID(other_.m_ID), m_flavor(other_.m_flavor), m_type(other_.m_type) {}

			//**************************************************
			// operators
			//**************************************************
			//compares properties (including m_ID and connected vert IDs)
			friend bool operator==(const edge_t& left_, const edge_t& right_);
			friend bool operator!=(const edge_t& left_, const edge_t& right_) {
				return !(left_ == right_);
			}
			friend bool operator<(const edge_t& left_, const edge_t& right);

			//**************************************************
			// methods
			//**************************************************
			vert& other(const vert v_) {
				if (first == v_) {
					return second;
				} else {
					assert(second == v_);
					return first;
				}
			}

			//compare properties (excluding m_ID, direction and connected vert IDs)
			static int compare(const edge_t& e1_, const edge_t& e2_);

			//**************************************************
			//members
			//**************************************************
			std::size_t m_ID;
			flavor_t m_flavor;
			dualLine_t m_type;
			//is to be interpreted that the graph line for directed types flows
			//from the left side of the dualLine to the right side of the dualLine

			//**************************************************
			// serialization
			//**************************************************
			template <typename Archive>
			void serialize(Archive& ar_) {
				ar_(cereal::make_nvp<std::size_t>("id", m_ID),
					cereal::make_nvp<flavor_t>("fl", m_flavor),
					cereal::make_nvp<dualLine_t>("t", m_type)
					);
			}
		};

		typedef edge_t* edge;

		struct vert_t : public std::vector<edge> {
			//**************************************************
			// constructors
			//**************************************************
			vert_t(const std::vector<std::size_t>& exts_ = std::vector<size_t>())
				: m_exts(exts_) {}
			//does not copy the edges
			vert_t(const vert_t& other_) : m_ID(other_.m_ID), m_exts(other_.m_exts) {}

			//**************************************************
			// operators
			//**************************************************
			//compares properties (including m_ID) and ID's of connected edges
			friend bool operator==(const vert_t& left_, const vert_t& right_);
			friend bool operator!=(const vert_t& left_, const vert_t& right_) {
				return !(left_ == right_);
			}
			friend bool operator<(const vert_t& left_, const vert_t& right_);

			//**************************************************
			// methods
			//**************************************************
			//compares properties (excluding m_ID)
			friend bool less(const vert_t& left_, const vert_t& right_) {
				//inverse ordering (bigger ext-vectors first)
				return left_.m_exts > right_.m_exts;
			}

			/**
			 * tries to find and remove the given edge (a single time!)
			 * return true if succeed
			 */
			bool removeEdge(const edge& e_);

			/**
			 * tries to find the edge that comes cyclically after the given edge
			 * returns nullptr if the given edge is not found
			 */
			edge next(const edge& e_);
			const edge next(const edge& e_) const;

			/**
			 * canonically rotate the exts
			 */
			void canonicalize();

			//**************************************************
			// members
			//**************************************************
			std::size_t m_ID;
			/**
			 * stores the numbers of the exts
			 */
			std::vector<size_t> m_exts;

			//**************************************************
			// serialization
			//**************************************************
			template <typename Archive>
			void serialize(Archive& ar_) {
				ar_(cereal::make_nvp<std::size_t>("id",m_ID),
					cereal::make_nvp<std::size_t>("es",m_exts));
			}
		};

		//**************************************************
		// constructors
		//**************************************************
		dualGraph() {};

		//**************************************************
		// operators
		//**************************************************
		vert operator[](const size_t& id_) const {
			return m_verts[id_].get();
		}

		vert operator[](const size_t& id_) {
			return m_verts[id_].get();
		}

		friend bool operator==(const dualGraph& left_, const dualGraph& right_);
		friend bool operator!=(const dualGraph& left_, const dualGraph& right_) {
			return !(left_ == right_);
		}

		friend bool operator<(const dualGraph& left_, const dualGraph& right_);

		//**************************************************
		// methods
		//**************************************************
		std::size_t nVerts() const {
			return m_verts.size();
		}
		std::size_t nEdges() const {
			return m_edges.size();
		}

		const std::vector<std::unique_ptr<vert_t> >& getVerts() const {
			return m_verts;
		}

		const std::vector<std::unique_ptr<edge_t> >& getEdges() const {
			return m_edges;
		}

		void canonicalize();

	private:
		//**************************************************
		// members
		//**************************************************
		std::vector<std::unique_ptr<vert_t> > m_verts;
		std::vector<std::unique_ptr<edge_t> > m_edges;

		//**************************************************
		// methods
		//**************************************************
		vert addVertex(const vert_t& vert_);

		/**
		 * from_ and to_ ar allowed to be nullptrs, then nothing is connected
		 */
		edge addEdge(const edge_t& edge_, const vert& from_ = nullptr,
					 const vert& to_ = nullptr);

		//**************************************************
		// static
		//**************************************************
		/**
		 * compare the ID of connected vertices by given edge
		 */
		struct adjVertLess {
			adjVertLess(const vert& v_) : m_v(v_) {}

			bool operator()(const edge& left_, const edge& right_);

			const vert& m_v;
		};
	};
}

#endif /*GKA_DUALGRAPH_HPP*/
