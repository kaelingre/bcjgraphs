#include "ggraph_config.h"

#include <iostream>
#include <fstream>

#include "graphLabelling.hpp"
#include "cut.hpp"
#include "eqns.hpp"
#include "utility/integerPartRunner.hpp"
#include "graphRunner.hpp"

using namespace ggraph;

int main(int argc, char* argv[]) {
	//just some checks
	BOOST_CONCEPT_ASSERT(( boost::GraphConcept<graph> ));
	BOOST_CONCEPT_ASSERT(( boost::IncidenceGraphConcept<graph> ));
	BOOST_CONCEPT_ASSERT(( boost::AdjacencyGraphConcept<graph> ));
	BOOST_CONCEPT_ASSERT(( boost::VertexListGraphConcept<graph> ));
	BOOST_CONCEPT_ASSERT(( boost::EdgeListGraphConcept<graph> ));

    flExt gluon(ext_t::GLUON, -1);
	coupling c1({gluon,gluon,gluon});
	coupling c2({gluon,gluon,gluon});

	labelledGraphs gs({gluon,gluon,gluon,gluon}, 1, {c1,c2});

	std::cout << gs << std::endl;

	gs.diffExts();
	std::cout << gs << std::endl;

	return 0;
}
