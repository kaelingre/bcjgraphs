#ifndef GKA_GRAPHLOOPRUNNER_HPP
#define GKA_GRAPHLOOPRUNNER_HPP

#include "ggraph_config.h"
#include "utility/runner.hpp"
#include "graph.hpp"

namespace ggraph {
	/**
	 * depth-first-search runner over vertices
	 * but only verts in the current stack and externals are considered back edges
	 * also keeps track of an edge stack/path that can be accessed
	 */
	class graphLoopRunner : public runner<graph::vert> {
	public:
		//**************************************************
		// constructor
		//**************************************************
		//this runner searches in all directions starting from given vertex
		graphLoopRunner(const graph::vert& v_);

		//**************************************************
		// methods
		//**************************************************
		graph::vert const& getCurrent() const override {
			return m_path.back().first;
		}

		graph::edge nextEdge() const {
			if (m_path.empty()) {
				return graph::edge(nullptr, true);
			}
			//else
			return (*m_path.back().first)[m_path.back().second];
		}

		const std::vector<graph::edge>& getEdgeStack() const {
			return m_edgePath;
		}
		
		bool next() override;
		
	private:
		//**************************************************
		// members
		//**************************************************
		//pairs of vertex and at which edge we turned
		std::vector<std::pair<graph::vert, std::size_t> > m_path;
		std::vector<graph::edge> m_edgePath;
	};
}

#endif /*GKA_GRAPHLOOPRUNNER_HPP*/
