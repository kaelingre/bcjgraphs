#include "eqns.hpp"

#include <cassert>
#include "utility/orderedChooseRunner.hpp"
#include "utility/rangeRunner.hpp"

namespace ggraph {
	term::term(labelledGraph g_, const labelledGraphs& gs_, bool& sign_,
			   const std::string& head_)
		: m_labelling(std::move(g_.getLabelling())) {
		sign_ = canonLabellingHelper(g_.getGraph());
		auto it = gs_.find(g_);
		if (it == gs_.end()) {
			m_graph = nullptr;
			return;
		}
		m_graph = &(it->getGraph());
		m_labelling.getMainLabels() = it->getLabelling().getMainLabels();
		m_head = head_ + "[" + std::to_string(std::distance(gs_.begin(), it)+1) + "]";
	}

	bool operator<(const term& left_, const term& right_) {
		if (left_.m_graph < right_.m_graph) {
			return true;
		} else if (right_.m_graph < left_.m_graph) {
			return false;
		}
		return left_.m_labelling < right_.m_labelling;
	}

	std::ostream& operator<<(std::ostream& ostream_, const term& t_) {
		auto copyG = *t_.m_graph;
		auto copyL = t_.m_labelling;
		ostream_ << labelledGraph(std::move(copyG), std::move(copyL));
		return ostream_;
	}

	void term::printNum(std::ostream& ostream_) const {
		auto copyG = *m_graph;
		auto copyL = m_labelling;
		labelledGraph(std::move(copyG), std::move(copyL)).printNum(m_head, ostream_);
	}
	
	bool term::canonLabellingHelper(graph& g_) {
		//idea: canonicalize graph with explicit external labels
		//      then set labels to unity and canonicalize again
		//      this should give us unique canonical labelings
		
		//label the externals according to momenta
		for (auto& v : g_.getVerts()) {
			if (v->m_number > 0) {
				auto& l = m_labelling[v->front()->m_ID];
				assert(l.size() == 1 && l.begin()->first > 0
					   && (l.begin()->second == 1 || l.begin()->second == -1));
				v->m_number = l.begin()->first;
			}
		}
		//canonicalize with explicit external labels
		auto map1 = g_.canonMap();
		g_.setExtUnity();
		auto map2 = g_.canonMap();
		
		//this map is now a map from *m_graph to itself with labelling canonicalized
		map1.concat(map2);
		//apply the map to the labelling
		map1.invert();
		std::vector<label> newLabels;
		newLabels.reserve(m_labelling.size());
		for (std::size_t i = 0, s = m_labelling.size(); i != s; ++i) {
			auto& lookup = map1(i);
			if (lookup.second) {
				newLabels.push_back(-m_labelling[lookup.first]);
			} else {
				newLabels.push_back(m_labelling[lookup.first]);
			}
		}
		m_labelling = graphLabelling(std::move(newLabels),
									 std::move(m_labelling.getMainLabels()));

		//use momentum conservation on internal legs
		//count exts
		const std::size_t nExts = g_.nExts();
		//make the replacement label
		label l(false);
		for (std::size_t i = 1; i != nExts; ++i) {
			l.insert(std::make_pair(i,-1));
		}
		//apply the replacement
		for (auto& e : g_.getEdges()) {
			auto vs = e->getActualVerts();
			if (vs.first->m_number <= 0 && vs.second->m_number <= 0) {
				m_labelling[e->m_ID].replace(nExts, l);
			}
		}

		bool sign = map1.sign();
		//use internal symmetries
		/* some experimental code
		auto it = intSyms_.find(m_graph);
		if (it != intSyms_.end()) {
			//there is an internal symmetry
			assert(it->second.size() == 1);//more not yet implemented
			//apply the symmetry and see if it makes *this "smaller" (i.e. more canonical)
			sum s(*this,1);
			s.replace(it->second.front());
			assert(s.size() == 1);
			if (s.begin()->first < *this) {
				//found a "smaller" version of *this
				*this = s.begin()->first;
				//compute sign
				assert(s.begin()->second == 1 || s.begin()->second == -1);
				if (s.begin()->second == -1) {
					sign = !sign;
				}
			}
		}
		*/
		
		return sign;
	}

	sum operator-(const sum& sum_) {
		auto ret = sum_;
		ret.negate();
		return ret;
	}

	sum operator-(sum&& sum_) {
		sum_.negate();
		return std::forward<sum>(sum_);
	}

	sum& sum::operator+=(const sum& right_) {
		for (auto& e : right_) {
			auto found = find(e.first);
			if (found != end()) {
				//sum the entries, i.e. sum coeffs
				found->second += e.second;
				if (found->second == 0) {
					erase(found);
				}
			} else {
				//just insert
				insert(e);
			}
		}
		return *this;
	}

	sum& sum::operator-=(const sum& right_) {
		for (auto& e : right_) {
			auto found = find(e.first);
			if (found != end()) {
				//sum the entries, i.e. sum coeffs
				found->second -= e.second;
				if (found->second == 0) {
					erase(found);
				}
			} else {
				//just insert
				auto itIns = insert(e).first;
				itIns->second = -itIns->second;
			}
		}
		return *this;
	}

	sum& sum::operator*=(const ratio& ratio_) {
		if (ratio_ == 0) {
			//multiply by zero
			clear();
			return *this;
		}
		for (auto& summand : *this) {
			summand.second *= ratio_;
		}
		return *this;
	}

	std::ostream& operator<<(std::ostream& ostream_, const sum& s_) {
		if (s_.empty()) {
			ostream_ << '0';
		}
		for (auto& t : s_) {
			ostream_ << "+(" << t.second << ")*";
			t.first.printNum(ostream_);
		}
		return ostream_;
	}

	void sum::negate() {
		for (auto& e : *this) {
			e.second = -e.second;
		}
	}

	void sum::replaceLabel(const std::size_t& ID_, const label& repl_) {
		std::map<term, ratio> newThis;
		for (auto& t : *this) {
			auto copy = t.first;
			copy.m_labelling.replaceLabel(ID_, repl_);
			newThis.insert(std::make_pair(std::move(copy), t.second));
		}
		std::map<term, ratio>::operator=(std::move(newThis));
	}

	void sum::replaceLabel(const std::map<std::size_t, label>& repl_) {
		std::map<term, ratio> newThis;
		for (auto& t : *this) {
			auto copy = t.first;
			copy.m_labelling.replaceLabel(repl_);
			newThis.insert(std::make_pair(std::move(copy), t.second));
		}
		std::map<term, ratio>::operator=(std::move(newThis));
	}

	bool sum::replace(const rule& rule_) {
		sum add;
		bool change = false;
		auto it = begin();
		while (it != end()) {
			if (it->first.m_graph == rule_.m_from.m_graph) {
				change = true;
				//make replacement map
				std::map<std::size_t, label> repl;
				auto& labellingRule = rule_.m_from.m_labelling;
				auto& labellingT = it->first.m_labelling;
				for (auto& mL : rule_.m_from.m_labelling.getMainLabels()) {
					auto& from = labellingRule[mL.first];
					assert(from.size() == 1);
					if (from.begin()->second == -1) {
						repl.insert(std::make_pair(from.begin()->first,
												   -labellingT[mL.first]));
					} else {
						assert(from.begin()->second == 1);
						repl.insert(std::make_pair(from.begin()->first,
												   labellingT[mL.first]));
					}
				}
				//apply the map and add to sum
				sum tmp = rule_.m_to;
				tmp.replaceLabel(repl);
				
				tmp.canonLabelling();
				tmp *= it->second;
				add += std::move(tmp);
				//remove the replaced term
				it = erase(it);
			} else {
				//just step
				++it;
			}
		}
		operator+=(std::move(add));
		return change;
	}

	void sum::canonLabelling() {
		std::map<term, ratio> newSum;
		for (auto& t : *this) {
			auto copy = t.first;
			auto sign = copy.canonLabelling();
			if (sign) {
				newSum.insert(std::make_pair(std::move(copy), -t.second));
			} else {
				newSum.insert(std::make_pair(std::move(copy), t.second));
			}
		}
		std::map<term, ratio>::operator=(std::move(newSum));
	}

	std::ostream& operator<<(std::ostream& ostream_, const rule& rule_) {
		rule_.m_from.printNum(ostream_);
		ostream_ << " -> " << rule_.m_to;
		return ostream_;
	}

	void rule::initialize() {
		//keep track of labels we have already used
		std::set<std::size_t> nmbrs;
		auto it = m_from.m_labelling.getMainLabels().begin();
		auto end = m_from.m_labelling.getMainLabels().end();
		//solve one after the other for a unique new k
		for (; it != end; ++it) {
			auto l = m_from.m_labelling[it->first];//we need a copy!
			auto itL = l.begin();
			for (auto end = l.end(); itL != end; ++itL) {
				if (nmbrs.count(itL->first) == 0) {
					//found a k we can use as the new label
					break;
				}
			}
			assert(itL != l.end());
			assert(itL->second == 1 || itL->second == -1);
			nmbrs.insert(itL->first);
			//prepare the label replacement
			if (itL->second == 1) {
				l.negate();
				itL->second = 1;
			} //else: nothing to do
			//apply the "solution" to m_from and m_to
			m_to.replaceLabel(itL->first, l);//m_to first, itL might change afterwards
			m_from.m_labelling.replaceLabel(itL->first, l);
		}
	}

	bool rule::replace(const rule& r_) {
		return m_to.replace(r_);
	}

	std::ostream& operator<<(std::ostream& ostream_, const eqn& eqn_) {
		ostream_ << eqn_.m_LHS << "==" << eqn_.m_RHS;
		return ostream_;
	}

	rule eqn::toRule() const {
		assert(m_LHS.size() == 1);
		return rule(m_LHS.begin()->first, m_RHS * (1/m_LHS.begin()->second));
	}

	bool eqn::replace(const rule& rule_) {
		bool ret = m_LHS.replace(rule_);
		if (m_RHS.replace(rule_)) {
			ret = true;
		}
		return ret;
	}

	void eqn::combineByFlavorSym(const std::vector<flavor_t>& fls_) {
		if (fls_.empty()) {
			return;
		}
		const auto end = m_LHS.end();
		for (auto it1 = m_LHS.begin(); it1 != end; ++it1) {
			auto it2 = it1;
			++it2;
			for (; it2 != end; ++it2) {
				if (it1->first.m_graph == it2->first.m_graph) {
					//found two terms with the same graph
					//let's check if they are related by a flavor sym
					auto g = it1->first.m_graph;

					//get flavors contained in this graph
					std::vector<flavor_t> gFls;
					for (auto& fl : fls_) {
						if (g->containsFlavor(fl)) {
							gFls.push_back(fl);
						}
					}

					if (gFls.empty()) {
						continue;
					}
					const auto size = gFls.size();

					auto cR = makeOrderedChooseRunner(fls_.begin(), fls_.end(), gFls.size());

					do {
						const auto& current = cR.getCurrent();
						//construct the map
						std::map<flavor_t, flavor_t> map;
						for (std::size_t i = 0; i != size; ++i) {
							auto pair = std::make_pair(gFls[i], *current[i]);
							if (pair.first != pair.second) {
								map.insert(std::move(pair));
							}
						}
						//skip over identity permutation
						if (map.empty()) {
							continue;
						}
						//make a copy and apply the map
						auto gCopy = *g;
						gCopy.permuteFlavors(map);
						auto lCopy = it2->first.m_labelling;
						//make a copy of the original
						auto gOrig = *g;
						auto& lOrig = it1->first.m_labelling;
						//idea: we canonicalize copy and orig (with external labels) to find a map

						//label the externals of the graph according to labelling
						for (auto& v : gCopy.getVerts()) {
							if (v->m_number > 0) {
								auto& l = lCopy[v->front()->m_ID];
								assert(l.size() == 1 && l.begin()->first > 0
									   && (l.begin()->second == 1 || l.begin()->second == -1));
								v->m_number = l.begin()->first;
							}
						}
						for (auto& v : gOrig.getVerts()) {
							if (v->m_number > 0) {
								auto& l = lOrig[v->front()->m_ID];
								assert(l.size() == 1 && l.begin()->first > 0
									   && (l.begin()->second == 1 || l.begin()->second == -1));
								v->m_number = l.begin()->first;
							}
						}
						//canonicalize both -> get map between the two labellings
						auto map1 = gCopy.canonMap();
						auto map2 = gOrig.canonMap();
						map2.invert();
						map1.concat(map2);

						lCopy.permuteEdges(map1);

						if (lCopy == lOrig) {
							//they are the same!
							if (map1.sign()) {
								it1->second -= it2->second;
							} else {
								it1->second += it2->second;
							}
							if (it1->second == 0) {
								m_LHS.erase(it1);
							}
							m_LHS.erase(it2);
							
							//restart this function
							return combineByFlavorSym(fls_);
						}
					} while (cR.next());
				}
			}
		}
	}

	eqns eqns::makeSymEqns(const labelledGraphs& gs_, const std::string& head_) {
		eqns ret;
		for (auto& g : gs_) {
			auto syms = g.syms();
			bool signG;
			term tG(g, gs_, signG, head_);
			sum s = sum(std::move(tG), signG ? ratio(-1) : ratio(1));
			for (auto& gg : syms) {
				bool sign;
				term t(gg.second, gs_, sign, head_);
				if (sign == gg.first) {
					//sign (we subtract, so there's an additional minus sign)
					ret.push_back(eqn(s + sum(std::move(t), ratio(-1)), sum()));
				} else {
					//no sign
					ret.push_back(eqn(s + sum(std::move(t), ratio(1)), sum()));
				}
			}
		}
		return ret;
	}

	eqns eqns::makeJacobiEqns(const labelledGraphs& gs_, const std::string& head_) {
		eqns ret;
		for (auto& g : gs_) {
			for (auto& pair : g.jacobies()) {
				bool sign;
				term t(g, gs_, sign, head_);
				sum s(std::move(t), sign ? ratio(-1) : ratio(1));

				t = term(pair.first, gs_, sign, head_);
				s += sum(std::move(t), sign ? ratio(1) : ratio(-1));

				t = term(pair.second, gs_, sign, head_);
				s += sum(std::move(t), sign ? ratio(-1) : ratio(1));
				ret.push_back(eqn(std::move(s), sum()));
			}
		}
		return ret;
	}

	eqns eqns::makeTwoTermEqns(const labelledGraphs& gs_,
							  const std::string& head_) {
		eqns ret;
		for (auto& g : gs_) {
			for (auto& pair : g.twoTerm()) {
				bool sign;
				term t (g, gs_, sign, head_);
				sum s(std::move(t), sign ? ratio(-1) : ratio(1));

				t = term(pair.second, gs_, sign, head_);
				s += sum(std::move(t), sign == pair.first ? ratio(-1) : ratio(1));
				ret.push_back(eqn(std::move(s), sum()));
			}
		}
		return ret;
	}

	eqns eqns::makeTwoTermFlEqns(const labelledGraphs& gs_,
								const std::array<flavor_t, 3>& fls_,
								const std::string& head_) {
		eqns ret;
		for (auto& g : gs_) {
			for (auto& pair : g.twoTermFl(fls_)) {
				bool sign;
				term t (g, gs_, sign, head_);
				sum s(std::move(t), sign ? ratio(-1) : ratio(1));

				t = term(pair.second, gs_, sign, head_);
				s += sum(std::move(t), sign == pair.first ? ratio(-1) : ratio(1));
				ret.push_back(eqn(std::move(s), sum()));
			}
		}
		return ret;
	}

	eqns eqns::makeScalarTwoTermEqns(const labelledGraphs& gs_,
									const std::string& head_) {
		eqns ret;
		for (auto& g : gs_) {
			for (auto& pair : g.scalarTwoTerm()) {
				bool sign;
				term t(g, gs_, sign, head_);
				sum s(std::move(t), sign ? ratio(-1) : ratio(1));

				t = term(pair.first, gs_, sign, head_);
				s += sum(std::move(t), sign ? ratio(1) : ratio(-1));

				t = term(pair.second, gs_, sign, head_);
				s += sum(std::move(t), sign ? ratio(-1) : ratio(1));
				ret.push_back(eqn(std::move(s), sum()));
			}
		}
		return ret;
	}

	eqns eqns::makeScalarTwoTermFlEqns(const labelledGraphs& gs_,
									  const std::array<flavor_t,3>& fls_,
									  const std::string& head_) {
		eqns ret;
		for (auto& g : gs_) {
			for (auto& pair : g.scalarTwoTermFl(fls_)) {
				bool sign;
				term t(g, gs_, sign, head_);
				sum s(std::move(t), sign ? ratio(-1) : ratio(1));

				t = term(pair.first, gs_, sign, head_);
				s += sum(std::move(t), sign ? ratio(1) : ratio(-1));

				t = term(pair.second, gs_, sign, head_);
				s += sum(std::move(t), sign ? ratio(-1) : ratio(1));
				ret.push_back(eqn(std::move(s), sum()));
			}
		}
		return ret;
	}

	eqns eqns::makeFlavorSymEqns(const labelledGraphs& gs_,
								 const std::array<flavor_t,3>& fls_,
								 const std::string& head_,
								 const bool& onlyTruePerms_) {
		eqns ret;
		for (auto& g : gs_) {
			//get flavors contained in this graph
			std::vector<flavor_t> gFls;
			for (auto& fl : fls_) {
				if (g.containsFlavor(fl)) {
					gFls.push_back(fl);
				}
			}

			if (gFls.empty()) {
				continue;
			}
			const auto size = gFls.size();

			bool sign;
			term t(g, gs_, sign, head_);
			sum s(std::move(t), sign ? ratio(-1) : ratio(1));

			runner<std::vector<const ggraph::flavor_t*> >* r;
			if (onlyTruePerms_) {
				r = new orderedChooseRunner(makeOrderedChooseRunner(fls_.begin(), fls_.end(), gFls.size()));
			} else {
				r = new rangeRunner(makeRangeRunner(gFls.size(), fls_.begin(), fls_.end()));
			}

			do {
				const auto& current = r->getCurrent();
				//construct the map
				std::map<flavor_t, flavor_t> map;
				for (std::size_t i = 0; i != size; ++i) {
					auto pair = std::make_pair(gFls[i], *current[i]);
					if (pair.first != pair.second) {
						map.insert(std::move(pair));
					}
				}
				//skip over identity permutation
				if (map.empty()) {
					continue;
				}
				//make a copy and apply the map
				auto gCopy = g;
				gCopy.permuteFlavors(map);

				t = term(gCopy, gs_, sign, head_);

				//check if valid graph, i.e. if it was found
				if (t.m_graph) {
					ret.push_back(eqn(s + sum(std::move(t), sign ? ratio(1) : ratio(-1)),
									  sum()));
				}
			} while (r->next());
		}
		return ret;
	}

	eqn eqns::makeNeq4DecompID(const labelledGraph& g_, const labelledGraphs& gsNeq4_,
							   const labelledGraphs& gsNeq2_,
							   const std::string& headNeq4_,
							   const std::string& headNeq2_) {
		auto gs = g_.decompose(labelledGraph::neq4to2DecomposeRules,
							   {coupling::THREEGLUON, coupling::QUARKGLUON});
		bool sign;
		term t(g_, gsNeq4_, sign, headNeq4_);
		sum s(std::move(t), sign ? ratio(-1) : ratio(1));
		for (auto& g : gs) {
			term t(g, gsNeq2_, sign, headNeq2_);
			s += sum(std::move(t), sign ? ratio(1) : ratio(-1));
		}
		return eqn(std::move(s), sum());
	}

	eqn eqns::makeNeq4DecompIDS(const labelledGraph& g_, const labelledGraphs& gsNeq4_,
							    const labelledGraphs& gsNeq2_,
							    const std::string& headNeq4_,
							    const std::string& headNeq2_) {
		auto gs = g_.decompose(labelledGraph::neq4to2DecomposeRulesS,
							   {coupling::THREEGLUON, coupling::SCALARGLUON});
		bool sign;
		term t(g_, gsNeq4_, sign, headNeq4_);
		sum s(std::move(t), sign ? ratio(-1) : ratio(1));
		for (auto& g : gs) {
			term t(g, gsNeq2_, sign, headNeq2_);
			s += sum(std::move(t), sign ? ratio(1) : ratio(-1));
		}
		return eqn(std::move(s), sum());
	}

	eqn eqns::makeNeq2DecompID(const labelledGraph& g_, const labelledGraphs& gsNeq2_,
							   const labelledGraphs& gsNeq1_,
							   const std::string& headNeq2_,
							   const std::string& headNeq1_) {
		auto gs = g_.decompose(labelledGraph::neq2to1DecomposeRules,
							   {coupling::THREEGLUON, coupling::QUARKGLUON,
								coupling::QUARKGLUON1, coupling::QUARKGLUON2,
								coupling::THREEQUARK, coupling::THREEAQUARK});
		bool sign;
		term t(g_, gsNeq2_, sign, headNeq2_);
		sum s(std::move(t), sign ? ratio(-1) : ratio(1));
		for (auto& g : gs) {
			term t(g, gsNeq1_, sign, headNeq1_);
			s += sum(std::move(t), sign ? ratio(1) : ratio(-1));
		}
		return eqn(std::move(s), sum());
	}

	eqn eqns::makeNeq2DecompIDS(const labelledGraph& g_, const labelledGraphs& gsNeq2_,
								const labelledGraphs& gsNeq1_,
								const std::string& headNeq2_,
								const std::string& headNeq1_) {
		auto gs = g_.decompose(labelledGraph::neq2to1DecomposeRulesS,
							   {coupling::THREEGLUON, coupling::SCALARGLUON,
								coupling::SCALARGLUON1, coupling::SCALARGLUON2,
								coupling::THREESCALAR});
		bool sign;
		term t(g_, gsNeq2_, sign, headNeq2_);
		sum s(std::move(t), sign ? ratio(-1) : ratio(1));
		//we need to correct for overcounting the directions:
		//we multiply the N=2 graph instead of dividing the N=1 graphs
		s *= power(ratio(2), g_.countQuarkLoops());
		for (auto& g : gs) {
			term t(g, gsNeq1_, sign, headNeq1_);
			s += sum(std::move(t), sign ? ratio(1) : ratio(-1));
		}
		return eqn(std::move(s), sum());
	}

	eqns eqns::makeRevSymEqns(const labelledGraphs& gs_, const std::string& head_) {
		eqns ret;
		for (auto& g : gs_) {
			bool sign;
			term t(g, gs_, sign, head_);
			sum s(std::move(t), sign ? ratio(-1) : ratio(1));

			for (auto& gg : g.reverseQuarkLines()) {
				t = term(gg.first, gs_, sign, head_);
				sum tmp = s + sum(std::move(t), (gg.second.sign() == sign) ? ratio(-1) : ratio(+1));
				ret.push_back(eqn(std::move(tmp), sum()));
			}
		}
		return ret;
	}

	std::ostream& operator<<(std::ostream& ostream_, const eqns& eqns_) {
		ostream_ << '{';
		bool first = true;
		for (auto& e : eqns_) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << e;
		}
		ostream_ << '}';
		return ostream_;
	}

	void eqns::add(const std::vector<eqn>& eqns_) {
		insert(end(), eqns_.begin(), eqns_.end());
	}

	void eqns::add(std::vector<eqn>&& eqns_) {
		insert(end(), std::make_move_iterator(eqns_.begin()),
			   std::make_move_iterator(eqns_.end()));
	}

	std::vector<rule> eqns::reduce(std::vector<const graph*>& masters_,
								   const std::vector<const graph*>& knowns_,
								   const std::vector<flavor_t>& fls_) {
		std::vector<rule> sol;
		//sort by size
		std::sort(this->begin(), this->end(), m_sorter);
		//remove 0==0
		remove0Eq0();
		//solve equations with just one term (i.e. somegraph = 0)
		while (!empty() && back().m_LHS.size() == 1) {
			sol.push_back(back().toRule());
			pop_back();
			replace(sol.back());
		}
		//pull out two term identities that consist of twice the same graph
		//(i.e. that are just consistency equations for later
		std::vector<eqn> consistencyEqns;
		auto it = rbegin();
		while (it != rend()) {
			if (it->m_LHS.size() > 2) {
				break;
			}
			assert(it->m_LHS.size() == 2);
			auto itSum = it->m_LHS.begin();
			if (itSum->first.m_graph == (++itSum)->first.m_graph) {
				consistencyEqns.push_back(std::move(*it));
				it = std::make_reverse_iterator(erase(it.base() - 1));
				//it = erase(it);
			} else {
				++it;
			}
		}

		//internal symmetries: eqns that consist of twice the same graph with same external labeling
		//auto intSyms = prepareIntSyms(consistencyEqns);
		canonLabelling(/*intSyms*/);

		//solve two-term IDs
		it = rbegin();
		while (it != rend()) {
			if (it->m_LHS.size() > 2) {
				break;
			}
			//solve for vanishing graphs
			if (it->m_LHS.size() == 1 && it->m_RHS.empty()) {
#ifndef NDEBUG
				auto foundM = std::find(masters_.begin(), masters_.end(),
										it->m_LHS.begin()->first.m_graph);
				assert(foundM == masters_.end());
				auto foundK = std::find(knowns_.begin(), knowns_.end(),
										it->m_LHS.begin()->first.m_graph);
				if (foundK != knowns_.end()) {
					//can happen if a known graph is 0
					std::cerr << "found vanishing known graph: " << std::endl
							  << *it << std::endl;
					pop_back();
					it = rbegin();
					continue;
				}
#endif
				sol.push_back(it->toRule());
				pop_back();
				replace(sol.back());
				it = rbegin();
				continue;
			}
			if (it->m_LHS.size() == 1) {
				++it;
				continue;
			}
			assert(it->m_LHS.size() == 2);
			//remove two-term identities with two equal graphs
			auto itLHS = it->m_LHS.begin();
			auto& first = *itLHS;
			auto& second = *(++itLHS);
			if (first.first.m_graph == second.first.m_graph) {
				consistencyEqns.push_back(std::move(*it));
				//it = erase(it);
				it = std::make_reverse_iterator(erase(it.base() - 1));
				continue;
			}
			auto foundM = std::find(masters_.begin(), masters_.end(),
								    first.first.m_graph);
			auto foundK = std::find(knowns_.begin(), knowns_.end(),
								    first.first.m_graph);
			if (foundM != masters_.end() || foundK != knowns_.end()) {
				//first is a master
#ifndef NDEBUG
				foundM = std::find(masters_.begin(), masters_.end(),
								   second.first.m_graph);
				foundK = std::find(knowns_.begin(), knowns_.end(),
								   second.first.m_graph);
				assert(foundM == masters_.end() && foundK == knowns_.end());
#endif
				--itLHS;
				//put minus sign
				itLHS->second = - itLHS->second;
				it->m_RHS.insert(std::move(*itLHS));
				it->m_LHS.erase(itLHS);
			} else {
				//put minus sign
				itLHS->second = - itLHS->second;
				it->m_RHS.insert(std::move(*itLHS));
				it->m_LHS.erase(itLHS);
			}
			sol.push_back(it->toRule());
			erase(it.base() - 1);

			replace(sol.back());
			it = rbegin();//we have resorted the vector
		}

		//move knowns to the right
		for (auto& e : *this) {
			auto itLHS = e.m_LHS.begin();
			while (itLHS != e.m_LHS.end()) {
				auto found = std::find(knowns_.begin(), knowns_.end(),
									   itLHS->first.m_graph);
				if (found != knowns_.end()) {
					itLHS->second = -itLHS->second;
					e.m_RHS.insert(std::move(*itLHS));
					itLHS = e.m_LHS.erase(itLHS);
				} else {
					++itLHS;
				}
			}
		}

		//combine by flavor symmetry
		if (!fls_.empty()) {
			combineByFlavorSym(fls_);
		}
		
		//sort again
		std::sort(this->begin(), this->end(), m_sorter);

   		//special case: there's no master at all:
		if (masters_.empty()) {
			//remove 0==... eqns
			while (!empty() && back().m_LHS.empty()) {
				consistencyEqns.push_back(std::move(back()));
				pop_back();
			}
			//solve for uniques
			while (!empty() && back().m_LHS.size() == 1) {
				sol.push_back(back().toRule());
				pop_back();
				moveToTheRight(sol.back().m_from.m_graph);
				std::sort(this->begin(), this->end(), m_sorter);
				//remove 0==... eqns
				while (!empty() && back().m_LHS.empty()) {
					consistencyEqns.push_back(std::move(back()));
					pop_back();
				}
			}
		}

		//finally solve for masters
		auto mastersIt = masters_.begin();
		while (!empty() && mastersIt != masters_.end()) {
			//move master to the right
			moveToTheRight(*mastersIt++);
			std::sort(this->begin(), this->end(), m_sorter);
			//remove 0==... eqns
			while (!empty() && back().m_LHS.empty()) {
				consistencyEqns.push_back(std::move(back()));
				pop_back();
			}
			//solve for uniques
			while (!empty() && back().m_LHS.size() == 1) {
				sol.push_back(back().toRule());
				pop_back();
				moveToTheRight(sol.back().m_from.m_graph);
				std::sort(this->begin(), this->end(), m_sorter);
				//remove 0==... eqns
				while (!empty() && back().m_LHS.empty()) {
					consistencyEqns.push_back(std::move(back()));
					pop_back();
				}
			}
		}

		if (empty()) {
			while (mastersIt != masters_.end()) {
				mastersIt = masters_.erase(mastersIt);
			}
		}

		if (!empty()) {
			std::cerr << "Warning: system has not been completely solved!" << std::endl;
		} else {
			insert(end(), std::make_move_iterator(consistencyEqns.begin()),
				   std::make_move_iterator(consistencyEqns.end()));
		}

		return sol;
	}

	bool eqns::replace(const rule& rule_) {
		bool ret = false;
		for (auto& e : *this) {
			if (e.replace(rule_)) {
				ret = true;
			}
		}
		std::sort(this->begin(), this->end(), m_sorter);
		remove0Eq0();

		return ret;
	}

	bool eqns::replace(const std::vector<rule>& rules_) {
		bool ret = false;
		for (auto& r : rules_) {
			for (auto& e : *this) {
				if (e.replace(r)) {
					ret = true;
				}
			}
		}
		std::sort(this->begin(), this->end(), m_sorter);
		remove0Eq0();
		
		return ret;
	}

	void eqns::combineByFlavorSym(const std::vector<flavor_t>& fls_) {
		for (auto& e : *this) {
			e.combineByFlavorSym(fls_);
		}
	}

	void eqns::remove0Eq0() {
		//remove 0==0
		while (back().m_LHS.empty() && back().m_RHS.empty()) {
			pop_back();
		}
	}

	void eqns::moveToTheRight(const graph* g_) {
		for (auto& e : *this) {
			auto itLHS = e.m_LHS.begin();
			while (itLHS != e.m_LHS.end()) {
				if (itLHS->first.m_graph == g_) {
					itLHS->second = -itLHS->second;
					e.m_RHS.insert(std::move(*itLHS));
					itLHS = e.m_LHS.erase(itLHS);
				} else {
					++itLHS;
				}
			}
		}
	}

	intSyms_t eqns::prepareIntSyms(const std::vector<eqn>& eqns_) {
		intSyms_t ret;

		for (auto& ee : eqns_) {
			//check if externals are not relabeled
			assert(ee.m_LHS.size() == 2 && ee.m_RHS.size() == 0);
			auto& t1 = ee.m_LHS.begin()->first;
			auto& t2 = (++ee.m_LHS.begin())->first;
			auto& l1 = t1.m_labelling;
			auto& l2 = t2.m_labelling;

			assert(t1.m_graph == t2.m_graph);
			auto& g = *(t1.m_graph);

			bool extAgree = true;
			for (auto& e : g.getEdges()) {
				if (e->first->m_number > 0 || e->second->m_number > 0) {
					//it's an external
					if (l1[e->m_ID] != l2[e->m_ID]) {
						extAgree = false;
						break;
					}
				}
			}

			if (extAgree) {
				//it's an internal symmetry, extract the rule
				eqn e = ee;
				auto itLHS = ++e.m_LHS.begin();
				itLHS->second = -itLHS->second;
				e.m_RHS.insert(std::move(*itLHS));
				e.m_LHS.erase(itLHS);
				ret[&g].push_back(e.toRule());
			}
		}

		return ret;
	}

	bool eqns::sorter::operator()(const eqn& left_, const eqn& right_) {
		auto lL = left_.m_LHS.size();
		auto rL = right_.m_LHS.size();
		if (lL > rL) {
			return true;
		}
		if (rL > lL) {
			return false;
		}
		if (lL + left_.m_RHS.size() > rL + right_.m_RHS.size()) {
			return true;
		}
		return false;
	}

	std::vector<rule>& replaceRepeated(std::vector<rule>& rules_) {
		auto end = rules_.end();
		bool change = true;
		while (change) {
			change = false;
			for (auto it1 = rules_.begin(); it1 != end; ++it1) {
				for (auto it2 = rules_.begin(); it2 != end; ++it2) {
					if (it1 == it2) {
						continue;
					}
					if (it1->replace(*it2)) {
						change = true;
					}
				}
			}
		}
		return rules_;
	}

	ratio power(const ratio& rat_, std::size_t exp_) {
		assert(!(rat_==0 && exp_==0));
		if (exp_ == 0) {
			return ratio(1);
		}

		ratio res = rat_;
		for (--exp_; exp_ != 0; --exp_) {
			res *= rat_;
		}
		return res;
	}
}
