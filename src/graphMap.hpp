#ifndef GKA_GRAPHMAP_HPP
#define GKA_GRAPHMAP_HPP

#include "ggraph_config.h"

#include <vector>
#include <utility>
#include <iostream>

namespace ggraph {
	//forward declaration
	class graph;
	
	class graphMap {
		friend class graph;
	public:
		//**************************************************
		// constructor
		//**************************************************
		/**
		 * identity map for given number of verts/edges
		 */
		graphMap(const std::size_t& nVerts_, const std::size_t& nEdges_);
		graphMap(const std::vector<std::size_t>& verts_ = std::vector<size_t>(),
				 const std::vector<std::pair<std::size_t, bool> >& edges_
				 = std::vector<std::pair<size_t, bool> >(), const bool& sign_ = false)
			: m_verts(verts_), m_edges(edges_), m_sign(sign_) {
		}

		graphMap(std::vector<std::size_t>&& verts_,
				 std::vector<std::pair<std::size_t, bool> >&& edges_,
				 const bool& sign_ = false)
			: m_verts(std::move(verts_)), m_edges(std::move(edges_)), m_sign(sign_) {
		}

		//**************************************************
		// operators
		//**************************************************
		std::size_t& operator[](const std::size_t& vID_) {
			return m_verts[vID_];
		}
		const std::size_t& operator[](const std::size_t& vID_) const {
			return m_verts[vID_];
		}

		std::pair<std::size_t, bool>& operator()(const std::size_t& eID_) {
			return m_edges[eID_];
		}
		const std::pair<std::size_t, bool>& operator()(const std::size_t& eID_) const {
			return m_edges[eID_];
		}

		//**************************************************
		// methods
		//**************************************************
		std::vector<std::size_t>& getVerts() {
			return m_verts;
		}
		const std::vector<std::size_t>& getVerts() const {
			return m_verts;
		}

		std::vector<std::pair<std::size_t, bool> >& getEdges() {
			return m_edges;
		}
		const std::vector<std::pair<std::size_t, bool> >& getEdges() const {
			return m_edges;
		}

		void invert();

		/**
		 * invert but take care of non-invertible edges and vertices carefully
		 */
		void invertCare();

		friend std::ostream& operator<<(std::ostream& ostream_, const graphMap& map_);

		/**
		 * computes the number of flips of legs at vertices mod 2
		 *   false = +, true = -
		 *   and assigns it to m_sign (also returns it)
		 */
		bool computeSign(const graph& from_, const graph& to_);

		const bool& sign() const {
			return m_sign;
		}

		bool& sign() {
			return m_sign;
		}

		/**
		 * concatenates the graph map (the given map is applied AFTER *this)
		 * non-invertible piece are left alone
		 */
		void concat(const graphMap& map_);

	private:
		//**************************************************
		// members
		//**************************************************
		std::vector<std::size_t> m_verts;
		std::vector<std::pair<std::size_t, bool> > m_edges;

		bool m_sign;
	};
}

#endif /*GKA_GRAPHMAP_HPP*/
