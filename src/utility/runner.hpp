#ifndef RUNNER_H
#define RUNNER_H

#include <utility>

namespace ggraph {
	/**
	 * abstract base class for runners, i.e. classes that run over a set of whatever,
	 * e.g. combinatorics
	 */
	template <typename T>
	class runner {
	public:
		//**************************************************
		//methods
		//**************************************************
		virtual bool next() = 0;

		virtual const T& getCurrent() const = 0;

		inline bool isRunning() const {
			return m_isRunning;
		}

	protected:
		//**************************************************
		//constructor
		//**************************************************
		runner(const bool& isRunning_ = true) : m_isRunning(isRunning_) {
		}
		
		//**************************************************
		//members
		//**************************************************
		bool m_isRunning;
	};
}

#endif /*RUNNER_H*/
