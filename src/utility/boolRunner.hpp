#ifndef BOOLRUNNER_HPP
#define BOOLRUNNER_HPP

#include <vector>

#include "runner.hpp"


namespace ggraph {
	/**
	 * runner that goes through all combinations of bool vector of given size
	 * always starts with all false!
	 */
	class boolRunner : public runner<std::vector<bool> > {
	public:
		//**************************************************
		//constructor
		//**************************************************
		boolRunner() : runner<std::vector<bool> >(false) {
		}
		boolRunner(const std::vector<bool>::size_type n_)
			: runner<std::vector<bool> >(n_ > 0), m_current(n_, false) {
		}

		//**************************************************
		//methods
		//**************************************************
		inline bool next() override;

		const std::vector<bool>& getCurrent() const override {
			return m_current;
		}

		void reset() {
			m_current = std::vector<bool>(m_current.size(), false);
			runner<std::vector<bool> >::m_isRunning = m_current.size() > 0;
		}

	private:
		//**************************************************
		//members
		//**************************************************
		std::vector<bool> m_current;
	};

	bool boolRunner::next() {
		if (!runner<std::vector<bool> >::isRunning()) {
			return false;
		}

		auto it = m_current.begin();
		for (; it != m_current.end(); ++it) {
			if (*it == false) {
				*it = true;
				return true;
			} else {
				*it = false;
			}
		}

		runner<std::vector<bool> >::m_isRunning = false;
		return false;
	}
	
}

#endif /*BOOLRUNNER_HPP*/
