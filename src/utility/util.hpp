#ifndef GKA_UTIL_HPP
#define GKA_UTIL_HPP

#include <type_traits>

namespace ggraph {
	/**
	 * helper for sortBy
	 */
	template <typename iter_t, typename iterBy_t>
	void quickPartition(iter_t first_, iter_t last_, iterBy_t firstBy_, iterBy_t lastBy_, iter_t& p_,
						iterBy_t& pBy_) {
		auto pivot = *std::next(firstBy_, (std::distance(firstBy_, lastBy_)-1)/2);
		auto i = first_;
		auto iBy = firstBy_;
		auto j = last_;
		auto jBy = lastBy_;
		while (true) {
			if (*iBy < pivot) {
				do {
					std::advance(i, 1);
					std::advance(iBy, 1);
				} while (*iBy < pivot);
			}

			do {
				std::advance(j, -1);
				std::advance(jBy, -1);
			} while (*jBy > pivot);

			if (iBy >= jBy) {//TODO: change to not use >= but something like std::distance
				std::advance(j,1);
				std::advance(jBy,1);
				p_ = std::move(j);
				pBy_ = std::move(jBy);
				return;
			}
			std::swap(*i, *j);
			std::swap(*iBy, *jBy);
		}
	}
	/**
	 * helper for sortBy
	 */
	template <typename iter_t, typename iterBy_t>
	void quickSortBy(iter_t first_, iter_t last_, iterBy_t firstBy_, iterBy_t lastBy_) {
		if (std::distance(firstBy_, lastBy_) >= 2) {
			iter_t p;
			iterBy_t pBy;
			quickPartition(first_, last_, firstBy_, lastBy_, p, pBy);
			quickSortBy(first_, p, firstBy_, pBy);
			std::advance(p,1);
			std::advance(pBy,1);
			quickSortBy(p, last_, pBy, lastBy_);
		}
	}
	/**
	 * sort a range by applying the function to each object and sort according to that value
	 * @return sorted vector of function applied to the range
	 */
	template <typename iter_t, typename fun>
	auto sortBy(iter_t first_, iter_t last_, fun fun_) -> std::vector<typename std::remove_reference<decltype((*first_))>::type> {
		std::vector<typename std::remove_reference<decltype((*first_))>::type> vec;
		vec.reserve(std::distance(first_,last_));

		for (auto it = first_; it != last_; std::advance(it,1)) {
			vec.push_back(fun_(*it));
		}

		quickSortBy(first_, last_, vec.begin(), vec.end());

		return vec;
	}
}

#endif /*GKA_UTIL_HPP*/
