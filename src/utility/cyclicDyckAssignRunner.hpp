#ifndef CYCLICDYCKASSIGNRUNNER_HPP
#define CYCLICDYCKASSIGNRUNNER_HPP

#include <cassert>
#include <vector>

#include "runner.hpp"
#include "dyckWord.hpp"
#include "heapSwapRunner.hpp"


namespace ggraph {
	/**
	 * permutes a vector (of pairs preferably) such that it can be assigned to a dyckWord
	 * and the first element is always in the first closed structure of the dyckWord
	 * (i.e. this kills the cyclic permutations if one considers all dyckWords and all
	 * permutations the assigned pairs)
	 * similar to permVectRunner it directly permutes the given vector (stores a reference)
	 */
	template <typename T>
	class cyclicDyckAssignRunner : public runner<std::vector<T>*> {
	public:
		//**************************************************
		//constructors
		//**************************************************
		cyclicDyckAssignRunner() : runner<std::vector<T>*>(false) {
		}
		cyclicDyckAssignRunner(std::vector<T>* vect_, const dyckWord& dyckWord_);
		//**************************************************
		//methods
		//**************************************************
		bool next() override;

		std::vector<T>* const& getCurrent() const override {
			return m_current;
		}

		/**
		 * resets m_isRunning, such that *this can be used again
		 * also resets the first element to first position
		 * and the dyckWord can be reset
		 * (but starting from a different start vector)
		 */
		void reset(const dyckWord& dyckWord_);
		
	private:
		//**************************************************
		//members
		//**************************************************
		std::vector<T>* m_current;

		/**
		 * current and highest position the first element is allowed to be
		 */
		std::size_t m_currentPos;
		std::size_t m_highestPos;

		/**
		 * runner that does the permutation of all but the first element
		 */
		heapSwapRunner m_swapRunner;

		//**************************************************
		//methods
		//**************************************************
		void determineHighestPos(const dyckWord& dyckWord_);
	};

	template <typename T>
	cyclicDyckAssignRunner<T>::cyclicDyckAssignRunner(std::vector<T>* vect_,
													  const dyckWord& dyckWord_)
		: runner<std::vector<T>*>(!vect_->empty()), m_current(vect_), m_currentPos(0),
		m_swapRunner(vect_->empty() ? 0 : vect_->size() - 1) {
		assert(vect_->size() == dyckWord_.size());
		//determine highestPos that first element can be
		determineHighestPos(dyckWord_);
	}
	

	template <typename T>
	bool cyclicDyckAssignRunner<T>::next() {
		if (!runner<std::vector<T>*>::isRunning()) {
			return false;
		}

		//swapRunner step
		if (m_swapRunner.next()) {
			//do the swap
			auto current = m_swapRunner.getCurrent();
			//adapt swap to not include the first element
			if (current.first >= m_currentPos) {
				++current.first;
			}
			if (current.second >= m_currentPos) {
				++current.second;
			}

			std::swap((*m_current)[current.first], (*m_current)[current.second]);
			return true;
		}

		assert(!m_swapRunner.isRunning());
		//change position of first element step
		if (m_currentPos < m_highestPos) {
			//reset swapRunner
			m_swapRunner.reset();
			std::swap((*m_current)[m_currentPos], (*m_current)[m_currentPos + 1]);
			++m_currentPos;

			return true;
		}
		//else
		runner<std::vector<T>*>::m_isRunning = false;
		return false;
	}

	template <typename T>
	void cyclicDyckAssignRunner<T>::reset(const dyckWord& dyckWord_) {
		//swap first element back to first position
		std::swap((*m_current)[0], (*m_current)[m_currentPos]);
		m_currentPos = 0;
		//recompute m_highestPos
		determineHighestPos(dyckWord_);

		m_swapRunner.reset();

		runner<std::vector<T>*>::m_isRunning = dyckWord_.size() > 0;
	}

	template <typename T>
	void cyclicDyckAssignRunner<T>::determineHighestPos(const dyckWord& dyckWord_) {
		if (dyckWord_.empty()) {
			m_highestPos = 0;
			return;
		}
		
		std::size_t counter = 0;
		auto it = dyckWord_.begin();
#ifndef NDEBUG
		bool found = false;
#endif		
		for (std::size_t i = 0; i != 2 * dyckWord_.size(); ++i) {
			assert(it != dyckWord_.end());
			if (i < *it) {
				++counter;
			} else {
				assert(i == *it);
				--counter;
				++it;
			}

			if (counter == 0) {
#ifndef NDEBUG
				found = true;
#endif				
				m_highestPos = (it - dyckWord_.begin()) - 1;
				break;
			}
		}
#ifndef NDEBUG		
		assert(found);
#endif		
	}
}

#endif /*CYCLICDYCKASSIGNRUNNER_HPP*/
