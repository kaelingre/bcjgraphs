#ifndef CYCLICORDEREDCHOOSERUNNER_HPP
#define CYCLICORDEREDCHOOSERUNNER_HPP

#include "runner.hpp"
#include "chooseRunner.hpp"
#include "permItRunner.hpp"

namespace ggraph {
	/**
	 * runner that does the operation "m choose n" on a vector, but chooses all different orderings
	 * up to cyclicity
	 * a selection is returned as a vector (of length n_) of iterators (that point to vector entries)
	 */
	template <typename T>
	class cyclicOrderedChooseRunner : public runner<choice<T> > {
		public:
		//**************************************************
		//constructor
		//**************************************************
		cyclicOrderedChooseRunner();
		cyclicOrderedChooseRunner(const typename std::vector<T>::iterator& begin_,
								  const typename std::vector<T>::iterator& end_,
								  const unsigned int& n_);
		
		//**************************************************
		//methods
		//**************************************************
		bool next() override;

		inline const choice<T>& getCurrent() const override {
			return m_current;
		}

		void reset();
		
	private:
		//**************************************************
		//members
		//**************************************************
		/**
		 * ordering is important for initialization order!
		 */
		chooseRunner<T> m_chooseRunner;
		choice<T> m_current;
		permItRunner<typename choice<T>::iterator> m_permItRunner;
	};

	template <typename T>
	cyclicOrderedChooseRunner<T>::cyclicOrderedChooseRunner()
		: runner<choice<T> >(false), m_permItRunner(std::make_pair(m_current.begin(), m_current.end())) {
	}

	template <typename T>
	cyclicOrderedChooseRunner<T>::cyclicOrderedChooseRunner(
		const typename std::vector<T>::iterator& begin_,
		const typename std::vector<T>::iterator& end_, const unsigned int& n_) 
		: runner<choice<T> >(end_ - begin_ >= n_), m_chooseRunner(begin_, end_, n_),
		m_current(m_chooseRunner.getCurrent()),
		m_permItRunner(std::make_pair( m_current.begin() + (m_current.size() > 1 ? 1 : 0),
									  m_current.end())) {
	}

	template <typename T>
	bool cyclicOrderedChooseRunner<T>::next() {
		if (!runner<choice<T> >::isRunning()) {
			return false;
		}

		if (m_permItRunner.next()) {
			return true;
		}
		//else
		if (m_chooseRunner.next()) {
			m_current = m_chooseRunner.getCurrent();
			m_permItRunner = permItRunner<typename choice<T>::iterator>(
				std::make_pair(m_current.begin() + 1, m_current.end()));
			return true;
		}
	
		runner<choice<T>>::m_isRunning = false;
		return false;
	}

	template <typename T>
	void cyclicOrderedChooseRunner<T>::reset() {
		m_chooseRunner.reset();
		m_permItRunner = permItRunner<typename choice<T>::iterator>(
			std::make_pair(m_current.begin() + (m_current.size() > 1 ? 1 : 0), m_current.end()));

		runner<choice<T>>::m_isRunning = m_chooseRunner.isRunning() && m_permItRunner.isRunning();
	}
}

#endif /*CYCLICORDEREDCHOOSERUNNER_HPP*/
