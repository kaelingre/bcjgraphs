#ifndef ORDEREDCHOOSERUNNER_HPP
#define ORDEREDCHOOSERUNNER_HPP

#include "runner.hpp"
#include "chooseRunner.hpp"
#include "heapSwapRunner.hpp"

namespace ggraph {
	/**
	 * runner that does the operation "m choose n" on a vector, but chooses all different orderings
	 * a selection is returned as a vector (of length n_) of iterators (that point to vector entries)
	 * i.e. (1, 2, 3) and (2,1,3) will both appear
	 * TODO: implement changes for non-random-access-iterators
	 */
	template <typename T>
	class orderedChooseRunner : public runner<std::vector<T> > {
		public:
		//**************************************************
		//constructor
		//**************************************************
		orderedChooseRunner();
		orderedChooseRunner(const T& begin_, const T& end_, const unsigned int& n_);
		
		//**************************************************
		//methods
		//**************************************************
		bool next() override;

		inline const std::vector<T>& getCurrent() const override {
			return m_current;
		}

		void reset();
		
	private:
		//**************************************************
		//members
		//**************************************************
		/**
		 * ordering is important for initialization order!
		 */
		chooseRunner<T> m_chooseRunner;
		std::vector<T> m_current;
		heapSwapRunner m_swapRunner;
	};

	template <typename T>
	orderedChooseRunner<T> makeOrderedChooseRunner(T&& begin_, T&& end_,
												   const unsigned int& n_) {
		return orderedChooseRunner<T>(std::forward<T>(begin_), std::forward<T>(end_),
									  n_);
	}

	template <typename T>
	orderedChooseRunner<T>::orderedChooseRunner()
		: runner<std::vector<T> >(false), m_swapRunner(0) {
	}
		
	template <typename T>
	orderedChooseRunner<T>::orderedChooseRunner(const T& begin_, const T& end_,
												const unsigned int& n_) 
		: runner<std::vector<T> >(end_ - begin_ >= n_ && n_ > 0), m_chooseRunner(begin_, end_, n_),
		m_current(m_chooseRunner.getCurrent()), m_swapRunner(m_current.size()) {
	}

	template <typename T>
	bool orderedChooseRunner<T>::next() {
		if (!runner<std::vector<T> >::isRunning()) {
			return false;
		}
		
		if (m_swapRunner.next()) {
			auto&& current = m_swapRunner.getCurrent();
			std::swap(m_current[current.first], m_current[current.second]);
			return true;
		}
		//else
		if (m_chooseRunner.next()) {
			m_current = m_chooseRunner.getCurrent();
			m_swapRunner.reset();
			return true;
		}
	
		runner<std::vector<T>>::m_isRunning = false;
		return false;
	}

	template <typename T>
	void orderedChooseRunner<T>::reset() {
		m_chooseRunner.reset();
		m_swapRunner.reset();
		runner<std::vector<T>>::m_isRunning = m_chooseRunner.isRunning() && m_swapRunner.isRunning();
	}
}

#endif /*ORDEREDCHOOSERUNNER_HPP*/
