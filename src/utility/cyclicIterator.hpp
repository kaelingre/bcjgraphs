#ifndef CYCLICITERATOR_HPP
#define CYCLICITERATOR_HPP

namespace ggraph {
	template <typename iteratorType>
	class cyclicIterator : public iteratorType {
	public:
		//**************************************************
		//constructor
		//**************************************************
		cyclicIterator(const iteratorType& it_, const iteratorType& begin_, const iteratorType& end_)
			: iteratorType(it_), m_begin(begin_), m_end(end_) {
		}

		//**************************************************
		//methods
		//**************************************************
		/**
		 * get underlying iterator
		 */
		const iteratorType& get() const {
			return *this;
		}
		
		cyclicIterator<iteratorType>& operator++() {
			if (iteratorType::operator++() == m_end) {
				iteratorType::operator=(m_begin);
			}
			return *this;
		}

		cyclicIterator<iteratorType> operator++(int) {
			cyclicIterator<iteratorType> ret = *this;
			operator++();
			return ret;
		}

		cyclicIterator<iteratorType>& operator--() {
			if ((*this) == m_begin) {
				if (m_begin == m_end) {
					return *this;
				}
				iteratorType::operator=(m_end - 1);
			} else {
				iteratorType::operator--();
			}
			return *this;
		}

		cyclicIterator<iteratorType> operator--(int) {
			cyclicIterator<iteratorType> ret = *this;
			operator--();
			return ret;
		}

		cyclicIterator<iteratorType>& operator+=(size_t add_) {
			while (add_ > 0) {
				operator++();
				//step
				--add_;
			}
			return *this;
		}

		cyclicIterator<iteratorType>& operator-=(size_t subtract_) {
			while (subtract_ > 0) {
				operator--();
				//step
				--subtract_;
			}
			return *this;
		}

		cyclicIterator<iteratorType> operator+(size_t add_) const {
			auto newIt = *this;
			while (add_ > 0) {
				++newIt;
				//step
				--add_;
			}
			return newIt;
		}

		cyclicIterator<iteratorType> operator-(size_t subtract_) const {
			auto newIt = *this;
			while (subtract_ > 0) {
				--newIt;
				//step
				--subtract_;
			}
			return newIt;
		}
	private:
		//**************************************************
		//members
		//**************************************************
		iteratorType m_begin;
		iteratorType m_end;
	};
	
	//**************************************************
	//factory (for automatic type deduction)
	//**************************************************
	template <typename iteratorType>
	cyclicIterator<iteratorType> makeCyclicIterator(const iteratorType& it_,
													const iteratorType& begin_,
													const iteratorType& end_) {
		return cyclicIterator<iteratorType>(it_, begin_, end_);
	}
}

#endif /*CYCLICITERATOR_HPP*/
