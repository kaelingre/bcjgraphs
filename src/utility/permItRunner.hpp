#ifndef PERMITRUNNER_HPP
#define PERMITRUNNER_HPP

#include "runner.hpp"

namespace ggraph {
	/**
	 * a runner that runs over all permutations of elements of a given range given by iterators,
	 * so it directly permutes given range (doesn't make a copy)
	 */
	template <typename randomAccessIt>
	class permItRunner : public runner<std::pair<randomAccessIt, randomAccessIt> > {
	public:
		//**************************************************
		//constructor
		//**************************************************
		permItRunner(const std::pair<randomAccessIt, randomAccessIt >& range_);

		bool next() override;

		inline const std::pair<randomAccessIt, randomAccessIt>& getCurrent() const
			override {
			return m_range;
		}

		/**
		 * resets m_vect, m_runner and m_isRunning, such that *this can be used again
		 * (but starting from a different start vector)
		 */
		inline void reset() {
			m_vect = std::vector<unsigned int>(m_vect.size(), 0);
			m_runner = 0;
			runner<std::pair<randomAccessIt, randomAccessIt>>::m_isRunning = (m_vect.size() > 1);
		}

	private:
		//**************************************************
		//members
		//**************************************************
		std::pair<randomAccessIt, randomAccessIt > m_range;
		
		std::vector<unsigned int> m_vect;
		std::vector<unsigned int>::size_type m_runner;
	};

	template <typename randomAccessIt>
	permItRunner<randomAccessIt>::permItRunner(const std::pair<randomAccessIt,
													 randomAccessIt >& range_)
		: runner<std::pair<randomAccessIt, randomAccessIt> >(range_.second - range_.first > 1),
		m_range(range_), m_vect(range_.second - range_.first, 0), m_runner(0) {
	}

	template <typename randomAccessIt>
	bool permItRunner<randomAccessIt>::next() {
		if (!runner<std::pair<randomAccessIt, randomAccessIt> >::isRunning()) {
			return false;
		}

		//non-recursive Heap's algorithm
		//end
		if (m_runner == m_vect.size()) {
			runner<std::pair<randomAccessIt, randomAccessIt> >::m_isRunning = false;
			return false;
		}

		if (m_vect[m_runner] < m_runner) {
			std::swap(m_runner % 2 ? *(m_range.first + m_vect[m_runner]) : *(m_range.first),
					  *(m_range.first + m_runner));
			m_vect[m_runner]++;
			m_runner = 0;
			return true;
		} else {
			m_vect[m_runner++] = 0;
			return next();
		}
	}
}

#endif /*PERMITRUNNER_HPP*/
