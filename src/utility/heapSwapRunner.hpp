#ifndef HEAPSWAPRUNNER_HPP
#define HEAPSWAPRUNNER_HPP

#include "runner.hpp"
#include <cstddef>
#include <utility>
#include <vector>

namespace ggraph {
	/**
	 * runner that produces all swaps given by Heap's algorithm
	 */
	class heapSwapRunner : public runner<std::pair<std::size_t, std::size_t> > {
	public:
		//**************************************************
		//constructor
		//**************************************************
		heapSwapRunner()
			: runner<std::pair<std::size_t, std::size_t> >(false), m_current(std::make_pair(0, 0)) {
		}
		inline heapSwapRunner(const std::size_t& n_);

		//**************************************************
		//methods
		//**************************************************
		inline bool next() override;

		const std::pair<std::size_t, std::size_t>& getCurrent() const override {
			return m_current;
		}

		inline void reset();

	private:
		//**************************************************
		//members
		//**************************************************
		std::pair<std::size_t, std::size_t> m_current;

		std::vector<std::size_t> m_vect;
		std::vector<std::size_t>::size_type m_runner;
	};

	heapSwapRunner::heapSwapRunner(const std::size_t& n_)
		: runner<std::pair<std::size_t, std::size_t> >(n_ > 0), m_current(std::make_pair(0, 0)),
		m_vect(n_, 0), m_runner(0) {
	}

	bool heapSwapRunner::next() {
		if (!runner<std::pair<std::size_t, std::size_t> >::isRunning()) {
			return false;
		}

		//non-recursive Heap's algorithm
		//end
		if (m_runner == m_vect.size()) {
			runner<std::pair<std::size_t, std::size_t> >::m_isRunning = false;
			return false;
		}

		if (m_vect[m_runner] < m_runner) {
			//swap
			if (m_runner % 2) {
				m_current = std::make_pair(m_vect[m_runner], m_runner);
			} else {
				m_current = std::make_pair(0, m_runner);
			}
			m_vect[m_runner]++;
			m_runner = 0;
			return true;
		} else {
			m_vect[m_runner++] = 0;
			return next();
		}
	}

	void heapSwapRunner::reset() {
		m_vect = std::vector<std::size_t>(m_vect.size(), 0);
		m_runner = 0;
		m_current = std::make_pair(0, 0);
		runner<std::pair<std::size_t, std::size_t> >::m_isRunning = (m_vect.size() > 0);
	}
}

#endif /*HEAPSWAPRUNNER_HPP*/
