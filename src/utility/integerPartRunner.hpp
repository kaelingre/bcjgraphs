#ifndef GGRAPH_INTEGERPARTRUNNER_HPP
#define GGRAPH_INTEGERPARTRUNNER_HPP

#include "runner.hpp"

#include <cassert>
#include <numeric>

namespace ggraph {
	/**
	 * Runner that goes through all partitions of an integer number of a given length (but the parts can be zero)
	 */
	class integerPartRunner : public runner< std::vector<unsigned int> > {
	public:
		//**************************************************
		// constructors
		//**************************************************
		integerPartRunner() : runner<std::vector<unsigned int> >(false) {
		}
		integerPartRunner(const unsigned int& n_, const std::size_t& size_)
			: runner<std::vector<unsigned int> >(size_>0), m_current(size_, 0) {
			if (size_ > 0) {
				m_current.front() = n_;
			}
		}

		//**************************************************
		// methods
		//**************************************************
		inline bool next() override;

		const std::vector<unsigned int>& getCurrent() const override {
			return m_current;
		}

		void reset() {
			unsigned int n = std::accumulate(m_current.begin(), m_current.end(), 0);
			m_current = std::vector<unsigned int>(m_current.size(), 0);
			m_current.front() = n;
			runner<std::vector<unsigned int> >::m_isRunning = m_current.size() > 0;
		}

	private:
		//**************************************************
		// members
		//**************************************************
		std::vector<unsigned int> m_current;
	};

	bool integerPartRunner::next() {
		if (!runner<std::vector<unsigned int> >::isRunning()) {
			return false;
		}

		if (m_current.size() == 1) {
			runner<std::vector<unsigned int> >::m_isRunning = false;
			return false;
		}

		//find, starting from the end, the first non-zero entry excluding the last entry
		assert(m_current.size() > 1);
		auto it = m_current.end() - 2;
		while (it != m_current.begin() && *it == 0) {
			--it;
		}
		if (it == m_current.begin() && *it == 0) {
			//end of recursion
			runner<std::vector<unsigned int> >::m_isRunning = false;
			return false;
		}
		//reduce that entry by one
		--(*it);
		++it;
		unsigned int n = std::accumulate(it, m_current.end(), 1);
		*(it++) = n;
		while (it != m_current.end()) {
			*it = 0;
			++it;
		}
		return true;
	}
}

#endif GGRAPH_INTEGERPARTRUNNER_HPP
