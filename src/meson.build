ggraph_lib = library(
	'ggraph',
	sources : [
		'graph.cpp', 'graph.hpp',
		'graphs.cpp', 'graphs.hpp',
		'graphMap.cpp', 'graphMap.hpp',
		'graphLabelling.cpp', 'graphLabelling.hpp',
		'orderedTrees.cpp', 'orderedTrees.hpp',
		'graphRunner.cpp', 'graphRunner.hpp',
		'graphLoopRunner.cpp', 'graphLoopRunner.hpp',
		'tuplesRunner.cpp', 'tuplesRunner.hpp',
		'couplingsRunner.cpp', 'couplingsRunner.hpp',
		'canonGraphRunner.hpp',
		'cut.cpp', 'cut.hpp',
		'eqns.cpp', 'eqns.hpp',
		'util.hpp',
		'libraryLink.cpp', 'libraryLink.hpp'
	],
	dependencies : [permGroup_dep, graphCanon_dep, openmp_dep],
	include_directories : incdirs,
	install : true
)

main_bin = executable(
	'main',
	sources : ['main.cpp'],
	link_with : [ggraph_lib],
	dependencies : [permGroup_dep, graphCanon_dep, openmp_dep],
	include_directories : incdirs,
	install : false
)

prepNeq1Leq2VS_bin = executable(
	'prepNeq1Leq2VS',
	sources : ['prepNeq1Leq2VS.cpp'],
	link_with : [ggraph_lib],
	dependencies : [permGroup_dep, graphCanon_dep, openmp_dep],
	include_directories : incdirs,
	install : false
)

prepNeq1Leq2V_bin = executable(
	'prepNeq1Leq2V',
	sources : ['prepNeq1Leq2V.cpp'],
	link_with : [ggraph_lib],
	dependencies : [permGroup_dep, graphCanon_dep, openmp_dep],
	include_directories : incdirs,
	install : false
)


# additional install
install_headers(
	'graph.hpp', 'graphs.hpp', 'graphMap.hpp', 'graphLabelling.hpp', 'orderedTrees.hpp',
	'graphRunner.hpp', 'graphLoopRunner.hpp', 'tuplesRunner.hpp', 'couplingsRunner.hpp',
	'canonGraphRunner.hpp', 'cut.hpp', 'eqns.hpp', 'util.hpp', 'libraryLink.hpp',
	subdir : 'ggraph')

install_subdir('utility', install_dir : 'include/ggraph', strip_directory : false)

install_subdir('cereal/include/cereal', install_dir : 'include', strip_directory : false)

# make a symlink to the installed library inside gGraph/LibraryResources/<sysID>/
if get_option('mma') and mma_cmd.found()
	meson.add_install_script('symLinkLib.sh', #ggraph_lib.name(),
		'libggraph.so', sysID.stdout().strip(), get_option('libdir'))
endif

# generate pkg_config file
pkg = import('pkgconfig')
if get_option('mma') and mma_cmd.found()
	pkg.generate(ggraph_lib,
				 description : 'Feynman graph library',
				 #adding MMA include dirs
				 extra_cflags : ''.join(['-I',incDirMMA.stdout().strip()]))
else
	pkg.generate(ggraph_lib,
				 description : 'Feynman graph library')
endif
