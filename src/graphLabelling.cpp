#include "graphLabelling.hpp"
#include "utility/orderedChooseRunner.hpp"
#include "utility/rangeRunner.hpp"
#include "canonGraphRunner.hpp"

#include <algorithm>
#include <cctype>

namespace ggraph {
	label::label(const std::size_t& id_, const int& coeff_) : m_null(false) {
		insert(std::make_pair(id_, coeff_));
	}

	label::label(const std::map<std::size_t, int>& map_)
		: std::map<std::size_t, int>(map_), m_null(false) {
	}

	label::label(std::map<std::size_t, int>&& map_)
		: std::map<std::size_t, int>(std::move(map_)), m_null(false) {
	}

	label::label(std::istream& istream_) : m_null(false) {
		istream_ >> std::skipws;
		char c;
		while (!istream_.eof()) {
			//coefficient
			int coeff = 1;

			//peek for possible sign (we have to peek because there might be a sign without a number)
			istream_ >> std::ws;//discard leading whitespace
			c = istream_.peek();
			if (c == '-') {
				coeff = -1;
				istream_.ignore();
			} else if (c == '+') {
				istream_.ignore();
			}

			istream_ >> std::ws;
			if (!isalpha(istream_.peek())) {
				int coeff2;
				istream_ >> coeff2;
				assert(istream_.good());
				coeff *= coeff2;

				istream_ >> std::ws;
				if (istream_.peek() == '*') {
					istream_.ignore();
					istream_ >> std::ws;
				}
			}

			if (coeff == 0) {
				assert(empty());
				return;
			}

			//label string
			assert(!istream_.eof());
			std::string momLabel;
			std::getline(istream_, momLabel, '[');
			assert(istream_.good());
			assert(momLabel == STD_LABEL_STR);

			//ID
			std::size_t id;
			istream_ >> id;
			assert(istream_.good());

			istream_ >> c;
			assert(c == ']');

			//add
			insert(std::make_pair(id, coeff));

			//check if we reach a delimiter
			istream_ >> std::ws;
			c = istream_.peek();
			if (c == '}' || c == ',') {
				break;
			}
		}
	}

	label operator-(const label& label_)  {
		auto ret = label_;
		ret.negate();
		return ret;
	}

	label operator-(label&& label_)  {
		label_.negate();
		return std::forward<label>(label_);
	}

	label& label::operator+=(const label& right_) {
		unsetNull();
		for (auto& e : right_) {
			auto found = find(e.first);
			if (found != end()) {
				//sum the entries (i.e. sum coefficients)
				found->second += e.second;
				if (found->second == 0) {
					//erase since zero
					erase(found);
				}
			} else {
				//just insert this entry
				insert(e);
			}
		}
		return *this;
	}

	label& label::operator-=(const label& right_) {
		unsetNull();
		for (auto& e : right_) {
			auto found = find(e.first);
			if (found != end()) {
				//subtract the entries (i.e. coefficients
				found->second -= e.second;
				if (found->second == 0) {
					//erase since zero
					erase(found);
				}
			} else {
				//insert this entry and negate
				auto itIns = insert(e).first;
				itIns->second = -itIns->second;
			}
		}
		return *this;
	}

	label operator+(label left_, const label& right_) {
		return left_ += right_;
	}

	label operator-(label left_, const label& right_) {
		return left_ -= right_;
	}

	void label::negate() {
		for (auto& e : *this) {
			e.second = - e.second;
		}
	}

	std::ostream& operator<<(std::ostream& ostream_, const label& label_) {
		if (label_.isNull()) {
			ostream_ << "null";
			return ostream_;
		}
		if (label_.isZero()) {
			ostream_ << 0;
		}
		ostream_ << std::showpos;//turn on explicit signs
		for (const auto& e : label_) {
			ostream_ << e.second << label::STD_LABEL_STR << '[' << e.first << ']';
		}
		//turn off explicit signs
		ostream_ << std::noshowpos;
		return ostream_;
	}

	size_t label::highestLabelID() const {
		size_t ret = 0;
		for (const auto& e : *this) {
			ret = std::max(ret, e.first);
		}
		return ret;
	}

	void label::permute(const permutation& perm_) {
		std::map<std::size_t, int> copy = std::move(*this);
		clear();
		for (auto& e : copy) {
			auto it = perm_.find(e.first);
			if (it != perm_.end()) {
				insert(std::make_pair(it->second, e.second));
			} else {
				insert(std::move(e));
			}
		}
	}

	void label::changeSign(const std::size_t& id_) {
		for (auto& l : *this) {
			if (l.first == id_) {
				l.second = -l.second;
				return;
			}
		}
	}

	void label::changeSign(const std::vector<std::size_t>& ids_) {
		for (auto& l : *this) {
			if (std::find(ids_.begin(), ids_.end(), l.first) != ids_.end()) {
				l.second = -l.second;
			}
		}
	}

	void label::replace(const std::size_t& ID_, label repl_) {
		auto it = find(ID_);
		if (it != end()) {
			//multiply repl_ by coefficient
			auto& coeff = it->second;
			if (coeff != 1) {
				for (auto& e : repl_) {
					e.second *= coeff;
				}
			}
			erase(it);
			operator+=(std::move(repl_));
		}
	}

	void label::replace(const std::map<std::size_t, label>& repl_) {
		label add(false);
		for (auto& pair : repl_) {
			auto it = find(pair.first);
			if (it != end()) {
				label tmp = pair.second;
				//multiply by coeff
				auto& coeff = it->second;
				if (coeff != 1) {
					for (auto& e : tmp) {
						e.second *= coeff;
					}
				}
				add += std::move(tmp);
				erase(it);
			}
		}
		operator+=(std::move(add));
	}

	const std::string label::STD_LABEL_STR("k");

	graphLabelling::graphLabelling(const graph& graph_, const bool& useExtNumbers_)
		: std::vector<label>(graph_.nEdges()) {
		//label externals
		for (const auto& v : graph_.getVerts()) {
			if (v->m_number > 0) {
				auto& e = v->front();
				if (isDirected(e->m_type)) {
					//also label fake edges
					if (e.m_dir) {
						label l(useExtNumbers_ ? v->m_number : m_mainLabels.size()+1, -1);
						(*this)[e->m_ID] = l;
						auto ee = e->second->other(e);
						(*this)[ee->m_ID] = l;
						//middle leg should be main label
						m_mainLabels.push_back(std::make_pair(ee->m_ID, true));
						ee = ee->second->other(ee);
						(*this)[ee->m_ID] = l;
					} else {
						label l(useExtNumbers_ ? v->m_number : m_mainLabels.size()+1, +1);
						(*this)[e->m_ID] = l;
						auto ee = e->first->other(e);
						(*this)[ee->m_ID] = l;
						//middle leg should be main label
						m_mainLabels.push_back(std::make_pair(ee->m_ID, false));
						ee = ee->first->other(ee);
						(*this)[ee->m_ID] = l;
					}
				} else {
					if (e.m_dir) {
						(*this)[e->m_ID] = label(
							useExtNumbers_ ? v->m_number : m_mainLabels.size()+1, -1);
						m_mainLabels.push_back(std::make_pair(e->m_ID, true));
					} else {
						(*this)[e->m_ID] = label(
							useExtNumbers_ ? v->m_number : m_mainLabels.size()+1, +1);
						m_mainLabels.push_back(std::make_pair(e->m_ID, false));
					}
				}
			}
		}

		//label internals
		labelInternal(graph_);
	}

	bool operator<(const graphLabelling& left_, const graphLabelling& right_) {
		if (left_.size() != right_.size()) {
			return left_.size() < right_.size();
		}

		for (std::size_t i = 0; i != left_.size(); ++i) {
			if (left_[i] != right_[i]) {
				return left_[i] < right_[i];
			}
		}
		return false;
	}

	std::ostream& operator<<(std::ostream& ostream_, const graphLabelling& gL_) {
		for (auto& e : gL_) {
			ostream_ << e << std::endl;
		}
		return ostream_;
	}

	label& graphLabelling::relabel(const graph::edge& e_, const graph::vert& v_) {
		auto& l = operator[](e_->m_ID);
		l.clear();
		for (auto& e : *v_) {
			if (e == e_) {
				continue;
			}
			if (e.m_dir != e_.m_dir) {
				l += operator[](e->m_ID);
			} else {
				l -= operator[](e->m_ID);
			}
		}
		return l;
	}
	
	void graphLabelling::printArgs(std::ostream& ostream_) const {
		bool first = true;
		for (const auto& l : m_mainLabels) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}

			if (l.second) {
				ostream_ << (-(*this)[l.first]);
			} else {
				ostream_ << (*this)[l.first];
			}
		}
	}

	void graphLabelling::labelCut(const graph& graph_) {
		//just in case:
		clear();
		std::vector<label>::operator=(std::vector<label>(graph_.nEdges()));
		//label externals
		for (const auto& v : graph_.getVerts()) {
			if (v->m_number > 0) {
				auto& e = v->front();
				graph::vert vv;
				if (e.m_dir) {
					(*this)[e->m_ID] = label(m_mainLabels.size() + 1, -1);
					m_mainLabels.push_back(std::make_pair(e->m_ID, true));
					vv = e->second;
				} else {
					(*this)[e->m_ID] = label(m_mainLabels.size() + 1, +1);
					m_mainLabels.push_back(std::make_pair(e->m_ID, false));
					vv = e->first;
				}
				//also label the other fake/actual edges
				if (!e->isActualEdge()) {
					auto ee = vv->other(e);
					if (e.m_dir) {
						(*this)[ee->m_ID] = label(m_mainLabels.size(), -1);
						//this should actually be the main label
						m_mainLabels.back().first = ee->m_ID;
						vv = ee->second;
					} else {
						(*this)[ee->m_ID] = label(m_mainLabels.size(), 1);
						//this should actually be the main label
						m_mainLabels.back().first = ee->m_ID;
						vv = ee->first;
					}
					ee = vv->other(ee);
					if (e.m_dir) {
						(*this)[ee->m_ID] = label(m_mainLabels.size(), -1);
					} else {
						(*this)[ee->m_ID] = label(m_mainLabels.size(), 1);
					}
				}
			}
		}

		//label internal
		labelInternalCut(graph_);
	}

	void graphLabelling::permute(const permutation& perm_) {
		for (auto& e : *this) {
			e.permute(perm_);
		}
	}

	void graphLabelling::permuteEdges(const graphMap& map_) {
		std::vector<label> newLabels(size(), label());
		for (std::size_t i = 0, s = size(); i != s; ++i) {
			auto& lookup = map_(i);
			if (lookup.second) {
				newLabels[lookup.first] = -(*this)[i];
			} else {
				newLabels[lookup.first] = std::move((*this)[i]);
			}
		}
		this->std::vector<label>::operator=(std::move(newLabels));
	}

	void graphLabelling::changeSign(const std::size_t& id_) {
		for (auto& l : *this) {
			l.changeSign(id_);
		}
	}

	void graphLabelling::changeSign(const std::vector<std::size_t>& ids_) {
		for (auto& l : *this) {
			l.changeSign(ids_);
		}
	}

	void graphLabelling::replaceLabel(const std::size_t& ID_, const label& repl_) {
		for (auto& l : *this) {
			l.replace(ID_, repl_);
		}
	}

	void graphLabelling::replaceLabel(const std::map<std::size_t, label>& repl_) {
		for (auto& l : *this) {
			l.replace(repl_);
		}
	}

	void graphLabelling::labelInternal(const graph& graph_) {
		//try to find an edge that is uniquely determined
		graph::edge nullEdge = nullptr;
		for (const auto& e : graph_.getEdges()) {
			if ((*this)[e->m_ID].isNull()) {
				//e is not yet labelled
				if (!nullEdge) {
					nullEdge = e.get();
				}

				if (e->first == e->second) {
					//skip tadpole
					continue;
				}

				//search towards first vertex
				const auto& v1 = e->first;
				bool foundNull = false;
				label newLabel;
				newLabel.unsetNull();
				for (const auto& e2 : *v1) {
					if (e2 != e.get()) {
						//check if null
						auto& eLabel = (*this)[e2->m_ID];
						if (eLabel.isNull()) {
							//found a not yet labelled edge
							foundNull = true;
							break;
						} else {
							//add to newLabel
							if (e2.m_dir) {
								newLabel -= eLabel;
							} else {
								newLabel += eLabel;
							}
						}
					}
				}
				if (!foundNull) {
					(*this)[e->m_ID] = std::move(newLabel);
					//call recursively
					return labelInternal(graph_);
				}

				//search towards second vertex
				const auto& v2 = e->second;
				foundNull = false;
				newLabel.clear();
				for (const auto& e2 : *v2) {
					if (e2 != e.get()) {
						//check if null
						auto& eLabel = (*this)[e2->m_ID];
						if (eLabel.isNull()) {
							//found a not yet labelled edge
							foundNull = true;
							break;
						} else {
							//add to newLabel
							if (e2.m_dir) {
								newLabel += eLabel;
							} else {
								newLabel -= eLabel;
							}
						}
					}
				}
				if (!foundNull) {
					(*this)[e->m_ID] = std::move(newLabel);
					//call recursively
					return labelInternal(graph_);
				}
			}
		}

		if (nullEdge) {
			if (!nullEdge->isLoop() || !nullEdge->isActualEdge()) {
				//find another null edge that is part of a loop
				nullEdge = nullptr;
				for (auto& e : graph_.getEdges()) {
					if (e->isActualEdge() && (*this)[e->m_ID].isNull()
						&& e->isLoop()) {
						nullEdge = e.get();
						break;
					}
				}
				assert(nullEdge);
			}
			//label this one with next higher momentum and call recursion
			(*this)[nullEdge->m_ID] = label(m_mainLabels.size()+1, 1);
			m_mainLabels.push_back(std::make_pair(nullEdge->m_ID, false));
			return labelInternal(graph_);
		}
	}

	void graphLabelling::labelInternalCut(const graph& graph_) {
		for (auto& e : graph_.getEdges()) {
			if (!e->isActualEdge()) {
				continue;
			}
			auto verts = e->getActualVerts();
			if (verts.first->m_number > 0 || verts.second->m_number > 0) {
				continue;
			}

			(*this)[e->m_ID] = label(m_mainLabels.size()+1, 1);
			m_mainLabels.push_back(std::make_pair(e->m_ID, false));

			//also label eventual fake edges
			if (isDirected(e->m_type)) {
				auto ee = e->first->other(e.get());
				assert(!ee.m_dir);
				(*this)[ee->m_ID] = label(m_mainLabels.size(), 1);
				ee = e->second->other(e.get());
				assert(ee.m_dir);
				(*this)[ee->m_ID] = label(m_mainLabels.size(), 1);

			}
		}
	}

	labelledGraph::labelledGraph(std::stringstream& stream_) {
		stream_ >> std::skipws;
		assert(!stream_.eof());
		char c;
		stream_ >> c;
		assert(c == '{');
		//read in vertices
		stream_ >> c;
		assert(stream_);
		assert(c == '{');//open list of verts
		std::map<std::size_t, std::size_t> vertMap;
		std::map<std::size_t, std::vector<std::size_t> > edgeOrdering;//maps vertID to edgeOrdering
		while (!stream_.eof()) {
			stream_ >> c;
			assert(c == '{');
			unsigned int id;
			stream_ >> id;
			assert(stream_.good());
			stream_ >> c;
			assert(c == ',');
			stream_ >> std::ws;
			bool isExt = !(stream_.peek() == '{');
			unsigned int extN = 0;
			if (isExt) {
				stream_ >> extN;
				assert(stream_.good());
				stream_ >> c;
				assert(c == ',');
			}

			//make the vertex
			auto v = m_graph.addVertex(graph::vert_t(extN));
			vertMap.insert(std::make_pair(id, v->m_ID));
			
			stream_ >> c;
			assert(c == '{');

			auto ordering = edgeOrdering.insert(std::make_pair(v->m_ID,
															   std::vector<std::size_t>())).first;
			while (!stream_.eof()) {
				int eID;
				assert(!stream_.eof());
				stream_ >> eID;
				assert(stream_.good());
				ordering->second.push_back(eID < 0 ? -eID : eID);
				stream_ >> std::ws;
				if (stream_.peek() == '}') {
					stream_.ignore();
					break;
				}
				assert(!stream_.eof());
				stream_ >> c;
				assert(c == ',');
			}
			assert(!stream_.eof());
			stream_ >> std::ws >> c;
			assert(c == '}');//closing vertex
			
			stream_ >> std::ws;
			assert(!stream_.eof());
			if (stream_.peek() == '}') {
				stream_.ignore();
				break;
			}
			stream_ >> c;
			assert(c == ',');
		}
		assert(!stream_.eof());
		stream_ >> c;
		assert(c == ',');
		assert(!stream_.eof());
		stream_ >> c;
		assert(c == '{');

		std::map<std::size_t, std::size_t> edgeMap;
		//read in edges
		while (!stream_.eof()) {
			assert(!stream_.eof());
			stream_ >> c;
			assert(c == '{');
			assert(!stream_.eof());
			stream_ >> c;
			assert(c == '"');
			std::string typeStr;
			std::getline(stream_, typeStr, '"');
			auto type = stringToLineT(typeStr);
			assert(!stream_.eof());
			stream_ >> c;
			assert(c == ',');
			std::size_t id;
			stream_ >> id;
			assert(stream_.good());
			assert(!stream_.eof());
			stream_ >> c;
			assert(c == ',');
			std::size_t fromID, toID;
			assert(!stream_.eof());
			stream_ >> fromID;
			assert(!stream_.eof());
			stream_ >> c;
			assert(c == '-');
			assert(!stream_.eof());
			stream_ >> c;
			assert(c == '>');
			assert(!stream_.eof());
			stream_ >> toID;
			assert(!stream_.eof());
			stream_ >> c;
			assert(c == ',');
			assert(!stream_.eof());
			stream_ >> c;
			assert(c == '{');
			flavor_t fl;
			assert(!stream_.eof());
			stream_ >> fl;
			assert(stream_.good());
			assert(!stream_.eof());
			stream_ >> c;
			assert(c == ',');
			label l(stream_);
			assert(!stream_.eof());
			stream_ >> c;
			assert(c == '}');

			assert(!stream_.eof());
			stream_ >> c;
			assert(c == '}');//closing this edge

			//make the edge
			auto v1 = m_graph[vertMap.at(fromID)];
			auto v2 = m_graph[vertMap.at(toID)];
			auto e = m_graph.addEdge(graph::edge_t(fl,type), v1, v2);
			//update labelling
			assert(m_labelling.size() == e->m_ID);
			m_labelling.push_back(std::move(l));
			if (isDirected(type)) {
				//also label helping edges
				auto e1 = e->first->other(e);
				assert(m_labelling.size() == e1->m_ID);
				m_labelling.push_back(std::move(l));
				auto e2 = e->second->other(e);
				assert(m_labelling.size() == e2->m_ID);
				m_labelling.push_back(std::move(l));
			}
			edgeMap.insert(std::make_pair(id, e->m_ID));

			//check if there's another edge
			stream_ >> std::ws;
			assert(!stream_.eof());
			if (stream_.peek() == '}') {
				stream_.ignore();
				break;
			}
			stream_ >> c;
			assert(c == ',');
		}

		//reorder edges at each vertex
		for (auto& v : m_graph.getVerts()) {
			if (v->size() <= 2) {
				continue;
			}
			auto ordering = edgeOrdering.at(v->m_ID);
			assert(v->size() == ordering.size());
			auto itV = v->begin();
			for (auto eID : ordering) {
				eID = edgeMap.at(eID);
				auto itV2 = itV;
				while ((*itV2)->getActualEdge()->m_ID != eID) {
					++itV2;
					assert(itV2 != v->end());
				}
				assert((*itV2)->getActualEdge()->m_ID == eID);
				std::swap(*itV,*itV2);
				//step
				++itV;
			}
		}
		
		//TODO: determine mainLabels

		assert(!stream_.eof());
		stream_ >> c;
		assert(c == '}');
	}

	std::ostream& operator<<(std::ostream& ostream_, const labelledGraph& g_) {
		//vertices
		ostream_ << "{{";
		bool first = true;
		for (const auto& v : g_.m_graph.getVerts()) {
			if (v->m_number < 0) {
				continue;
			}
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << '{' << v->m_ID + 1 << ',';
			//check if external
			if (v->m_number > 0) {
				ostream_ << v->m_number << ',';
			}
			//connected edges
			ostream_ << '{';
			bool firstE = true;
			for (const auto& e : *v) {
				if (firstE) {
					firstE = false;
				} else {
					ostream_ << ',';
				}
				if (!e.m_dir) {
					ostream_ << '-';
				}
				ostream_ << (e->getActualEdge()->m_ID + 1);
			}
			ostream_ << "}}";
		}
		ostream_ << "},{";
		//edges
		first = true;
		for (const auto& e : g_.m_graph.getEdges()) {
			if (!e->isActualEdge()) {
				continue;
			}
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			auto verts = e->getActualVerts();
			ostream_ << '{' << e->m_type << ',' << e->m_ID + 1 << ',' << verts.first->m_ID + 1
					 << "->" << verts.second->m_ID + 1 << ',' << '{' << e->m_flavor << ','
					 << g_.m_labelling[e->m_ID] << "}}";
			
		}
		ostream_ << "}}";

		return ostream_;
	}

	void labelledGraph::printVerbose(std::ostream& ostream_) const {
		//vertices
		ostream_ << "{{";
		bool first = true;
		for (const auto& v : m_graph.getVerts()) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << '{' << v->m_ID + 1 << ',';
			//check if external
			if (v->m_number > 0) {
				ostream_ << v->m_number << ',';
			}
			//connected edges
			ostream_ << '{';
			bool firstE = true;
			for (const auto& e : *v) {
				if (firstE) {
					firstE = false;
				} else {
					ostream_ << ',';
				}
				if (!e.m_dir) {
					ostream_ << '-';
				}
				ostream_ << (e->m_ID + 1);
			}
			ostream_ << "}}";
		}
		ostream_ << "},{";
		//edges
		first = true;
		for (const auto& e : m_graph.getEdges()) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << '{' << e->m_type << ',' << e->m_ID + 1 << ',' << e->first->m_ID + 1
					 << "->" << e->second->m_ID + 1 << ',' << '{' << e->m_flavor << ','
					 << m_labelling[e->m_ID] << "}}";
			
		}
		ostream_ << "}}";
	}

	std::vector<std::pair<bool, labelledGraph> > labelledGraph::syms() const {
		std::vector<std::pair<bool, labelledGraph> > ret;
		auto symGens = m_graph.makeSymmetryGrp();
		for (auto& gen : symGens) {
			auto map = m_graph.symGrpGenToMap(gen);
			map.invert();
			//make a copy
			auto g = *this;
			g.getLabelling().permuteEdges(map);
			ret.push_back(std::make_pair(map.sign(), std::move(g)));
		}
		return ret;
	}

	std::vector<std::pair<labelledGraph, labelledGraph> > labelledGraph::jacobies() const {
		//find gluon tri-vertices
		//or find a matter leg connecting a three-matter vertex with a 2-quark-1-gluon vertex
		std::vector<std::pair<labelledGraph, labelledGraph> > ret;
		for (const auto& v : m_graph.getVerts()) {
			if (v->m_number == 0) {
				bool isAllGluon = true;
				bool isAllMatter = true;
				for (const auto& e : *v) {
					if (e->m_type != line_t::GLUON) {
						isAllGluon = false;
					} else {
						isAllMatter = false;
					}
				}
				if (isAllGluon) {
					for (const auto& e : *v) {
						//e shouldn't be a tadpole leg
						if (e->other(v.get())->m_number <= 0 && e->first != e->second) {
							//other side is not an external
							ret.emplace_back(jacobies(e, v.get()));
						}
					}
				} else if (isAllMatter) {
					for (const auto& e: *v) {
						auto vv = e->otherActual(v.get());
						bool containsGluon = false;
						for (const auto& ee: *vv) {
							if (ee->m_type == line_t::GLUON) {
								containsGluon = true;
								break;
							}
						}
						if (containsGluon) {
							if (isDirected(e->m_type)) {
								ret.emplace_back(jacobies2Dir(e, v.get()));
							} else {
								ret.emplace_back(jacobies2Undir(e, v.get()));
							}
						}
					}
				}
			}
		}

		return ret;
	}

	std::vector<std::pair<bool, labelledGraph> > labelledGraph::twoTerm() const {
		//find gluon leg connected to two matter lines
		std::vector<std::pair<bool, labelledGraph> > ret;
		for (const auto& e : m_graph.getEdges()) {
			graph::edge ee(e.get(), true);
			if (ee->m_type != line_t::GLUON) {
				continue;
			}
			const auto& v1 = ee->first;
			const auto& v2 = ee->second;
			if (v1->size() != 3 || v2->size() != 3) {
				continue;
			}
			assert(v1->m_number == 0 && v2->m_number == 0);
			auto firstEdges = v1->nextTwo(ee);
			ee.m_dir = false;
			auto secondEdges = v2->nextTwo(ee);
			if (firstEdges.first->m_type == line_t::QUARK
				&& firstEdges.second->m_type == line_t::QUARK
				&& secondEdges.first->m_type == line_t::QUARK
				&& secondEdges.second->m_type == line_t::QUARK
				&& firstEdges.first->m_flavor == secondEdges.first->m_flavor) {
				assert(firstEdges.second->m_flavor == firstEdges.first->m_flavor
					   && secondEdges.second->m_flavor == secondEdges.first->m_flavor);
				ret.emplace_back(twoTerm(ee));
			}
		}
		
		return ret;
	}

	std::vector<std::pair<bool, labelledGraph> > labelledGraph::twoTermFl(
		const std::array<flavor_t, 3>& fls_) const {
		//find gluon leg connected to two matter lines with different flavors
		std::vector<std::pair<bool, labelledGraph> > ret;
		for (const auto& e : m_graph.getEdges()) {
			graph::edge ee(e.get(), true);
			if (ee->m_type != line_t::GLUON) {
				continue;
			}
			const auto& v1 = ee->first;
			const auto& v2 = ee->second;
			if (v1->size() != 3 || v2->size() != 3) {
				continue;
			}
			assert(v1->m_number == 0 && v2->m_number == 0);
			auto firstEdges = v1->nextTwo(ee);
			ee.m_dir = false;
			auto secondEdges = v2->nextTwo(ee);
			if (firstEdges.first->m_type == line_t::QUARK
				&& firstEdges.second->m_type == line_t::QUARK
				&& secondEdges.first->m_type == line_t::QUARK
				&& secondEdges.second->m_type == line_t::QUARK
				&& firstEdges.first->m_flavor != secondEdges.first->m_flavor) {
				auto flIt1 = std::find(fls_.begin(), fls_.end(), firstEdges.first->m_flavor);
				auto flIt2 = std::find(fls_.begin(), fls_.end(), secondEdges.first->m_flavor);
				if (flIt1 != fls_.end() && flIt2 != fls_.end()) {
					assert(firstEdges.second->m_flavor == firstEdges.first->m_flavor
						   && secondEdges.second->m_flavor == secondEdges.first->m_flavor);
					//find the third flavor
					auto it = fls_.begin();
					for (auto e = fls_.end(); it != e; ++it) {
						if (it != flIt1 && it != flIt2) {
							break;
						}
					}
					assert(it != fls_.end());
					ret.emplace_back(twoTermFl(ee, *it));
				}
			}
		}
		
		return ret;
	}

	std::vector<std::pair<labelledGraph, labelledGraph> >
	labelledGraph::scalarTwoTerm() const {
		//find gluon leg connected to two scalar lines
		std::vector<std::pair<labelledGraph, labelledGraph> > ret;
		for (const auto& e : m_graph.getEdges()) {
			graph::edge ee(e.get(), true);
			if (ee->m_type != line_t::GLUON) {
				continue;
			}
			const auto& v1 = ee->first;
			const auto& v2 = ee->second;
			if (v1->size() != 3 || v2->size() != 3) {
				continue;
			}
			assert(v1->m_number == 0 && v2->m_number == 0);
			auto firstEdges = v1->nextTwo(ee);
			ee.m_dir = false;
			auto secondEdges = v2->nextTwo(ee);
			if (firstEdges.first->m_type == line_t::SCALAR
				&& firstEdges.second->m_type == line_t::SCALAR
				&& secondEdges.first->m_type == line_t::SCALAR
				&& secondEdges.second->m_type == line_t::SCALAR
				&& firstEdges.first->m_flavor == secondEdges.first->m_flavor) {
				assert(firstEdges.second->m_flavor == firstEdges.first->m_flavor
					   && secondEdges.second->m_flavor == secondEdges.first->m_flavor);
				ret.emplace_back(scalarTwoTerm(ee));
			}
		}
		
		return ret;
	}

	std::vector<std::pair<labelledGraph, labelledGraph> >
	labelledGraph::scalarTwoTermFl(const std::array<flavor_t, 3>& fls_) const {
		//find gluon leg connected to two scalar lines of different flavors
		std::vector<std::pair<labelledGraph, labelledGraph> > ret;
		for (const auto& e : m_graph.getEdges()) {
			graph::edge ee(e.get(), true);
			if (ee->m_type != line_t::GLUON) {
				continue;
			}
			const auto& v1 = ee->first;
			const auto& v2 = ee->second;
			if (v1->size() != 3 || v2->size() != 3) {
				continue;
			}
			assert(v1->m_number == 0 && v2->m_number == 0);
			auto firstEdges = v1->nextTwo(ee);
			ee.m_dir = false;
			auto secondEdges = v2->nextTwo(ee);
			if (firstEdges.first->m_type == line_t::SCALAR
				&& firstEdges.second->m_type == line_t::SCALAR
				&& secondEdges.first->m_type == line_t::SCALAR
				&& secondEdges.second->m_type == line_t::SCALAR
				&& firstEdges.first->m_flavor != secondEdges.first->m_flavor) {
				auto flIt1 = std::find(fls_.begin(), fls_.end(), firstEdges.first->m_flavor);
				auto flIt2 = std::find(fls_.begin(), fls_.end(), secondEdges.first->m_flavor);
				if (flIt1 != fls_.end() && flIt2 != fls_.end()) {
					assert(firstEdges.second->m_flavor == firstEdges.first->m_flavor
						   && secondEdges.second->m_flavor == secondEdges.first->m_flavor);
					//find the third flavor
					auto it = fls_.begin();
					for (auto e = fls_.end(); it != e; ++it) {
						if (it != flIt1 && it != flIt2) {
							break;
						}
					}
					assert(it != fls_.end());
					ret.emplace_back(scalarTwoTermFl(ee, *it));
				}
			}
		}
		
		return ret;
	}

	void labelledGraph::printNum(const std::string& head_, std::ostream& ostream_) const {
		ostream_ << head_ << '[';
		//m_labelling.printArgs(ostream_);
		bool first = true;
		for (const auto& l : getLabelling().getMainLabels()) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}

			auto& e = m_graph.m_edges[l.first];
			auto vs = e->getActualVerts();
			if (vs.first->m_number > 0 || vs.second->m_number > 0) {
				//it's an external
				ostream_ << '{';
				if (l.second) {
					ostream_ << (-m_labelling[l.first]);
				} else {
					ostream_ << m_labelling[l.first];
				}
				ostream_ << ',';
				printLineFl(e->m_type, e->m_flavor, vs.second->m_number > 0, ostream_);
				ostream_ << '}';
			} else {
				//not an external
				if (l.second) {
					ostream_ << (-m_labelling[l.first]);
				} else {
					ostream_ << m_labelling[l.first];
				}
			}
		}
		ostream_ << ']';
	}

	void labelledGraph::printNum(const std::string& head_, const graphMap& map_,
								 const std::vector<std::pair<std::size_t, bool> >& mainLabels_,
								 std::ostream& ostream_) const {
		ostream_ << (map_.sign() ? '-' : '+') << head_ << '[';
		bool first = true;
		for (const auto& l : mainLabels_) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}

			auto mappedE = map_(l.first);
			
			auto& e = m_graph.m_edges[mappedE.first];
			auto vs = e->getActualVerts();
			if (vs.first->m_number > 0 || vs.second->m_number > 0) {
				//it's an external
				ostream_ << '{';
				if (l.second == mappedE.second) {
					ostream_ << m_labelling[mappedE.first];
				} else {
					ostream_ << (-m_labelling[mappedE.first]);
				}
				ostream_ << ',';
				printLineFl(e->m_type, e->m_flavor, vs.second->m_number > 0, ostream_);
				ostream_ << '}';
			} else {
				//not an external
				if (l.second == mappedE.second) {
					ostream_ << m_labelling[mappedE.first];
				} else {
					ostream_ << (-m_labelling[mappedE.first]);
				}
			}
		}
		ostream_ << ']';
	}

	void labelledGraph::printSyms(const std::string& head_, std::ostream& ostream_) const {
		ostream_ << '{';
		//symmetries from the symmetry group
		auto symGrp = m_graph.makeSymmetryGrp();
		bool first = true;
		for (auto& gen : symGrp) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			auto map = m_graph.symGrpGenToMap(gen);
			printNum(head_, map, m_labelling.getMainLabels(), ostream_);
		}

		//symmetries from multi-edges
		//since the graph is canonicalized the edges are ordered, i.e. multi-edges
		//come next to each other in the graph
		//make an identity graph map
		//TODO: multiedges for directed edges (shouldn't happen for our problems though)
		graphMap idMap(m_graph.nVerts(), m_graph.nEdges());
		
		assert(m_graph.nEdges() >= 1);
		for (auto it = m_graph.eBegin(), end = m_graph.eEnd() - 1; it != end; ++it) {
			auto& e1 = **it;
			auto& e2 = **(it+1);
			//compare the properties and the starting end ending vertex
			if (graph::edge_t::compare(e1,e2)==0 && e1.first == e2.first
				&& e1.second == e2.second) {
				//we don't want double tadpoles (for now, TODO?)
				assert(e1.first != e1.second);
				auto id1 = (*it)->m_ID;
				auto id2 = (*(it+1))->m_ID;
				//found a multi-edge pair
				idMap(id1).first = id2;
				idMap(id2).first = id1;
				ostream_ << ',';
				printNum(head_, idMap, m_labelling.getMainLabels(), ostream_);
				//swap back to re-use idMap
				idMap(id1).first = id1;
				idMap(id2).first = id2;
			}
		}

		//symmetries from (undirected) tadpoles
		for (auto& e : m_graph.getEdges()) {
			if (e->first == e->second) {
				//found a tadpoles
				idMap(e->m_ID).second = true;
				idMap.sign() = true;
				ostream_ << ',';
				printNum(head_, idMap, m_labelling.getMainLabels(), ostream_);
				//swap back to re-use idMap
				idMap(e->m_ID).second = false;
				idMap.sign() = false;
			}
		}

		ostream_ << '}';
	}

	/*
	std::size_t labelledGraph::countMultiEdgeTadpoleSyms() const {
		std::size_t ret = 1;
		//multi-edges: they come after each other if canonicalized
		auto it = m_graph.eBegin();
		const auto ee = m_graph.eEnd();
		while (it != ee) {
			std::size_t counter = 1;
			auto it2 = it+1;
			for (; it2 != ee; ++it2) {
				if (graph::edge_t::compare(**it,**it2)==0 && (*it)->first == (*it2)->first
					&& (*it)->second == (*it2)->second) {
					//we don't want double tadpoles (for now, TODO?)
					assert((*it)->first != (*it)->second);
					++counter;
				} else {
					break;
				}
			}
			//multiply by counter! (factorial)
			while (counter != 1) {
				ret *= counter--;
			}
			//step
			it = it2;
		}

		//simple tadpoles
		it = m_graph.eBegin();
		while (it != ee) {
			if ((*it)->first == (*it)->second) {
				ret *= 2;
			}
			//step
			++it;
		}

		return ret;
	}
	*/

	void labelledGraph::permute(const permutation& perm_) {
		m_graph.permute(perm_);
		m_labelling.permute(perm_);
	}

	std::pair<labelledGraph, graphMap> labelledGraph::toTopo() const {
		auto g = m_graph.toTopo();

		//update the labelling
		const auto& map = g.second;
		std::vector<label> newLabelling;
		for (std::size_t i = 0; i != m_graph.nEdges(); ++i) {
			if (map(i).first != -1) {
				newLabelling.push_back(m_labelling[i]);
			}
		}
		graphLabelling labelling(std::move(newLabelling),
								 std::vector<std::pair<std::size_t, bool> >(
									 m_labelling.getMainLabels()));

		//update mainLabels
		auto& mainL = labelling.m_mainLabels;
		for (auto it = mainL.begin(); it != mainL.end(); ++it) {
			//update the mainLabel id
			it->first = map(it->first).first;
		}
		labelledGraph lG(std::move(g.first), std::move(labelling));
		return std::make_pair(std::move(lG), std::move(map));
	}
	
	graphMap labelledGraph::canonMap() {
		auto map = m_graph.canonMap();

		//update labelling
		auto nEdges = m_graph.nEdges();
		std::vector<label> newLabels (nEdges, label());
		for (std::size_t i = 0; i != nEdges; ++i) {
			auto& lookup = map(i);
			if (lookup.second) {
				m_labelling[i].negate();
			}
			newLabels[lookup.first] = std::move(m_labelling[i]);
		}
		m_labelling.std::vector<label>::operator=(std::move(newLabels));

		//update main labels
		for (auto& l : m_labelling.m_mainLabels) {
			auto& lookup = map(l.first);
			l.first = lookup.first;
			l.second = (l.second != lookup.second);
		}

		return map;
	}

	void labelledGraph::printProps(std::ostream& ostream_) const {
		ostream_ << '{';
		bool first = true;
		for (auto& e : getGraph().getEdges()) {
			if (!e->isActualEdge()) {
				continue;
			}
			auto vs = e->getActualVerts();
			if (vs.first->m_number > 0 || vs.second->m_number > 0) {
				continue;
			}
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << m_labelling[e->m_ID];
		}
		ostream_ << '}';
	}

	std::vector<labelledGraph> labelledGraph::decompose(
		const std::map<flLine_t, std::vector<flExt> > map_,
		const std::vector<coupling>& couplings_) const {
		typedef std::vector<flExt>::const_iterator iter;
		//iterator together with its begin and end boundaries
		typedef std::pair<iter , std::pair<iter, iter> > itRange;
		//map from edge id to an itRange giving it's current new flExt
		std::map<std::size_t, itRange> mapToIts;

		//prepare this map
		for (auto& e : m_graph.getEdges()) {
			if (!e->isActualEdge()) {
				continue;
			}
			auto vs = e->getActualVerts();
			//exclude ext edges
			if (vs.first->m_number > 0 || vs.second->m_number > 0) {
				continue;
			}

			auto it = map_.find(std::make_pair(e->m_type, e->m_flavor));
			if (it != map_.end()) {
				assert(!it->second.empty());
				mapToIts.insert(std::make_pair(e->m_ID, std::make_pair(
												   it->second.begin(),
												   std::make_pair(it->second.begin(),
																  it->second.end()))));
			}
		}

		std::vector<labelledGraph> ret;
		if (mapToIts.empty()) {
			//no lines to decompose present, just return the given graph
			ret.push_back(*this);
			return ret;
		}
		bool next = false;
		do {
			//check if valid combination according to couplings
			bool isValid = true;
			for (auto& v : m_graph.getVerts()) {
				if (v->m_number < 0 || v->m_number > 0) {
					continue;
				}
				std::vector<flExt> partons;
				for (auto& e : *v) {
					auto ee = e->getActualEdge();
					auto it = mapToIts.find(ee->m_ID);
					flExt flE;
					if (it != mapToIts.end()) {
						flE = *(it->second.first);
						if (!e.m_dir) {
							flE.first = antiParticle(flE.first);
						}
					} else {
						flE = flExt(lineDirToExt(e->m_type, e.m_dir), e->m_flavor);
					}
					partons.push_back(flE);
				}
				
				auto it = std::find(couplings_.begin(), couplings_.end(),
									coupling(partons));
				if (it == couplings_.end()) {
					isValid = false;
					break;
				}
			}
			if (isValid) {
				//we might remove some fake vertices and might have to shift the
				//edge ID's inside mapToIts
				std::vector<std::size_t> shifts;
				//make a copy
				auto lG = *this;
				auto& g = lG.m_graph;
				//actually apply the line changes
				for (auto& itR : mapToIts) {
					auto lineDir = extToLineDir(itR.second.first->first);
					//shift the edgeID
					std::size_t edgeID = itR.first;
					for (auto& s : shifts) {
						if (edgeID > s ) {
							--edgeID;
						}
					}
					graph::edge e = g.m_edges[edgeID].get();
					bool oldIsDirected = isDirected(e->m_type);
					std::size_t fakeID1, fakeID2;
					if (oldIsDirected) {
						fakeID1 = e->first->other(e)->m_ID;
						fakeID2 = e->second->other(e)->m_ID;
					}
					if (lineDir.second) {
						e = g.replaceEdge(graph::edge_t(itR.second.first->second,
														lineDir.first),e, false);
					} else {
						//be careful with tadpoles that need to be inverted!
						e = g.replaceEdge(graph::edge_t(itR.second.first->second,
														lineDir.first),e, true);
					}
					bool newIsDirected = isDirected(e->m_type);
					
					//relabel
					//if old and new are undirected: nothing to do
					//else:
					if (!oldIsDirected && newIsDirected) {
						assert(g.nEdges() == lG.m_labelling.size() + 2);
						lG.m_labelling.push_back(label(false));
						lG.m_labelling.push_back(label(false));
						//swap sign switch if direction changed
						auto& l = lG.m_labelling[e->m_ID];
						if (!lineDir.second) {
							l.negate();
						}
						//set new fake vertex labels
						auto it = lG.m_labelling.end() - 1;
						*it = l;
						*(--it) = l;
					} else if (oldIsDirected && newIsDirected) {
						//swap signs switch if direction changed
						if (!lineDir.second) {
							lG.m_labelling[e->m_ID].negate();
							lG.m_labelling[e->first->other(e)->m_ID].negate();
							lG.m_labelling[e->second->other(e)->m_ID].negate();
						}
					} else if (oldIsDirected && !newIsDirected) {
						//we just need to remove labels of previously removed fake edges
						if (fakeID1 < fakeID2) {
							lG.m_labelling.erase(lG.m_labelling.begin() + fakeID2);
							lG.m_labelling.erase(lG.m_labelling.begin() + fakeID1);
							shifts.push_back(fakeID2);
							shifts.push_back(fakeID1);
						} else {
							lG.m_labelling.erase(lG.m_labelling.begin() + fakeID1);
							lG.m_labelling.erase(lG.m_labelling.begin() + fakeID2);
							shifts.push_back(fakeID1);
							shifts.push_back(fakeID2);
						}
					}
				}
				ret.push_back(std::move(lG));
			}

			//step
			next = false;
			for (auto& itR : mapToIts) {
				if (++itR.second.first == itR.second.second.second) {
					itR.second.first = itR.second.second.first;
				} else {
					next = true;
					break;
				}
			}
		} while(next);

		return ret;
	}

	std::vector<std::pair<labelledGraph, graphMap> >
	labelledGraph::reverseQuarkLines() const {
		auto gs = m_graph.reverseQuarkLines(graphMap(m_graph.nVerts(),
													 m_graph.nEdges()));

		//update labelling (only flip some signs)
		std::vector<std::pair<labelledGraph, graphMap> > ret;
		for (auto& g : gs) {
			graphLabelling copy = m_labelling;
			for (auto& e : g.second.getEdges()) {
				if (e.second) {
					copy[e.first].negate();
				}
			}
			ret.push_back(std::make_pair(
							  labelledGraph(std::move(g.first), std::move(copy)),
							  std::move(g.second)));
		}
		return ret;
	}

	labelledGraph labelledGraph::reverseAllQuarks() const {
		auto g = m_graph.reverseAllQuarks();
		auto copy = m_labelling;
		for (auto& e : g.getEdges()) {
			if (e->m_type == line_t::QUARK) {
				copy[e->m_ID].negate();
			}
		}
		return labelledGraph(std::move(g), std::move(copy));
	}

	std::ostream& labelledGraph::printColorFactor(std::ostream& ostream_) const {
		for (const auto& v : m_graph.getVerts()) {
			if (v->m_number < 0) {
				continue;
			}
			if (v->m_number > 0) {
				auto l = m_labelling[v->front()->m_ID];
				if (v->front()->first == v.get()) {
					l.negate();
				}
				//external
				ostream_ << "e[" << l << ',' << (*v->begin())->m_ID << ']';
				continue;
			}
			//else
			assert(v->size() == 3);
			auto l1 = (*v)[0];
			auto l2 = (*v)[1];
			auto l3 = (*v)[2];
			//all gluon
			if (l1->m_type == line_t::GLUON && l2->m_type == line_t::GLUON
				&& l3->m_type == line_t::GLUON) {
				ostream_ << "f[" << l1->m_ID << ',' << l2->m_ID << ',' << l3->m_ID << ']';
				continue;
			}
			//else
			//all quark (antiquark)
			if (l1->m_type == line_t::QUARK && l2->m_type == line_t::QUARK
				&& l3->m_type == line_t::QUARK) {
				ostream_ << "c[" << l1->getActualEdge()->m_ID << ',' << l2->getActualEdge()->m_ID
						 << ',' << l3->getActualEdge()->m_ID << ']';
				continue;
			}
			//else
			//all scalar
			if (l1->m_type == line_t::SCALAR && l2->m_type == line_t::SCALAR
				&& l3->m_type == line_t::SCALAR) {
				ostream_ << "cS[" << l1->m_ID << ',' << l2->m_ID << ',' << l3->m_ID << ']';
				continue;
			}
			//gluon-quark-antiquark / gluon-scalar scalar
			if (l1->m_type == line_t::GLUON) {
				if (l2->m_type == line_t::QUARK && l3->m_type == line_t::QUARK) {
					if (!l2.m_dir) {
						assert(l3.m_dir);
						ostream_ << "t[" << l1->m_ID << "][" << l3->getActualEdge()->m_ID
								 << ',' << l2->getActualEdge()->m_ID << ']';
					} else {
						assert(!l3.m_dir);
						ostream_ << "(-1)t[" << l1->m_ID << "][" << l2->getActualEdge()->m_ID
								 << ',' << l3->getActualEdge()->m_ID << ']';
					}
					continue;
				}
				//else
				if (l2->m_type == line_t::SCALAR && l3->m_type == line_t::SCALAR) {
					ostream_ << "tS[" << l1->m_ID << "][" << l2->m_ID << ',' << l3->m_ID << ']';
					continue;
				}
			}
			if (l2->m_type == line_t::GLUON) {
				if (l3->m_type == line_t::QUARK && l1->m_type == line_t::QUARK) {
					if (!l3.m_dir) {
						assert(l1.m_dir);
						ostream_ << "t[" << l2->m_ID << "][" << l1->getActualEdge()->m_ID
								 << ',' << l3->getActualEdge()->m_ID << ']';
					} else {
						assert(!l1.m_dir);
						ostream_ << "(-1)t[" << l2->m_ID << "][" << l3->getActualEdge()->m_ID
								 << ',' << l1->getActualEdge()->m_ID << ']';
					}
					continue;
				}
				//else
				if (l3->m_type == line_t::SCALAR && l1->m_type == line_t::SCALAR) {
					ostream_ << "tS[" << l2->m_ID << "][" << l3->m_ID << ',' << l1->m_ID << ']';
					continue;
				}
			}
			if (l3->m_type == line_t::GLUON) {
				if (l1->m_type == line_t::QUARK && l2->m_type == line_t::QUARK) {
					if (!l1.m_dir) {
						assert(l2.m_dir);
						ostream_ << "t[" << l3->m_ID << "][" << l2->getActualEdge()->m_ID
								 << ',' << l1->getActualEdge()->m_ID << ']';
					} else {
						assert(!l2.m_dir);
						ostream_ << "(-1)t[" << l3->m_ID << "][" << l1->getActualEdge()->m_ID
								 << ',' << l2->getActualEdge()->m_ID << ']';
					}
					continue;
				}
				//else
				if (l1->m_type == line_t::SCALAR && l2->m_type == line_t::SCALAR) {
					ostream_ << "tS[" << l3->m_ID << "][" << l1->m_ID << ',' << l2->m_ID << ']';
					continue;
				}
			}
			assert(false);
		}
		
		return ostream_;
	}

	const decompRules labelledGraph::neq4to2DecomposeRules(
		{
			{std::make_pair(line_t::GLUON, -1),
			 {
				 std::make_pair(ext_t::GLUON, -1),
				 std::make_pair(ext_t::QUARK, 0),
				 std::make_pair(ext_t::ANTIQUARK, 0)
			 }
			}
		}
	);

	const decompRules labelledGraph::neq4to2DecomposeRulesS(
		{
			{std::make_pair(line_t::GLUON, -1),
			 {
				 std::make_pair(ext_t::GLUON, -1),
				 std::make_pair(ext_t::SCALAR, 0)
			 }
			}
		}
	);

	const decompRules labelledGraph::neq2to1DecomposeRules(
		{
			{std::make_pair(line_t::GLUON, -1),
			 {
				 std::make_pair(ext_t::GLUON, -1),
				 std::make_pair(ext_t::QUARK, 0),
				 std::make_pair(ext_t::ANTIQUARK, 0)
			 }
			},
			{std::make_pair(line_t::QUARK, 0), {
					std::make_pair(ext_t::QUARK, 1),
					std::make_pair(ext_t::ANTIQUARK, 2)
				}
			}
		}
	);

	const decompRules labelledGraph::neq2to1DecomposeRulesS(
		{
			{std::make_pair(line_t::GLUON, -1),
			 {
				 std::make_pair(ext_t::GLUON, -1),
				 std::make_pair(ext_t::SCALAR, 0)
			 }
			},
			{std::make_pair(line_t::QUARK, 0), {
					std::make_pair(ext_t::SCALAR, 1),
					std::make_pair(ext_t::SCALAR, 2)
				}
			}
		}
	);

	const decompRules labelledGraph::undirToDirDecomposeRules(
		{
			{std::make_pair(line_t::SCALAR, 0),
			 {
				 std::make_pair(ext_t::QUARK, 0),
				 std::make_pair(ext_t::ANTIQUARK, 0)
			 }
			},
			{std::make_pair(line_t::SCALAR, 1),
			 {
				 std::make_pair(ext_t::QUARK, 1),
				 std::make_pair(ext_t::ANTIQUARK, 1)
			 }
			},
			{std::make_pair(line_t::SCALAR, 2),
			 {
				 std::make_pair(ext_t::QUARK, 2),
				 std::make_pair(ext_t::ANTIQUARK, 2)
			 }
			}
		}
	);
	
	std::pair<labelledGraph, labelledGraph> labelledGraph::jacobies(
		const graph::edge& e_, const graph::vert& v_) const {
		//**************************************************
		//first graph
		//make a copy
		auto g1 = *this;
			
		//get the involved edges and vertices
		graph::edge e1 = g1.m_graph.getEdges()[e_->m_ID].get();
		graph::vert v1 = g1.m_graph.getVerts()[v_->m_ID].get();
		e1.m_dir = e1->first == v1;
		auto v2 = e1->other(v1);
		//get the gluons
		auto gluons = v1->nextTwo(e1);
		assert(gluons.first && gluons.second);
		//get the edges on the other side
		e1.m_dir = !e1.m_dir;
		auto otherEs = v2->nextTwo(e1);
		assert(otherEs.first && otherEs.second);

		//disconnect second gluon and second otherEs from the vertex
		bool as = v1->removeEdge(gluons.second);
		assert(as);
		as = v2->removeEdge(otherEs.second);
		assert(as);

		//make the new edge (and reconnect otherEs.first)
		if (otherEs.first.m_dir) {
			e1 = g1.m_graph.replaceEdge(*otherEs.first, v1, v2, e1);
		} else {
			e1 = g1.m_graph.replaceEdge(*otherEs.first, v2, v1, e1);
		}

		//reconnect edges
		if (gluons.second.m_dir) {
			gluons.second->first = v2;
		} else {
			gluons.second->second = v2;
		}
		v2->push_back(gluons.second);//m_dir is still correct from before
		if (otherEs.second.m_dir) {
			otherEs.second->first = v1;
		} else {
			otherEs.second->second = v1;
		}
		v1->push_back(otherEs.second);//m_dir is still correct from before

		//recompute label
		e1.m_dir = true;
		if (isDirected(e1->m_type)) {
			//we also need to relabel new fake vertices
			assert(g1.m_graph.nEdges() == g1.m_labelling.size() + 2);
			g1.m_labelling.push_back(label(false));
			g1.m_labelling.push_back(label(false));
			auto ee = e1->first->other(e1);
			ee.m_dir = true;
			auto& l = g1.m_labelling.relabel(ee, ee->first);
			g1.m_labelling[e1->m_ID] = l;
			g1.m_labelling[e1->second->other(e1)->m_ID] = l;
		} else {
			g1.m_labelling.relabel(e1, e1->first);
		}

		//**************************************************
		//second graph
		//make a copy
		auto g2 = *this;
		//get the involved edges and vertices
		e1 = g2.m_graph.getEdges()[e_->m_ID].get();
		v1 = g2.m_graph.getVerts()[v_->m_ID].get();
		e1.m_dir = e1->first == v1;
		v2 = e1->other(v1);
		//get the gluons
		gluons = v1->nextTwo(e1);
		assert(gluons.first && gluons.second);
		//get the edges on the other side
		e1.m_dir = !e1.m_dir;
		otherEs = v2->nextTwo(e1);
		assert(otherEs.first && otherEs.second);

		//disconnect first gluon and second otherEs from the vertex
		as = v1->removeEdge(gluons.first);
		assert(as);
		as = v2->removeEdge(otherEs.second);
		assert(as);

		//make the new edge (and reconnect otherEs.first)
		if (otherEs.first.m_dir) {
			e1 = g2.m_graph.replaceEdge(*otherEs.first, v1, v2, e1);
		} else {
			e1 = g2.m_graph.replaceEdge(*otherEs.first, v2, v1, e1);
		}

		//reconnect edges
		if (gluons.first.m_dir) {
			gluons.first->first = v2;
		} else {
			assert(gluons.first->second == v1);
			gluons.first->second = v2;
		}
		v2->push_back(gluons.first);//m_dir is still correct from before
		if (otherEs.second.m_dir) {
			otherEs.second->first = v1;
		} else {
			assert(otherEs.second->second == v2);
			otherEs.second->second = v1;
		}
		v1->push_back(otherEs.second);//m_dir is still correct from before

		//recompute label
		e1.m_dir = true;
		if (isDirected(e1->m_type)) {
			//we also need to relabel new fake vertices
			assert(g2.m_graph.nEdges() == g2.m_labelling.size() + 2);
			g2.m_labelling.push_back(label(false));
			g2.m_labelling.push_back(label(false));
			auto ee = e1->first->other(e1);
			ee.m_dir = true;
			auto& l = g2.m_labelling.relabel(ee, ee->first);
			g2.m_labelling[e1->m_ID] = l;
			g2.m_labelling[e1->second->other(e1)->m_ID] = l;
		} else {
			g2.m_labelling.relabel(e1, e1->first);
		}

		return std::make_pair(std::move(g1), std::move(g2));
	}

	std::pair<labelledGraph, labelledGraph> labelledGraph::jacobies2Dir(
		const graph::edge& e_, const graph::vert& v_) const {
		//**************************************************
		//first graph
		//make a copy
		auto g1 = *this;
			
		//get the involved edges and vertices
		graph::edge e1 = g1.m_graph.getEdges()[e_->m_ID].get();
		graph::vert v1 = g1.m_graph.getVerts()[v_->m_ID].get();
		e1.m_dir = (e1->first == v1);

		//get the two other matter legs
		auto e1s = v1->nextTwo(e1); 
		assert(e1s.first && e1s.second);

		auto vFake1 = e1->other(v1);
		auto e1Middle = vFake1->other(e1);
		auto vFake2 = e1Middle->other(vFake1);
		auto e1Fake2 = vFake2->other(e1Middle);
		auto v2 = e1Fake2->other(vFake2);
		
		//get the edges on the other side
		e1Fake2.m_dir = (e1Fake2->first == v2);
		auto e2s = v2->nextTwo(e1Fake2);
		assert(e2s.first && e2s.second);

		//disconnect second e1s and second e2s from the vertex
		bool as = v1->removeEdge(e1s.second);
		assert(as);
		as = v2->removeEdge(e2s.second);
		assert(as);

		//make the new edge
		if (e2s.second->m_type == line_t::GLUON) {
			if (e1s.first.m_dir) {
				e1Middle = g1.m_graph.replaceEdge(*e1s.first, v2, v1, e1Middle);
			} else {
				e1Middle = g1.m_graph.replaceEdge(*e1s.first, v1, v2, e1Middle);
			}
		} else {
			if (e1s.second.m_dir) {
				e1Middle = g1.m_graph.replaceEdge(*e1s.second, v1, v2, e1Middle);
			} else {
				e1Middle = g1.m_graph.replaceEdge(*e1s.second, v2, v1, e1Middle);
			}
		}

		//reconnect edges
		if (e1s.second.m_dir) {
			e1s.second->first = v2;
		} else {
			e1s.second->second = v2;
		}
		v2->push_back(e1s.second);//m_dir is still correct from before
		if (e2s.second.m_dir) {
			e2s.second->first = v1;
		} else {
			e2s.second->second = v1;
		}
		v1->push_back(e2s.second);//m_dir is still correct from before

		//recompute label
		e1Middle.m_dir = true;
		//we also need to relabel fake vertices
		e1 = e1Middle->first->other(e1Middle);
		e1.m_dir = true;
		auto& l = g1.m_labelling.relabel(e1, e1->first);
		g1.m_labelling[e1Middle->m_ID] = l;
		g1.m_labelling[e1Middle->second->other(e1Middle)->m_ID] = l;

		//**************************************************
		//second graph
		//make a copy
		auto g2 = *this;
		
		//get the involved edges and vertices
		e1 = g2.m_graph.getEdges()[e_->m_ID].get();
		v1 = g2.m_graph.getVerts()[v_->m_ID].get();
		e1.m_dir = e1->first == v1;
		
		//get the two other matter legs
		e1s = v1->nextTwo(e1);
		assert(e1s.first && e1s.second);

		vFake1 = e1->other(v1);
		e1Middle = vFake1->other(e1);
		vFake2 = e1Middle->other(vFake1);
		e1Fake2 = vFake2->other(e1Middle);
		v2 = e1Fake2->other(vFake2);
		
		//get the edges on the other side
		e1Fake2.m_dir = (e1Fake2->first == v2);
		e2s = v2->nextTwo(e1Fake2);
		assert(e2s.first && e2s.second);

		//disconnect first e1s and second e2s from the vertex
		as = v1->removeEdge(e1s.first);
		assert(as);
		as = v2->removeEdge(e2s.second);
		assert(as);

		//make the new edge
		if (e2s.second->m_type == line_t::GLUON) {
			if (e1s.second.m_dir) {
				e1Middle = g2.m_graph.replaceEdge(*e1s.second, v2, v1, e1Middle);
			} else {
				e1Middle = g2.m_graph.replaceEdge(*e1s.second, v1, v2, e1Middle);
			}
		} else {
			if (e1s.first.m_dir) {
				e1Middle = g2.m_graph.replaceEdge(*e1s.first, v1, v2, e1Middle);
			} else {
				e1Middle = g2.m_graph.replaceEdge(*e1s.first, v2, v1, e1Middle);
			}
		}

		//reconnect edges
		if (e1s.first.m_dir) {
			e1s.first->first = v2;
		} else {
			e1s.first->second = v2;
		}
		v2->push_back(e1s.first);//m_dir is still correct from before
		if (e2s.second.m_dir) {
			e2s.second->first = v1;
		} else {
			e2s.second->second = v1;
		}
		v1->push_back(e2s.second);//m_dir is still correct from before

		//recompute label
		e1Middle.m_dir = true;
		//we also need to relabel fake vertices
		e1 = e1Middle->first->other(e1Middle);
		e1.m_dir = true;
		auto& l2 = g2.m_labelling.relabel(e1, e1->first);
		g2.m_labelling[e1Middle->m_ID] = l2;
		g2.m_labelling[e1Middle->second->other(e1Middle)->m_ID] = l2;

		return std::make_pair(std::move(g1), std::move(g2));
	}

	std::pair<labelledGraph, labelledGraph> labelledGraph::jacobies2Undir(
		const graph::edge& e_, const graph::vert& v_) const {
		//**************************************************
		//first graph
		//make a copy
		auto g1 = *this;
			
		//get the involved edges and vertices
		graph::edge e1 = g1.m_graph.getEdges()[e_->m_ID].get();
		graph::vert v1 = g1.m_graph.getVerts()[v_->m_ID].get();
		e1.m_dir = (e1->first == v1);
		auto v2 = e1->other(v1);
		//get the two other matter legs
		auto e1s = v1->nextTwo(e1); 
		assert(e1s.first && e1s.second);
		//get the edges on the other side
		e1.m_dir = !e1.m_dir;
		auto e2s = v2->nextTwo(e1);
		assert(e2s.first && e2s.second);

		//disconnect second e1s and second e2s from the vertex
		bool as = v1->removeEdge(e1s.second);
		assert(as);
		as = v2->removeEdge(e2s.second);
		assert(as);

		//make the new edge
		if (e2s.second->m_type == line_t::GLUON) {
			if (e1s.first.m_dir) {
				e1 = g1.m_graph.replaceEdge(*e1s.first, v2, v1, e1);
			} else {
				e1 = g1.m_graph.replaceEdge(*e1s.first, v1, v2, e1);
			}
		} else {
			if (e1s.second.m_dir) {
				e1 = g1.m_graph.replaceEdge(*e1s.second, v1, v2, e1);
			} else {
				e1 = g1.m_graph.replaceEdge(*e1s.second, v2, v1, e1);
			}
		}

		//reconnect edges
		if (e1s.second.m_dir) {
			e1s.second->first = v2;
		} else {
			e1s.second->second = v2;
		}
		v2->push_back(e1s.second);//m_dir is still correct from before
		if (e2s.second.m_dir) {
			e2s.second->first = v1;
		} else {
			e2s.second->second = v1;
		}
		v1->push_back(e2s.second);//m_dir is still correct from before

		//recompute label
		e1.m_dir = true;
		if (isDirected(e1->m_type)) {
			//we also need to relabel fake vertices
			auto ee = e1->first->other(e1);
			ee.m_dir = true;
			auto& l = g1.m_labelling.relabel(ee, ee->first);
			g1.m_labelling[e1->m_ID] = l;
			g1.m_labelling[e1->second->other(e1)->m_ID] = l;
		} else {
			g1.m_labelling.relabel(e1, e1->first);
		}

		//**************************************************
		//second graph
		//make a copy
		auto g2 = *this;
		//get the involved edges and vertices
		e1 = g2.m_graph.getEdges()[e_->m_ID].get();
		v1 = g2.m_graph.getVerts()[v_->m_ID].get();
		e1.m_dir = e1->first == v1;
		v2 = e1->other(v1);
		//get the two other matter legs
		e1s = v1->nextTwo(e1);
		assert(e1s.first && e1s.second);
		//get the edges on the other side
		e1.m_dir = !e1.m_dir;
		e2s = v2->nextTwo(e1);
		assert(e2s.first && e2s.second);

		//disconnect first e1s and second e2s from the vertex
		as = v1->removeEdge(e1s.first);
		assert(as);
		as = v2->removeEdge(e2s.second);
		assert(as);

		//make the new edge
		if (e2s.second->m_type == line_t::GLUON) {
			if (e1s.second.m_dir) {
				e1 = g2.m_graph.replaceEdge(*e1s.second, v2, v1, e1);
			} else {
				e1 = g2.m_graph.replaceEdge(*e1s.second, v1, v2, e1);
			}
		} else {
			if (e1s.first.m_dir) {
				e1 = g2.m_graph.replaceEdge(*e1s.first, v1, v2, e1);
			} else {
				e1 = g2.m_graph.replaceEdge(*e1s.first, v2, v1, e1);
			}
		}

		//reconnect edges
		if (e1s.first.m_dir) {
			e1s.first->first = v2;
		} else {
			e1s.first->second = v2;
		}
		v2->push_back(e1s.first);//m_dir is still correct from before
		if (e2s.second.m_dir) {
			e2s.second->first = v1;
		} else {
			e2s.second->second = v1;
		}
		v1->push_back(e2s.second);//m_dir is still correct from before

		//recompute label
		e1.m_dir = true;
		if (isDirected(e1->m_type)) {
			//we also need to relabel fake vertices
			auto ee = e1->first->other(e1);
			ee.m_dir = true;
			auto& l = g2.m_labelling.relabel(ee, ee->first);
			g2.m_labelling[e1->m_ID] = l;
			g2.m_labelling[e1->second->other(e1)->m_ID] = l;
		} else {
			g2.m_labelling.relabel(e1, e1->first);
		}

		return std::make_pair(std::move(g1), std::move(g2));
	}

	std::pair<bool, labelledGraph> labelledGraph::twoTerm(const graph::edge& e_) const {
		//make a copy
		auto g = *this;
		graph::edge e(g.getGraph().getEdges()[e_->m_ID].get(), true);
		auto v1 = e->first, v2 = e->second;
		auto e1s = v1->nextTwo(e);
		assert(e1s.first && e1s.second);
		e.m_dir = false;
		auto e2s = v2->nextTwo(e);
		assert(e2s.first && e2s.second);

		bool e11Incoming = e1s.first->second == v1;
		bool e21Incoming = e2s.first->second == v2;

		if (e11Incoming == e21Incoming) {
			//plus/t-channel case
			//disconnect e11 and e21
			bool as = v1->removeEdge(e1s.first);
			assert(as);
			as = v2->removeEdge(e2s.first);
			assert(as);

			//reconnect at correct place
			auto insertPosIt = std::find(v1->begin(), v1->end(), e);
			assert(insertPosIt != v1->end());
			v1->insert(insertPosIt, e2s.first);
			if (e2s.first.m_dir) {
				e2s.first->first = v1;
			} else {
				e2s.first->second = v1;
			}

			insertPosIt = std::find(v2->begin(), v2->end(), e);
			assert(insertPosIt != v2->end());
			v2->insert(insertPosIt, e1s.first);
			if (e1s.first.m_dir) {
				e1s.first->first = v2;
			} else {
				e1s.first->second = v2;
			}
			//recompute label
			assert(!isDirected(e->m_type));
			e.m_dir = true;
			g.m_labelling.relabel(e, e->first);

			return std::make_pair(false, std::move(g));
		}
		//else
		//minus/u-channel case
		//disconnect e11 and e22
		bool as = v1->removeEdge(e1s.first);
		assert(as);
		as = v2->removeEdge(e2s.second);
		assert(as);

		//reconnect at correct place
		auto insertPosIt = std::find(v1->begin(), v1->end(), e1s.second);
		assert(insertPosIt != v1->end());
		v1->insert(insertPosIt, e2s.second);
		if (e2s.second.m_dir) {
			e2s.second->first = v1;
		} else {
			e2s.second->second = v1;
		}
		
		insertPosIt = std::find(v2->begin(), v2->end(), e2s.first);
		assert(insertPosIt != v2->end());
		v2->insert(insertPosIt, e1s.first);
		if (e1s.first.m_dir) {
			e1s.first->first = v2;
		} else {
			e1s.first->second = v2;
		}

		//recompute label
		assert(!isDirected(e->m_type));
		e.m_dir = true;
		g.m_labelling.relabel(e, e->first);

		return std::make_pair(true, std::move(g));
	}

	std::pair<bool, labelledGraph> labelledGraph::twoTermFl(
		const graph::edge& e_, const flavor_t& fl_) const {
		//**************************************************
		//first graph
		//make a copy
		auto g = *this;
			
		//get the involved edges and vertices
		graph::edge e1(g.m_graph.getEdges()[e_->m_ID].get(), true);
		auto v1 = e1->first;
		auto v2 = e1->second;
		//get the first quark-antiquark-pair
		auto greenEs = v1->nextTwo(e1);
		assert(greenEs.first && greenEs.second);
		//get the second quark-antiquark-pair
		e1.m_dir = false;
		auto blueEs = v2->nextTwo(e1);
		assert(blueEs.first && blueEs.second);

		//get directions
		bool greenInc = (greenEs.second->second == v1);
		bool blueInc = (blueEs.first->second == v2);

		if (greenInc == blueInc) {
			//positive/t-channel case
			//disconnect both second legs
			bool as = v1->removeEdge(greenEs.second);
			assert(as);
			as = v2->removeEdge(blueEs.second);
			assert(as);

			//make the new edge (and reconnect otherEs.first)
			if (greenInc) {
				e1 = g.m_graph.replaceEdge(graph::edge_t(fl_, line_t::QUARK), v1, v2, e1);
			} else {
				e1 = g.m_graph.replaceEdge(graph::edge_t(fl_, line_t::QUARK), v2, v1, e1);
			}
			//reconnect edges
			if (greenEs.second->first == v1) {
				greenEs.second->first = v2;
			} else {
				assert(greenEs.second->second == v1);
				greenEs.second->second = v2;
			}
			v2->push_back(greenEs.second);
			if (blueEs.second->first == v2) {
				blueEs.second->first = v1;
			} else {
				assert(blueEs.second->second == v2);
				blueEs.second->second = v1;
			}
			v1->push_back(blueEs.second);

			//recompute label
			assert(isDirected(e1->m_type));
			e1.m_dir = true;
			//we also need to relabel new fake vertices
			assert(g.m_graph.nEdges() == g.m_labelling.size() + 2);
			g.m_labelling.push_back(label(false));
			g.m_labelling.push_back(label(false));
			auto ee = e1->first->other(e1);
			ee.m_dir = true;
			auto& l = g.m_labelling.relabel(ee, ee->first);
			g.m_labelling[e1->m_ID] = l;
			g.m_labelling[e1->second->other(e1)->m_ID] = l;

			
			return std::make_pair(false, std::move(g));
		}
		//negative/u-channel case

		//disconnect first green and second blue
		bool as = v1->removeEdge(greenEs.first);
		assert(as);
		as = v2->removeEdge(blueEs.second);
		assert(as);

		//make the new edge (and reconnect otherEs.first)
		if (greenInc) {
			e1 = g.m_graph.replaceEdge(graph::edge_t(fl_, line_t::QUARK), v2, v1, e1);
		} else {
			e1 = g.m_graph.replaceEdge(graph::edge_t(fl_, line_t::QUARK), v1, v2, e1);
		}
		
		//reconnect edges
		if (greenEs.first->first == v1) {
			greenEs.first->first = v2;
		} else {
			assert(greenEs.first->second == v1);
			greenEs.first->second = v2;
		}
		v2->push_back(greenEs.first);
		if (blueEs.second->first == v2) {
			blueEs.second->first = v1;
		} else {
			assert(blueEs.second->second == v2);
			blueEs.second->second = v1;
		}
		v1->push_back(blueEs.second);

		//recompute label
		assert(isDirected(e1->m_type));
		e1.m_dir = true;
		//we also need to relabel new fake vertices
		assert(g.m_graph.nEdges() == g.m_labelling.size() + 2);
		g.m_labelling.push_back(label(false));
		g.m_labelling.push_back(label(false));
		auto ee = e1->first->other(e1);
		ee.m_dir = true;
		auto& l = g.m_labelling.relabel(ee, ee->first);
		g.m_labelling[e1->m_ID] = l;
		g.m_labelling[e1->second->other(e1)->m_ID] = l;

		return std::make_pair(true, std::move(g));
	}

	std::pair<labelledGraph, labelledGraph>
	labelledGraph::scalarTwoTerm(const graph::edge& e_) const {
		//first graph
		//make a copy
		auto g1 = *this;
		graph::edge e(g1.getGraph().getEdges()[e_->m_ID].get(), true);
		auto v1 = e->first, v2 = e->second;
		auto e1s = v1->nextTwo(e);
		assert(e1s.first && e1s.second);
		e.m_dir = false;
		auto e2s = v2->nextTwo(e);
		assert(e2s.first && e2s.second);

		//disconnect e11 and e21
		bool as = v1->removeEdge(e1s.first);
		assert(as);
		as = v2->removeEdge(e2s.first);
		assert(as);

		//reconnect at correct place
		auto insertPosIt = std::find(v1->begin(), v1->end(), e);
		assert(insertPosIt != v1->end());
		v1->insert(insertPosIt, e2s.first);
		if (e2s.first.m_dir) {
			e2s.first->first = v1;
		} else {
			e2s.first->second = v1;
		}

		insertPosIt = std::find(v2->begin(), v2->end(), e);
		assert(insertPosIt != v2->end());
		v2->insert(insertPosIt, e1s.first);
		if (e1s.first.m_dir) {
			e1s.first->first = v2;
		} else {
			e1s.first->second = v2;
		}

		//recompute label
		assert(!isDirected(e->m_type));
		e.m_dir = true;
		g1.m_labelling.relabel(e, e->first);

		//second graph
		//make a copy
		auto g2 = *this;
		e = graph::edge(g2.getGraph().getEdges()[e_->m_ID].get(), true);
		v1 = e->first;
		v2 = e->second;
		e1s = v1->nextTwo(e);
		assert(e1s.first && e1s.second);
		e.m_dir = false;
		e2s = v2->nextTwo(e);
		assert(e2s.first && e2s.second);

		//disconnect e11 and e22
		as = v1->removeEdge(e1s.first);
		assert(as);
		as = v2->removeEdge(e2s.second);
		assert(as);

		//reconnect at correct place
		insertPosIt = std::find(v1->begin(), v1->end(), e1s.second);
		assert(insertPosIt != v1->end());
		v1->insert(insertPosIt, e2s.second);
		//this order of checking should ensure that tadpoles are handled correctly
		if (e2s.second.m_dir) {
			e2s.second->first = v1;
		} else {
			e2s.second->second = v1;
		}

		insertPosIt = std::find(v2->begin(), v2->end(), e2s.first);
		assert(insertPosIt != v2->end());
		v2->insert(insertPosIt, e1s.first);
		//this order of checking should ensure that tadpoles are handled correctly
		if (e1s.first.m_dir) {
			e1s.first->first = v2;
		} else {
			e1s.first->second = v2;
		}
		//recompute label
		assert(!isDirected(e->m_type));
		e.m_dir = true;
		g2.m_labelling.relabel(e, e->first);
		
		return std::make_pair(std::move(g1), std::move(g2));
	}

	std::pair<labelledGraph, labelledGraph> labelledGraph::scalarTwoTermFl(
		const graph::edge& e_, const flavor_t& fl_) const {
		//**************************************************
		//first graph
		//make a copy
		auto g1 = *this;
			
		//get the involved edges and vertices
		graph::edge e1(g1.m_graph.getEdges()[e_->m_ID].get(),true);
		auto v1 = e1->first;
		auto v2 = e1->second;
		//get the first edges
		auto greenEs = v1->nextTwo(e1);
		assert(greenEs.first && greenEs.second);
		e1.m_dir = false;
		//get the edges on the other side
		auto blueEs = v2->nextTwo(e1);
		assert(blueEs.first && blueEs.second);

		//disconnect both second legs
		bool as = v1->removeEdge(greenEs.second);
		assert(as);
		as = v2->removeEdge(blueEs.second);
		assert(as);

		//make the new edge (and reconnect otherEs.first)
		e1 = g1.m_graph.replaceEdge(graph::edge_t(fl_, line_t::SCALAR), v1, v2, e1);

		//reconnect edges
		if (greenEs.second.m_dir) {
			greenEs.second->first = v2;
		} else {
			greenEs.second->second = v2;
		}
		v2->push_back(greenEs.second);
		if (blueEs.second.m_dir) {
			blueEs.second->first = v1;
		} else {
			blueEs.second->second = v1;
		}
		v1->push_back(blueEs.second);

		//recompute label
		e1.m_dir = true;
		g1.m_labelling.relabel(e1, e1->first);

		//**************************************************
		//second graph
		//make a copy
		auto g2 = *this;
		//get the involved edges and vertices
		e1 = graph::edge(g2.m_graph.getEdges()[e_->m_ID].get(), true);
		v1 = e1->first;
		v2 = e1->second;
		//get the first edges
		greenEs = v1->nextTwo(e1);
		assert(greenEs.first && greenEs.second);
		e1.m_dir = false;
		//get the edges on the other side
		blueEs = v2->nextTwo(e1);
		assert(blueEs.first && blueEs.second);

		//disconnect first green and second blue from the vertex
		as = v1->removeEdge(greenEs.first);
		assert(as);
		as = v2->removeEdge(blueEs.second);
		assert(as);

		//make the new edge (and reconnect otherEs.first)
		e1 = g2.m_graph.replaceEdge(graph::edge_t(fl_, line_t::SCALAR), v1, v2, e1);

		//reconnect edges
		if (greenEs.first.m_dir) {
			greenEs.first->first = v2;
		} else {
			greenEs.first->second = v2;
		}
		v2->push_back(greenEs.first);
		if (blueEs.second.m_dir) {
			blueEs.second->first = v1;
		} else {
			blueEs.second->second = v1;
		}
		v1->push_back(blueEs.second);

		//recompute label
		e1.m_dir = true;
		g2.m_labelling.relabel(e1, e1->first);

		return std::make_pair(std::move(g1), std::move(g2));
	}

	void labelledGraph::connect(labelledGraph&& other_) {
		//connect the graph (and delete edges from graphLabelling)
		m_graph.connect(std::move(other_.m_graph), other_.makeDeleter());
		//join the graphLabellings
		m_labelling.insert(m_labelling.end(),
						   std::make_move_iterator(other_.m_labelling.begin()),
						   std::make_move_iterator(other_.m_labelling.end()));
		//TODO: also upgrade main labels!!!
	}

	labelledGraphs::labelledGraphs(const graphs& graphs_, const bool& useExtNumbers_) {
		for (const auto& g : graphs_) {
			insert(end(), labelledGraph(g, useExtNumbers_));
		}
	}

	labelledGraphs::labelledGraphs(graphs&& graphs_, const bool& useExtNumbers_) {
		for (auto& g : graphs_) {
			insert(end(), labelledGraph(std::move(g), useExtNumbers_));
		}
	}

	labelledGraphs::labelledGraphs(const std::vector<flExt>& exts_,
								   const unsigned int& nLoops_,
								   const std::vector<coupling>& couplings_,
								   const bool& useExtNumbers_, const std::size_t& exactNV_) {
		graphs gs(exts_, nLoops_, couplings_, exactNV_);
		for (auto& g : gs) {
			insert(end(), labelledGraph(std::move(g), useExtNumbers_));
		}
	}

	labelledGraphs labelledGraphs::makePMEFTGraphs(const std::size_t& order_, const std::size_t& nBH_,
												   const bool& higherWLCoupling_) {
		auto gs = graphs::makePMEFTGraphs(order_, nBH_, higherWLCoupling_);
		labelledGraphs lGs;
		//make the labelling
		for (auto g : gs) {
			std::vector<label> labels;
			for (std::size_t i = 0, size = g.nEdges(); i != size; ++i) {
				labels.push_back(label(i+1, 1));
			}
			lGs.insert(labelledGraph(std::move(g),
									 graphLabelling(std::move(labels), {})));
		}
		return lGs;
	}

	labelledGraphs labelledGraphs::makePMEFTGraphsInIn(const std::size_t& order_, const std::size_t& nBH_,
													   const bool& higherWLCoupling_) {
		auto gs = graphs::makePMEFTGraphsInIn(order_, nBH_, higherWLCoupling_);
		labelledGraphs lGs;
		//make the labelling graph by graph
		for (auto g : gs) {
			std::vector<label> labels(g.nEdges(), label());
			if (labels.empty()) {
				lGs.insert(labelledGraph(std::move(g), graphLabelling(std::move(labels), {})));
				continue;
			}
			//find starting vertex (i.e. the one where all arrows point into)
			auto& vs = g.getVerts();
			auto itV = vs.begin();
			for(const auto end = vs.end(); itV != end; ++itV) {
				bool allIncoming = true;
				for (const auto& e : **itV) {
					if (e->first == itV->get()) {
						allIncoming = false;
						break;
					}
				}
				if (allIncoming) {
					break;
				}
			}
			assert(itV != vs.end());
			canonGraphRunner r(itV->get());
			std::size_t counter = 1;
			while (r.next()) {
				//first fake edge
				auto e = r.prevEdge();
				assert(!e->isActualEdge());
				labels[e->m_ID] = label(counter, 1);
				//actual edge
				r.next();
				assert(r.isRunning());
				e = r.prevEdge();
				assert(e->isActualEdge());
				labels[e->m_ID] = label(counter, 1);
				//second fake edge
				r.next();
				assert(r.isRunning());
				e = r.prevEdge();
				assert(!e->isActualEdge());
				labels[e->m_ID] = label(counter, 1);
				//step
				++counter;
			}
			lGs.insert(labelledGraph(std::move(g), graphLabelling(std::move(labels), {})));
		}

		return lGs;
	}

	labelledGraphs labelledGraphs::makePMEFTRadGraphs(const std::size_t& order_, const std::size_t& nBH_,
													  const bool& higherWLCoupling_) {
		auto gs = graphs::makePMEFTRadGraphs(order_, nBH_, higherWLCoupling_);
		labelledGraphs lGs;
		//make the labelling graph by graph
		for (auto g : gs) {
			std::vector<label> labels(g.nEdges(), label());
			if (labels.empty()) {
				lGs.insert(labelledGraph(std::move(g), graphLabelling(std::move(labels), {})));
				continue;
			}
			//find starting vertex (i.e. the one where all arrows point into)
			auto& vs = g.getVerts();
			auto itV = vs.begin();
			for(const auto end = vs.end(); itV != end; ++itV) {
				bool allIncoming = true;
				for (const auto& e : **itV) {
					if (e->first == itV->get()) {
						allIncoming = false;
						break;
					}
				}
				if (allIncoming) {
					break;
				}
			}
			assert(itV != vs.end());
			canonGraphRunner r(itV->get());
			std::size_t counter = 1;
			while (r.next()) {
				//first fake edge
				auto e = r.prevEdge();
				assert(!e->isActualEdge());
				labels[e->m_ID] = label(counter, 1);
				//actual edge
				r.next();
				assert(r.isRunning());
				e = r.prevEdge();
				assert(e->isActualEdge());
				labels[e->m_ID] = label(counter, 1);
				//second fake edge
				r.next();
				assert(r.isRunning());
				e = r.prevEdge();
				assert(!e->isActualEdge());
				labels[e->m_ID] = label(counter, 1);
				//step
				++counter;
			}
			lGs.insert(labelledGraph(std::move(g), graphLabelling(std::move(labels), {})));
		}

		return lGs;
	}

	labelledGraphs labelledGraphs::makeGRWLImpulseGraphs(const std::size_t& orderG_) {
		auto gs = graphs::makeGRWLImpulseGraphs(orderG_);
		labelledGraphs lGs;
		//make the labelling
		for (auto g : gs) {
			std::vector<label> labels;
			for (const auto& e : g.getEdges()) {
				labels.push_back(label(e->getActualEdge()->m_ID+1,1));
			}
			lGs.insert(labelledGraph(std::move(g),
									 graphLabelling(std::move(labels), {})));
		}
		return lGs;
	}

	labelledGraphs labelledGraphs::makeGRWLSGraphs(const std::size_t& orderG_) {
		auto gs = graphs::makeGRWLSGraphs(orderG_);
		labelledGraphs lGs;
		//make the labelling
		for (auto g : gs) {
			std::vector<label> labels;
			for (const auto& e : g.getEdges()) {
				labels.push_back(label(e->getActualEdge()->m_ID+1,1));
			}
			lGs.insert(labelledGraph(std::move(g),
									 graphLabelling(std::move(labels), {})));
		}
		return lGs;
	}

	const labelledGraph& labelledGraphs::operator[](const std::size_t& n_) const {
		assert(n_ <= size());
		auto it = begin();
		std::advance(it, n_);
		return *it;
	}

	std::ostream& operator<<(std::ostream& ostream_, const labelledGraphs& graphs_) {
		ostream_ << '{';
		bool first = true;
		for (const auto& g : graphs_) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << g;
		}
		ostream_ << '}';
		return ostream_;
	}

	void labelledGraphs::printVerbose(std::ostream& ostream_) const {
		ostream_ << '{';
		bool first = true;
		for (const auto& g : *this) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			g.printVerbose(ostream_);
		}
		ostream_ << '}';
	}

	std::pair<std::set<labelledGraph>::const_iterator, graphMap> labelledGraphs::findMap(
		const labelledGraph& g_) const {
		//copy the graph
		graph g = g_.m_graph;
		//find the map
		auto map = g.canonMap();
		//find the iterator
		auto it = find(g);
		
		return std::make_pair(std::move(it), std::move(map));
	}

	std::pair<std::set<labelledGraph>::const_iterator, graphMap> labelledGraphs::findMap(
		labelledGraph&& g_) const {
		//find the map
		auto map = g_.m_graph.canonMap();
		//find the iterator
		auto it = find(g_);
		
		return std::make_pair(std::move(it), std::move(map));
	}

	std::pair<std::set<labelledGraph>::iterator, graphMap> labelledGraphs::findMap(
		const labelledGraph& g_) {
		//copy the graph
		graph g = g_.m_graph;
		//find the map
		auto map = g.canonMap();
		//find the iterator
		auto it = find(g);
		
		return std::make_pair(std::move(it), std::move(map));
	}

	std::pair<std::set<labelledGraph>::iterator, graphMap> labelledGraphs::findMap(
		labelledGraph&& g_) {
		//find the map
		auto map = g_.m_graph.canonMap();
		//find the iterator
		auto it = find(g_);
		
		return std::make_pair(std::move(it), std::move(map));
	}

	void labelledGraphs::printSyms(const std::string& head_, std::ostream& ostream_) const {
		ostream_ << '{';
		std::size_t counter = 0;
		for (auto& g : *this) {
			if (counter++ != 0) {
				ostream_ << ',';
			}
			g.printSyms(head_ + '[' + std::to_string(counter) + ']', ostream_);
		}
		ostream_ << '}';
	}

	void labelledGraphs::printJacobies(const std::string& head_,
									   std::ostream& ostream_) const {
		ostream_ << '{';
		std::size_t counter = 0;
		bool first = true;
		for (auto& g : *this) {
			const std::string headG = head_ + '[' + std::to_string(++counter) + ']';
			
			for (auto& pair : g.jacobies()) {
				if (first) {
					first = false;
				} else {
					ostream_ << ',';
				}

				//print g
				g.printNum(headG, ostream_);
				
				//print first jacobi factor
				auto it = findMap(pair.first);

				assert(it.first != end());
				//invert the map (we need canon -> pair.first)
				it.second.invert();
				//change sign (we subtract!)
				it.second.sign() = !it.second.sign();
				std::string headJac = head_ + '[' +
					std::to_string(std::distance(begin(), it.first)+1) + ']';
				pair.first.printNum(headJac, it.second, it.first->getMainLabels(),
								    ostream_);
				
				//print second jacobi factor
				it = findMap(pair.second);
				assert(it.first != end());
				//invert the map (we need canon -> it.second)
				it.second.invert();
				headJac = head_ + '[' +
					std::to_string(std::distance(begin(), it.first)+1) + ']';
				pair.second.printNum(headJac, it.second, it.first->getMainLabels(),
								     ostream_);
			}
		}
		ostream_ << '}';
	}

	void labelledGraphs::printTwoTerm(const std::string& head_,
									  std::ostream& ostream_) const {
		ostream_ << '{';
		std::size_t counter = 0;
		bool first = true;
		for (auto& g : *this) {
			const std::string headG = head_ + '[' + std::to_string(++counter) + ']';

			for (auto& pair : g.twoTerm()) {
				if (first) {
					first = false;
				} else {
					ostream_ << ',';
				}

				//print g
				g.printNum(headG, ostream_);
				
				//print the diagrams
				auto it = findMap(pair.second);
				assert(it.first != end());
				//invert the map (we need canon -> pair.second)
				it.second.invert();
				//change sign (there's a 2nd sign since we subtract)
				if (!pair.first) {
					it.second.sign() = !it.second.sign();
				}
				std::string headJac = head_ + '[' +
					std::to_string(std::distance(begin(), it.first)+1) + ']';
				pair.second.printNum(headJac, it.second, it.first->getMainLabels(),
								     ostream_);
			}
		}
		ostream_ << '}';
	}

	void labelledGraphs::printTwoTermFl(const std::string& head_,
										const std::array<flavor_t, 3>& fls_,
										std::ostream& ostream_) const {
		ostream_ << '{';
		std::size_t counter = 0;
		bool first = true;
		for (auto& g : *this) {
			const std::string headG = head_ + '[' + std::to_string(++counter) + ']';
			
			for (auto& pair : g.twoTermFl(fls_)) {
				if (first) {
					first = false;
				} else {
					ostream_ << ',';
				}

				//print g
				g.printNum(headG, ostream_);
				
				//print the diagrams
				auto it = findMap(pair.second);
				assert(it.first != end());
				//invert the map (we need canon -> pair.second)
				it.second.invert();
				//change sign (there's a 2nd sign since we subtract)
				if (!pair.first) {
					it.second.sign() = !it.second.sign();
				}
				std::string headJac = head_ + '[' +
					std::to_string(std::distance(begin(), it.first)+1) + ']';
				pair.second.printNum(headJac, it.second, it.first->getMainLabels(),
								     ostream_);
			}
		}
		ostream_ << '}';
	}

	void labelledGraphs::printScalarTwoTerm(const std::string& head_,
											std::ostream& ostream_) const {
		ostream_ << '{';
		std::size_t counter = 0;
		bool first = true;
		for (auto& g : *this) {
			const std::string headG = head_ + '[' + std::to_string(++counter) + ']';
			
			for (auto& pair : g.scalarTwoTerm()) {
				if (first) {
					first = false;
				} else {
					ostream_ << ',';
				}

				//print g
				g.printNum(headG, ostream_);
				
				//print first graph
				auto it = findMap(pair.first);
				assert(it.first != end());
				//invert the map (we need canon -> pair.first)
				it.second.invert();
				//change sign (we subtract!)
				it.second.sign() = !it.second.sign();
				std::string headJac = head_ + '[' +
					std::to_string(std::distance(begin(), it.first)+1) + ']';
				pair.first.printNum(headJac, it.second, it.first->getMainLabels(),
								    ostream_);

				//print second graph
				it = findMap(pair.second);
				assert(it.first != end());
				//invert the map (we need canon -> it.second)
				it.second.invert();
				headJac = head_ + '[' +
					std::to_string(std::distance(begin(), it.first)+1) + ']';
				pair.second.printNum(headJac, it.second, it.first->getMainLabels(),
								     ostream_);
			}
		}
		ostream_ << '}';
	}

	void labelledGraphs::printScalarTwoTermFl(const std::string& head_,
											  const std::array<flavor_t, 3>& fls_,
											  std::ostream& ostream_) const {
		ostream_ << '{';
		std::size_t counter = 0;
		bool first = true;
		for (auto& g : *this) {
			const std::string headG = head_ + '[' + std::to_string(++counter) + ']';
			
			for (auto& pair : g.scalarTwoTermFl(fls_)) {
				if (first) {
					first = false;
				} else {
					ostream_ << ',';
				}

				//print g
				g.printNum(headG, ostream_);
				
				//print first graph
				auto it = findMap(pair.first);
				assert(it.first != end());
				//invert the map (we need canon -> pair.first)
				it.second.invert();
				//change sign (we subtract!)
				it.second.sign() = !it.second.sign();
				std::string headJac = head_ + '[' +
					std::to_string(std::distance(begin(), it.first)+1) + ']';
				pair.first.printNum(headJac, it.second, it.first->getMainLabels(),
								    ostream_);

				//print second graph
				it = findMap(pair.second);
				assert(it.first != end());
				//invert the map (we need canon -> it.second)
				it.second.invert();
				headJac = head_ + '[' +
					std::to_string(std::distance(begin(), it.first)+1) + ']';
				pair.second.printNum(headJac, it.second, it.first->getMainLabels(),
								     ostream_);
			}
		}
		ostream_ << '}';
	}

	void labelledGraphs::printFlavorSyms(const std::string& head_, const std::vector<flavor_t>& fls_,
										 std::ostream& ostream_, const bool& onlyTruePerms_) const {
		ostream_ << '{';
		std::size_t counter = 0;
		bool first = true;
		for (auto& g : *this) {
			const std::string headG = head_ + '[' + std::to_string(++counter) + ']';
			
			//get flavors contained in this graph
			std::vector<flavor_t> gFls;
			for (auto& fl : fls_) {
				if (g.containsFlavor(fl)) {
					gFls.push_back(fl);
				}
			}

			if (gFls.empty()) {
				continue;
			}
			const auto size = gFls.size();

			runner<std::vector<std::vector<flavor_t>::const_iterator> >* r;
			if (onlyTruePerms_) {
				r = new orderedChooseRunner(makeOrderedChooseRunner(fls_.begin(), fls_.end(), gFls.size()));
			} else {
				r = new rangeRunner(makeRangeRunner(gFls.size(), fls_.begin(), fls_.end()));
			}


			do {
				const auto& current = r->getCurrent();
				//construct the map
				std::map<flavor_t, flavor_t> map;
				for (std::size_t i = 0; i != size; ++i) {
					auto pair = std::make_pair(gFls[i], *current[i]);
					if (pair.first != pair.second) {
						map.insert(std::move(pair));
					}
				}
				//skip over identity permutation
				if (map.empty()) {
					continue;
				}
				//make a copy and apply the map
				auto gCopy = g;
				gCopy.permuteFlavors(map);
				

				//find the map (if valid graph)
				auto it = findMap(gCopy);
				if (it.first == end()) {
					continue;
				}

				//print
				if (first) {
					first = false;
				} else {
					ostream_ << ',';
				}
				//print g
				g.printNum(headG, ostream_);
				
				//invert the map (we need canon -> pair.first)
				it.second.invert();
				//change sign (we subtract!)
				it.second.sign() = !it.second.sign();
				//print the copy
				std::string headGCopy = head_ + '[' +
					std::to_string(std::distance(begin(), it.first)+1) + ']';
				gCopy.printNum(headGCopy, it.second, it.first->getMainLabels(),
							   ostream_);
			} while (r->next());
		}

		ostream_ << '}';
	}

	void labelledGraphs::printNeq4DecompID(const std::string& head_,
										   const labelledGraph& g_,
										   std::ostream& ostream_) const {
		auto gs = g_.decompose(labelledGraph::neq4to2DecomposeRules,
							   {coupling::THREEGLUON, coupling::QUARKGLUON});
		for (auto& g : gs) {
			auto it = findMap(g);

			assert(it.first != end());
			//invert the map (we need canon -> g)
			it.second.invert();
			std::string head = head_ + '[' +
				std::to_string(std::distance(begin(), it.first)+1) + ']';
			g.printNum(head, it.second, it.first->getMainLabels(), ostream_);
		}
	}

	void labelledGraphs::printNeq4DecompIDS(const std::string& head_,
											const labelledGraph& g_,
											std::ostream& ostream_) const {
		auto gs = g_.decompose(labelledGraph::neq4to2DecomposeRulesS,
							   {coupling::THREEGLUON, coupling::SCALARGLUON});
		for (auto& g : gs) {
			auto it = findMap(g);

			assert(it.first != end());
			//invert the map (we need canon -> g)
			it.second.invert();
			std::string head = head_ + '[' +
				std::to_string(std::distance(begin(), it.first)+1) + ']';
			g.printNum(head, it.second, it.first->getMainLabels(), ostream_);
		}
	}

	void labelledGraphs::printNeq2DecompID(const std::string& head_,
										   const labelledGraph& g_,
										   std::ostream& ostream_) const {
		auto gs = g_.decompose(labelledGraph::neq2to1DecomposeRules,
							   {coupling::THREEGLUON, coupling::QUARKGLUON,
								coupling::QUARKGLUON1, coupling::QUARKGLUON2,
								coupling::THREEQUARK, coupling::THREEAQUARK});
		for (auto& g : gs) {
			auto it = findMap(g);

			assert(it.first != end());
			//invert the map (we need canon -> g)
			it.second.invert();
			std::string head = head_ + '[' +
				std::to_string(std::distance(begin(), it.first)+1) + ']';
			g.printNum(head, it.second, it.first->getMainLabels(), ostream_);
		}
	}

	void labelledGraphs::printNeq2DecompIDS(const std::string& head_,
										   const labelledGraph& g_,
										   std::ostream& ostream_) const {
		auto gs = g_.decompose(labelledGraph::neq2to1DecomposeRulesS,
							   {coupling::THREEGLUON, coupling::SCALARGLUON,
								coupling::SCALARGLUON1, coupling::SCALARGLUON2,
								coupling::THREESCALAR});
		for (auto& g : gs) {
			auto it = findMap(g);

			assert(it.first != end());
			//invert the map (we need canon -> g)
			it.second.invert();
			std::string head = head_ + '[' +
				std::to_string(std::distance(begin(), it.first)+1) + ']';
			g.printNum(head, it.second, it.first->getMainLabels(), ostream_);
		}
	}

	void labelledGraphs::printReversalSyms(const std::string& head_,
										   std::ostream& ostream_) const {
		ostream_ << '{';
		std::size_t counter = 0;
		for (auto& g : *this) {
			if (counter > 0) {
				ostream_ << ',';
			}
			ostream_ << '{';
			//print g
			const std::string headG = head_ + '[' + std::to_string(++counter) + ']';
			g.printNum(headG, ostream_);
			auto gs = g.reverseQuarkLines();

			for (auto& gg : gs) {
				ostream_ << ',';
				auto it = findMap(gg.first);
				assert(it.first != end());
				//invert the map (we need canon -> gg)
				it.second.invert();
				std::string head = head_ + '[' +
					std::to_string(std::distance(begin(), it.first)+1) + ']';
				gg.first.printNum(head, it.second, it.first->getMainLabels(),
								  ostream_);
			}
			ostream_ << '}';
		}
		ostream_ << '}';
	}

	void labelledGraphs::printRevAllQuarks(const std::string& head_,
										   std::ostream& ostream_) const {
		ostream_ << '{';
		bool first = true;
		for (auto& g : *this) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}

			auto gg = g.reverseAllQuarks();
			auto it = findMap(gg);
			assert(it.first != end());
			//invert the map (we need canon -> gg)
			it.second.invert();
			std::string head = head_ + '[' +
				std::to_string(std::distance(begin(), it.first)+1) + ']';
			gg.printNum(head, it.second, it.first->getMainLabels(), ostream_);

		}
		ostream_ << '}';
	}

	void labelledGraphs::printUndirToDir(const std::string& head_, const labelledGraphs& gs_,
										 const std::vector<coupling>& couplings_,
										 std::ostream& ostream_) const {
		ostream_ << '{';
		bool first = true;
		for (auto& g : gs_) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			auto gDec = g.decompose(labelledGraph::undirToDirDecomposeRules,
									couplings_);
			for (auto& gg : gDec) {
				auto it = findMap(gg);
				assert(it.first != end());
				//invert the map (we need canon -> gg)
				it.second.invert();
				std::string head = head_ + '[' +
					std::to_string(std::distance(begin(), it.first)+1) + ']';
				gg.printNum(head, it.second, it.first->getMainLabels(), ostream_);
			}
		}
		ostream_ << '}';
	}

	std::map<labelledGraph,
			 std::vector<std::pair<labelledGraphs::const_iterator, graphMap> > >
	labelledGraphs::sortByTopo() const {
		std::map<labelledGraph, std::vector<std::pair<const_iterator, graphMap> > > ret;
		for (auto it = begin(); it != end(); ++it) {
			auto gMap = it->m_graph.toTopo();
			auto map2 = gMap.first.canonMap();
			gMap.second.concat(map2);

			auto found = ret.find(gMap.first);
			if (found != ret.end()) {
				found->second.push_back(std::make_pair(it, std::move(gMap.second)));
			} else {
				//label it
				labelledGraph lG(std::move(gMap.first));
				std::vector<std::pair<labelledGraphs::const_iterator, graphMap> > v;
				v.push_back(std::make_pair(it, std::move(gMap.second)));
				ret.insert(std::make_pair(std::move(lG),std::move(v)));
			}
		}
		return ret;
	}

	void labelledGraphs::printSortByTopo(const std::string& head_,
										 std::ostream& ostream_) const {
		auto map = sortByTopo();
		ostream_ << '{';
		bool first = true;
		for (auto& e : map) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << '{' << e.first << ",{";
			bool first2 = true;
			for (auto& itMap : e.second) {
				if (first2) {
					first2 = false;
				} else {
					ostream_ << ',';
				}

				//print the numerator
				std::string head = head_ + '[' +
					std::to_string(std::distance(begin(), itMap.first) + 1) + ']';
				e.first.printNum(head, itMap.second, itMap.first->getMainLabels(),
								 ostream_);
			}
			ostream_ << "}}";
		}
		ostream_ << '}';
	}

	void labelledGraphs::printColorFactors(std::ostream& ostream_) const {
		ostream_ << '{';
		bool first = true;
		for (auto g : *this) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			g.printColorFactor(ostream_);
		}
		ostream_ << '}';
	}

	void labelledGraphs::diffExts() {
		std::set<graph> gs;
		for (const auto& g : *this) {
			gs.insert(g.getGraph());
		}
		graphs gGs(std::move(gs));
		gGs.diffExts();
		*this = labelledGraphs(std::move(gGs), true);
	}
}
