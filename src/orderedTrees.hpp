#ifndef GKA_ORDEREDTREE_HPP
#define GKA_ORDEREDTREE_HPP

#include "ggraph_config.h"

#include <map>

#include "graphLabelling.hpp"

namespace ggraph {
	typedef std::vector<flExt> extOrdering;
	extOrdering& canonRotate(extOrdering& ordering_);

	//also rotates the second given object with the same shift using std::rotate
	//(T needs begin() and end() defined)
	template <typename T>
	extOrdering& canonRotate(extOrdering& ordering_, T& other_) {
		auto it = cyclicMinElement(ordering_);
		auto dist = it - ordering_.begin();
		std::rotate(ordering_.begin(), cyclicMinElement(ordering_),
					ordering_.end());
		auto itT = other_.begin();
		std::advance(itT, dist);
		std::rotate(other_.begin(), itT, other_.end());
		return ordering_;
	}

	//also rotates the other objects as above
	template <typename T1, typename T2>
	extOrdering& canonRotate(extOrdering& ordering_, T1& other1_, T2& other2_) {
		auto it = cyclicMinElement(ordering_);
		auto dist = it - ordering_.begin();
		std::rotate(ordering_.begin(), cyclicMinElement(ordering_),
					ordering_.end());
		
		auto itT1 = other1_.begin();
		std::advance(itT1, dist);
		std::rotate(other1_.begin(), itT1, other1_.end());
		
		auto itT2 = other2_.begin();
		std::advance(itT2, dist);
		std::rotate(other2_.begin(), itT2, other2_.end());
		
		return ordering_;
	}
	
	/**
	 * a tree graph with fixed ordering of external legs
	 * can be canonicalized and compared for the given ordering!
	 * (i.e. the canonicalize function does not(!) touch the ordering at each vertex
	 */
	class orderedTree : public graph {
		friend class orderedTrees;
	public:
		//**************************************************
		// constructor
		//**************************************************
		orderedTree(const graph& g_) : graph(g_) {}
		orderedTree(graph&& g_) : graph(std::move(g_)) {}

		//**************************************************
		// methods
		//**************************************************
		/**
		 * gets the external ordering of this graph by starting from m_verts[0]
		 * returns pairs of flExt and vertID
		 * (i.e. m_verts[0] needs to be an external, otherwise error!)
		 */
		std::vector<std::pair<flExt, std::size_t> > getExtOrdering() const;
		
		/**
		 * canonicalizes wrt to given (ext) vertex using canonicalizeOrdered
		 */
		void canonicalize(const vert& v_);

	private:
		//**************************************************
		// methods
		//**************************************************
		/**
		 * cyclically numbers the external vertices according to given numbers
		 * starts at first vertex (that needs to be an external)
		 */
		void number(const std::vector<int>& numbers_);
	};

	/**
	 * class that collects all tree graphs as a map from the external ordering
	 * to all the orderedTrees with this ordering
	 * the external ordering needs to be canonicalized (see function canonRotate)
	 */
	class orderedTrees : std::map<extOrdering, std::set<orderedTree> > {
	public:
		//**************************************************
		// constructors
		//**************************************************
		orderedTrees(const std::vector<coupling>& couplings_)
			: m_couplings(couplings_) {}

		//**************************************************
		// methods
		//**************************************************
		friend std::ostream& operator<<(std::ostream& ostream_,
										const orderedTrees& trees_);

		/**
		 * retrieves the trees for the given external ordering and numbers
		 * if not there, the trees will be computed and added first
		 * extOrdering needs to be canonicalized
		 */
		std::vector<labelledGraph> get(const extOrdering& extOrdering_,
									   const std::vector<int>& numbers_);

	private:
		//**************************************************
		// members
		//**************************************************
		std::vector<coupling> m_couplings;
		
		//**************************************************
		// methods
		//**************************************************
		/**
		 * computes the trees for the given ordering and and adds them to *this
		 * (if not already there)
		 * returns iterator to (newly inserted) element
		 */
		const mapped_type& add(extOrdering extOrdering_);
		
		/**
		 * constructs all ordered trees from the given graphs and adds all possible
		 * orderings thereof to *this
		 * undefined behaviour if not tree graphs
		 */
		void add(const graphs& gs_);
	};
}

#endif /*GKA_ORDEREDTREE_HPP*/
