#include "graphRunner.hpp"

namespace ggraph {
	graphRunner::graphRunner(const graph::vert& v_) : m_edge(nullptr), m_current(v_) {
		if (!v_) {
			runner<graph::vert>::m_isRunning = false;
			return;
		}
		m_path.push(std::make_pair(v_, 0));
		m_visited.insert(v_);
	}

	graphRunner::graphRunner(const graph::vert& v_, const graph::edge& e_)
		: m_edge(e_), m_current(v_) {
		if (!v_ || (v_->front() == e_ && v_->size() == 1)) {
			runner<graph::vert>::m_isRunning = false;
			return;
		}
		m_path.push(std::make_pair(v_, v_->front() == e_ ? 1 : 0));
		m_visited.insert(v_);
	}

	bool graphRunner::next() {
		if (!runner<graph::vert>::isRunning()) {
			return false;
		}

		// we already worked hard, and already are at the next v
		auto v = m_path.top().first;
		auto ePos = m_path.top().second;

		m_current = (*v)[ePos]->other(v);
		m_path.push(std::make_pair(m_current, m_current->front() == m_edge ? 1 : 0));
		m_visited.insert(m_current);

		v = m_path.top().first;
		ePos = m_path.top().second;

	    //to get the correct nextEdge we already move to the next vertex, going back in the stack if necessary
		while (m_path.top().second >= m_path.top().first->size()
			   || m_visited.find((*v)[ePos]->other(v)) != m_visited.end()) {
			//we visited all edges starting from this vertex
			if (m_path.top().second >= m_path.top().first->size()) {
				m_path.pop();
				if (m_path.empty()) {
					runner<graph::vert>::m_isRunning = false;
					return true;//we are still at a real vertex, next time we will automatically return false
				}
				if ((*m_path.top().first)[++m_path.top().second] == m_edge) {
					++m_path.top().second;
				}
			} else {
				//alread visited
				//we found a back edge
				++m_path.top().second;
			}
			v = m_path.top().first;
			ePos = m_path.top().second;
		}

		return true;
	}

	/*old version
	bool graphRunner::next() {
		if (!runner<graph::vert>::isRunning()) {
			return false;
		}

		auto& v = m_path.top().first;
		auto& ePos = m_path.top().second;

		assert(ePos < v->size());
		if (ePos >= v->size()) {
			//we visited all edges starting from this vertex
			m_path.pop();
			if (m_path.empty()) {
				runner<graph::vert>::m_isRunning = false;
				return false;
			}
			if ((*m_path.top().first)[++m_path.top().second] == m_edge) {
				++m_path.top().second;
			}
			//call recursively
			return next();
		}
		//else
		auto newV = (*v)[ePos]->other(v);
		
		if (m_visited.find(newV) != m_visited.end()) {
			//alread visited
			//we found a back edge
			++m_path.top().second;
			//call recursively
			return next();
		}
		//else (not yet visited)
		m_path.push(std::make_pair(newV, newV->front() == m_edge ? 1 : 0));
		m_visited.insert(newV);

		return true;
	}
	*/
}
