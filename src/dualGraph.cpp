#include <algorithm>
#include "dualGraph.hpp"
#include "util.hpp"

namespace ggraph {
	std::ostream& operator<<(std::ostream& ostream_, const dualLine_t& type_) {
		switch(type_) {
		case dualLine_t::GLUON:
			return ostream_ << "\"gluon\"";
		case dualLine_t::QUARK:
			return ostream_ << "\"quark\"";
		case dualLine_t::ANTIQUARK:
			return ostream_ << "\"antiquark\"";
		case dualLine_t::UQUARK:
			return ostream_ << "\"uquark\"";
		case dualLine_t::SCALAR:
			return ostream_ << "\"scalar\"";
		case dualLine_t::ANTISCALAR:
			return ostream_ << "\"antiscalar\"";
		case dualLine_t::USCALAR:
			return ostream_ << "\"uscalar\"";
		}
		return ostream_;
	}

	bool operator==(const dualGraph::edge_t& left_, const dualGraph::edge_t& right_) {
		//compare properties
		if (left_.m_ID != right_.m_ID || left_.m_flavor != right_.m_flavor
			|| left_.m_type != right_.m_type) {
			return false;
		}

		//check direction
		return (left_.first->m_ID == right_.first->m_ID)
			&& (left_.second->m_ID == right_.second->m_ID);
	}

	bool operator<(const dualGraph::edge_t& left_, const dualGraph::edge_t& right_) {
		//compare properties
		if (left_.m_ID < right_.m_ID) {
			return true;
		} else if (left_.m_ID > right_.m_ID) {
			return false;
		}
		if (left_.m_flavor < right_.m_flavor) {
			return true;
		} else if (left_.m_flavor > right_.m_flavor) {
			return false;
		}
		if (left_.m_type < right_.m_type) {
			return true;
		} else if (left_.m_type > right_.m_type) {
			return false;
		}

		//compare connected vertices
		auto& l1 = left_.first->m_ID;
		auto& r1 = right_.first->m_ID;
		if (l1 < r1) {
			return true;
		} else if (l1 > r1) {
			return false;
		}

		auto& l2 = left_.second->m_ID;
		auto& r2 = right_.second->m_ID;
		if (l2 < r2) {
			return true;
		}// else if (l2 > r2) {
		//	return false;
		//}

		return false;
	}

	int dualGraph::edge_t::compare(const dualGraph::edge_t& e1_,
								   const dualGraph::edge_t& e2_) {
		if (e1_.m_type < e2_.m_type) {
			return -1;
		} else if (e1_.m_type > e2_.m_type) {
			return 1;
		}

		if (e1_.m_flavor < e2_.m_flavor) {
			return -1;
		} else if (e1_.m_flavor > e2_.m_flavor) {
			return 1;
		}

		return 0;
	}

	bool operator==(const dualGraph::vert_t& left_, const dualGraph::vert_t& right_) {
		if (left_.m_ID != right_.m_ID || left_.m_exts != right_.m_exts
			|| left_.size() != right_.size()) {
			return false;
		}

		for (auto i = 0; i != left_.size(); ++i) {
			if (left_[i]->m_ID != right_[i]->m_ID) {
				return false;
			}
		}

		return true;
	}

	bool operator<(const dualGraph::vert_t& left_, const dualGraph::vert_t& right_) {
		//compare properties
		if (left_.m_ID < right_.m_ID) {
			return true;
		} else if (left_.m_ID > right_.m_ID) {
			return false;
		}
		//(inverse) ordering (bigger ext-vectors first)
		if (left_.m_exts.size() > right_.m_exts.size()) {
			return true;
		} else if (left_.m_exts.size() < right_.m_exts.size()) {
			return false;
		}
		if (left_.m_exts < right_.m_exts) {
			return true;
		} else if (left_.m_exts < right_.m_exts) {
			return false;
		}
		//compare vertex size
		if (left_.size() < right_.size()) {
			return true;
		} else if (left_.size() > right_.size()) {
			return false;
		}

		//compare edges
		for (auto i = 0; i != left_.size(); ++i) {
			auto& l = left_[i]->m_ID;
			auto& r = right_[i]->m_ID;
			if (l < r) {
				return true;
			} else if (l > r) {
				return false;
			}
		}

		return false;
	}

	bool dualGraph::vert_t::removeEdge(const edge& e_) {
		auto it = std::find(begin(), end(), e_);
		if (it == end()) {
			return false;
		}
		//else
		erase(it);
		return true;
	}

	dualGraph::edge dualGraph::vert_t::next(const edge& e_) {
		auto it = std::find(begin(), end(), e_);
		if (it == end()) {
			return nullptr;
		}
		//else
		if (++it == end()) {
			it = begin();
		}
		return *it;
	}

	const dualGraph::edge dualGraph::vert_t::next(const edge& e_) const {
		auto it = std::find(begin(), end(), e_);
		if (it == end()) {
			return nullptr;
		}
		//else
		if (++it == end()) {
			it = begin();
		}
		return *it;
	}

	void dualGraph::vert_t::canonicalize() {
		std::rotate(m_exts.begin(), std::min_element(m_exts.begin(), m_exts.end()),
					m_exts.end());
	}

	bool operator==(const dualGraph& left_, const dualGraph& right_) {
		//compare sizes
		if (left_.m_verts.size() != right_.m_verts.size()
			|| left_.m_edges.size() != right_.m_edges.size()) {
			return false;
		}

		//compare vertices
		for (auto i = 0; i != left_.m_verts.size(); ++i) {
			if (*left_.m_verts[i] != *right_.m_verts[i]) {
				return false;
			}
		}

		//compare edges
		for (auto i = 0; i != left_.m_edges.size(); ++i) {
			if (*left_.m_edges[i] != *right_.m_edges[i]) {
				return false;
			}
		}

		return true;
	}

	bool operator<(const dualGraph& left_, const dualGraph& right_) {
		//compare sizes
		if (left_.m_verts.size() < right_.m_verts.size()) {
			return true;
		} else if (left_.m_verts.size() > right_.m_verts.size()) {
			return false;
		}
		if (left_.m_edges.size() < right_.m_edges.size()) {
			return true;
		} else if (left_.m_edges.size() > right_.m_edges.size()) {
			return false;
		}

		//compare vertices
		for (auto i = 0; i != left_.m_verts.size(); ++i) {
			auto& l = *left_.m_verts[i];
			auto& r = *right_.m_verts[i];
			if (l < r) {
				return true;
			} else if (r < l) {
				return false;
			}
		}

		for (auto i = 0; i != left_.m_edges.size(); ++i) {
			auto& l = *left_.m_edges[i];
			auto& r = *right_.m_edges[i];
			if (l < r) {
				return true;
			} else if (r < l) {
				return false;
			}
		}

		return false;
	}

	void dualGraph::canonicalize() {
		//canonicalize exts in vertices
		for (auto& v : m_verts) {
			v->canonicalize();
		}
		//sort the vertices by size and first elements of exts
		std::sort(m_verts.begin(), m_verts.end(),
				  [] (const std::unique_ptr<vert_t>& left_,
					  const std::unique_ptr<vert_t>& right_) -> bool {
					  if (left_->m_exts.size() > right_->m_exts.size()) {
						  return true;
					  } else if (left_->m_exts.size() < right_->m_exts.size()) {
						  return false;
					  }
					  if (left_->m_exts.empty()) {
						  return false;
					  }
					  return left_->m_exts.front() < right_->m_exts.front();
				  });

		//sort the empty vertices
		//recompute vert IDs
		std::size_t emptyStart = 0;
		for (std::size_t i = 0, size = m_verts.size(); i != m_verts.size(); ++i) {
			m_verts[i]->m_ID = i;
			if (emptyStart == 0 && m_verts[i]->empty()) {
				emptyStart = i;
			}
		}

		//canonically rotate edges at each vertex
		for (auto& v : m_verts) {
			std::rotate(v->begin(), cyclicMinElement(*v, adjVertLess(v.get())),
						v->end());
		}

		//finally actually sort the empty vertices
		std::sort(m_verts.begin() + emptyStart, m_verts.end(),
				  [&emptyStart] (const std::unique_ptr<vert_t>& left_,
								 const std::unique_ptr<vert_t>& right_) {
					  //compare sizes
					  if (left_->size() < right_->size()) {
						  return true;
					  } else if (left_->size() > right_->size()) {
						  return false;
					  }
					  //compare elements
					  for (std::size_t i = 0; i != left_->size(); ++i) {
						  const auto l = (*left_)[i]->m_ID;
						  const auto r = (*right_)[i]->m_ID;
						  if (l < r && l < emptyStart) {
							  return true;
						  }
						  if (r < l && r < emptyStart) {
							  return false;
						  }
					  }
					  return false;
				  });
		//nice, all vertices are sorted now, reset indices for empty vertices
		for (auto i = emptyStart, size = m_verts.size(); i != size; ++i) {
			m_verts[i]->m_ID = i;
		}

		//canonicalize edge directions
		for (auto& e : m_edges) {
			if (e->first->m_ID > e->second->m_ID) {
				std::swap(e->first->m_ID, e->second->m_ID);
				e->m_type = conjugate(e->m_type);
			}
		}

		//sort edges globally
		std::sort(m_edges.begin(), m_edges.end(),
				  [] (const std::unique_ptr<edge_t>& left_,
					  const std::unique_ptr<edge_t>& right_) {
					  if (left_->first->m_ID < right_->first->m_ID) {
						  return true;
					  }
					  if (left_->first->m_ID > right_->first->m_ID) {
						  return false;
					  }
					  if (left_->second->m_ID < right_->second->m_ID) {
						  return true;
					  }
					  /*
					  if (left_->second->m_ID > right_->second->m_ID) {
						  return false;
					  }
					  */
					  return false;
				  });

		//recompute edge IDs
		for (std::size_t i = 0, size = m_edges.size(); i != size; ++i) {
			m_edges[i]->m_ID = i;
		}

		//canonically rotate edges at each vertex (comparing their ID's)
		for (auto& v : m_verts) {
			std::rotate(v->begin(),
						cyclicMinElement(*v,
										 [](const edge& left_, const edge& right_) {
											 return left_->m_ID < right_->m_ID;
										 }
							), v->end()); 
		}
	}

	dualGraph::vert dualGraph::addVertex(const vert_t& vert_) {
		m_verts.emplace_back(new vert_t(vert_));
		auto& ret = m_verts.back();
		ret->m_ID = m_verts.size() - 1;
		return ret.get();
	}

	dualGraph::edge dualGraph::addEdge(const edge_t& edge_, const vert& from_,
									   const vert& to_) {
		m_edges.emplace_back(new edge_t(edge_));
		auto ret = m_edges.back().get();
		ret->m_ID = m_edges.size() - 1;

		//connect
		if (from_) {
			ret->first = from_;
			from_->push_back(ret);
		}
		if (to_) {
			ret->second = to_;
			to_->push_back(ret);
		}

		return ret;
	}

	bool dualGraph::adjVertLess::operator()(const edge& left_, const edge& right_) {
		return left_->other(m_v)->m_ID < right_->other(m_v)->m_ID;
	}
}
