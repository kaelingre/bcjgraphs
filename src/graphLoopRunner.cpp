#include "graphLoopRunner.hpp"

namespace ggraph {
	graphLoopRunner::graphLoopRunner(const graph::vert& v_) {
		if (!v_) {
			runner<graph::vert>::m_isRunning = false;
			return;
		}
		m_path.push_back(std::make_pair(v_, 0));
	}

	bool graphLoopRunner::next() {
		if (!runner<graph::vert>::isRunning()) {
			return false;
		}

		auto& v = m_path.back().first;
		auto& ePos = m_path.back().second;

		if (ePos >= v->size()) {
			//we visited all edges starting from this vertex
			m_path.pop_back();
			if (m_path.empty()) {
				runner<graph::vert>::m_isRunning = false;
				return false;
			}
			m_edgePath.pop_back();
			++m_path.back().second;
			//call recursively
			return next();
		}
		//else
		auto newV = (*v)[ePos]->other(v);

		bool visited = false;
		for (auto& pair : m_path) {
			if (newV == pair.first) {
				visited = true;
				break;
			}
		}
		if (visited || newV->m_number > 0) {
			//alread visited or external (dead end!)
			//we found a back edge
			++m_path.back().second;
			//call recursively
			return next();
		}
		//else (not yet visited)
		m_edgePath.push_back((*v)[ePos]);
		m_path.push_back(std::make_pair(newV, 0));

		return true;
	}
}
