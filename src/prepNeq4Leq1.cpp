#include "config.h"

#include <iostream>
#include <fstream>

#include "graphLabelling.hpp"
#include "cut.hpp"
#include "eqns.hpp"

using namespace ggraph;


int main(int argc, char* argv[]) {
	std::vector<flExt> exts;
	auto gluon =std::make_pair(ext_t::GLUON,-1);
	exts.push_back(gluon);
	exts.push_back(gluon);
	exts.push_back(gluon);
	exts.push_back(gluon);

	std::fstream fs("/tmp/test.m", std::fstream::out);

	labelledGraphs gs(exts, 1, {coupling::THREEGLUON});
	auto es = eqns::makeJacobiEqns(gs);
	
	std::vector<const graph*> masters;
	auto it = gs.begin();
	masters.push_back(&it->getGraph());

	auto sol = es.reduce(masters, {});
	replaceRepeated(sol);

	fs << "solFunctional=" << sol << ';' << std::endl
	   << "consistencyEqns=" << es << ';' << std::endl;

	return 0;
}
