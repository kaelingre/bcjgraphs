#ifndef GKA_GRAPHRUNNER_HPP
#define GKA_GRAPHRUNNER_HPP

#include "ggraph_config.h"

#include <set>
#include <stack>

#include "utility/runner.hpp"
#include "graph.hpp"

namespace ggraph {
	/**
	 * depth-first-search runner over vertices
	 */
	class graphRunner : public runner<graph::vert> {
	public:
		//**************************************************
		// constructor
		//**************************************************
		//this runner searches in all directions starting from given vertex
		graphRunner(const graph::vert& v_);
		//this runner does NOT search in the direction of given edge starting
		//from given vertex (edge should be connected to given vertex)
		graphRunner(const graph::vert& v_, const graph::edge& e_);

		//**************************************************
		// methods
		//**************************************************
		graph::vert const& getCurrent() const override {
			return m_current;
		}

		graph::edge nextEdge() const {
			if (m_path.empty()) {
				return graph::edge(nullptr, true);
			}
			//else
			return (*m_path.top().first)[m_path.top().second];
		}
		
		bool next() override;
		
	private:
		//**************************************************
		// members
		//**************************************************
		graph::edge m_edge;

		graph::vert m_current;

		std::set<graph::vert> m_visited;
		//pairs of vertex and at which edge we turned
		std::stack<std::pair<graph::vert, std::size_t> > m_path;
	};
}

#endif /*GKA_GRAPHRUNNER_HPP*/
