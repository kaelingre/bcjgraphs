#include "cut.hpp"

#include "utility/rangeRunner.hpp"
#include "utility/boolRunner.hpp"
#include "utility/permItRunner.hpp"
#include "utility/cyclicIterator.hpp"

namespace ggraph {
	std::ostream& operator<<(std::ostream& ostream_, const cut& c_) {
		//vertices
		ostream_ << "{{";
		bool first = true;
		for (const auto& v : c_.getGraph().getVerts()) {
			if (v->m_number < 0) {
				continue;
			}
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << '{' << v->m_ID + 1 << ',';
			//check if external/blob
			if (v->m_number > 0) {
				ostream_ << v->m_number << ',';
			} else if (v->m_number == 0) {
				ostream_ << "\"blob\"" << ',';
			}
			//connected edges
			ostream_ << '{';
			bool firstE = true;
			for (const auto& e : *v) {
				if (firstE) {
					firstE = false;
				} else {
					ostream_ << ',';
				}
				if (e->second == v.get()) {
					ostream_ << '-';
				}
				ostream_ << (e->getActualEdge()->m_ID + 1);
			}
			ostream_ << "}}";
		}
		ostream_ << "},{";
		//edges
		first = true;
		for (const auto& e : c_.getGraph().getEdges()) {
			if (!e->isActualEdge()) {
				continue;
			}
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			auto verts = e->getActualVerts();
			ostream_ << '{' << e->m_type << ',' << e->m_ID + 1 << ',' << verts.first->m_ID + 1
					 << "->" << verts.second->m_ID + 1 << ',' << '{' << e->m_flavor << ','
					 << c_.getLabelling()[e->m_ID] << "}}";
			
		}
		ostream_ << "}}";

		return ostream_;
	}

	cut::cut(const std::vector<flExt>& exts_, orderedTrees* treesPtr_)
		: m_treesPtr(treesPtr_) {
		auto& g = getGraph();
		auto v = g.addVertex(graph::vert_t(0));
		for (auto& ext_ : exts_) {
			auto vv = g.addVertex(graph::vert_t(1));
			auto lineDir = extToLineDir(ext_.first);
			if (!lineDir.second) {
				g.addEdge(graph::edge_t(ext_.second, lineDir.first), vv, v);
			} else {
				g.addEdge(graph::edge_t(ext_.second, lineDir.first), v, vv);
			}
		}
	}

	std::vector<cut> cut::cutBlobs(const std::vector<std::pair<flLine_t, bool> >&
								   chosenTypes_) const {
		std::vector<cut> ret;
		for (auto& v : getGraph().getVerts()) {
			if (v->m_number == 0) {
				assert(v->size() > 3);
				const auto s = v->size();
				//find all combinations of insertion points
				for (std::size_t i1 = 0; i1 != s - 2; ++i1) {
					for (auto i2 = i1 + 2; i2 != s; ++i2) {
						if (s - i2 + i1 < 2) {
							continue;
						}

						//make a copy
						auto c = *this;
						//new blob
						auto& g = c.getGraph();
						auto oldV = g[v->m_ID];
						auto newV = g.addVertex(graph::vert_t(0));
						//move i1 to i2 (i2=after-end)
						newV->insert(newV->end(),
									 std::make_move_iterator(oldV->begin()+i1),
									 std::make_move_iterator(oldV->begin()+i2));
						oldV->erase(oldV->begin()+i1,oldV->begin()+i2);
						//update the edges
						for (auto& e : *newV) {
							e->replaceVertex(oldV, newV);
						}
						//insert new edges
						auto itOld = oldV->begin()+i1;
						auto itNew = newV->end();
						for (auto& t : chosenTypes_) {
							if (t.second) {
								g.addEdge(graph::edge_t(t.first),oldV,newV,itOld,itNew);
							} else {
								g.addEdge(graph::edge_t(t.first),newV,oldV,itNew,itOld);
							}
							++itOld;
						}

						//insert into ret
						ret.push_back(std::move(c));
					}
                }
			}
		}

		return ret;
	}

	void cut::label() {
		m_labelling.labelCut(getGraph());
	}

	std::vector<labelledGraph> cut::getGraphs() const {
		auto trees = makeTrees();
		assert(!trees.empty());
		std::vector<labelledGraph> ret = std::move(trees.front());
		//connect the trees, set by set
		const auto endTrees = trees.end();
		for (auto it = trees.begin()+1; it != endTrees; ++it) {
			auto retOld = std::move(ret);
			ret.clear();

			for (auto& old : retOld) {
				//we need copies of the trees to move them
				for (auto tree : *it) {
					//copy
					auto oldCopy = old;
					oldCopy.connect(std::move(tree));
					ret.push_back(std::move(oldCopy));
				}
			}
		}

		return ret;
	}

	void cut::printNums(const std::string& head_, const labelledGraphs& gs_,
						std::ostream& ostream_) const {
		ostream_ << '{';
		bool first = true;
		auto gs = getGraphs();
		for (auto& g : gs) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			g.getGraph().setExtUnity();
			auto it = gs_.findMap(g);//this makes a copy, we need g intact!
			assert(it.first != gs_.end());
			//invert the map (we need canon -> pair.first)
			it.second.invert();
			g.printNum(head_+"["+std::to_string(std::distance(gs_.begin(),it.first)+1)+"]",
					   it.second, it.first->getMainLabels(), ostream_);
		}
		ostream_ << '}';
	}

	void cut::printNumsProp(const std::string& head_, const labelledGraphs& gs_,
							std::ostream& ostream_) const {
		ostream_ << '{';
		bool first = true;
		auto gs = getGraphs();
		for (auto& g : gs) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << '{';

			//print graph
			g.getGraph().setExtUnity();
			auto it = gs_.findMap(g);//this makes a copy, we need g intact!
			assert(it.first != gs_.end());
			//invert the map (we need canon -> pair.first)
			it.second.invert();
			g.printNum(head_+"["+std::to_string(std::distance(gs_.begin(),it.first)+1)+"]",
					   it.second, it.first->getMainLabels(), ostream_);
			//print propagators
			ostream_ << ',';
			g.printProps(ostream_);
			ostream_ << '}';
		}
		ostream_ << '}';
	}

	void cut::printTrees(std::ostream& ostream_) const {
		ostream_ << '{';
		bool first = true;
		for (auto& v : getGraph().getVerts()) {
			if (v->size() <= 2) {
				continue;
			}
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << '{';
			auto first2 = true;
			for (auto& e: *v) {
				if (first2) {
					first2 = false;
				} else {
					ostream_ << ',';
				}
				ostream_ << '{';
				if (e.m_dir) {
					ostream_ << m_labelling[e->m_ID];
				} else {
					ostream_ << (-m_labelling[e->m_ID]);
				}
				ostream_ << ',';
				printLineFl(e->m_type, e->m_flavor, e.m_dir, ostream_);
				ostream_ << '}';
				
			}
			ostream_ << '}';
		}
		ostream_ << '}';
	}

	std::vector<cut> cut::decomposeToDirected(const std::set<cut>& cuts_) const {
		//map from scalar edge IDs to they direction of the quark line they
		//should be replaced by
		std::map<std::size_t, bool> mapToDirs;

		//prepare this map
		for (auto& e : getGraph().getEdges()) {
			if (e->m_type == line_t::SCALAR) {
				mapToDirs.insert(std::make_pair(e->m_ID, false));
			}
		}
		std::vector<cut> ret;
		bool next = !mapToDirs.empty();
		while (next) {
			//replace the scalar edges
			//make a copy
			auto newC = *this;
			auto& g = newC.getGraph();
			for (auto& itR  : mapToDirs) {
				graph::edge e = g.m_edges[itR.first].get();
				if (itR.second) {
					e = g.replaceEdge(graph::edge_t(e->m_flavor, line_t::QUARK), e,
									  false);
				} else {
					e = g.replaceEdge(graph::edge_t(e->m_flavor, line_t::QUARK), e,
									  true);
				}
				assert(isDirected(e->m_type));

				//update the labelling
				assert(g.nEdges() == newC.getLabelling().size() + 2);
				newC.getLabelling().push_back(ggraph::label(false));
				newC.getLabelling().push_back(ggraph::label(false));
				//swap sign switch if direction changed
				auto& l = newC.getLabelling()[e->m_ID];
				if (!itR.second) {
					l.negate();
				}
				//set new fake vertex labels
				auto it = newC.m_labelling.end() - 1;
				*it = l;
				*(--it) = l;

				//canonicalize a copy
				auto copy = newC;
				copy.canonicalize();
				
				//try to find the graph:
				if (cuts_.find(copy) != cuts_.end()) {
					ret.push_back(std::move(newC));
				}
			}
			
			//step
			next = false;
			for (auto& d : mapToDirs) {
				if (!d.second) {
					d.second = true;
					next = true;
					break;
				} else {
					d.second = false;
				}
			}
		}

		if (mapToDirs.empty()) {
			ret.push_back(*this);
		}
		return ret;
	}
	
	std::vector<std::size_t> cut::canonicalize() {
		auto ret = getGraph().canonicalizeOrdered();

		return ret;
	}

	std::vector<labelledGraph> cut::makeTrees(const graph::vert& v_) const {
		//collects ext types, numbers and momentum flow directions
		extOrdering exts;
		std::vector<int> numbers;
		std::vector<bool> dirs; //true->outgoing
		for (auto& e: *v_) {
			exts.push_back(std::make_pair(lineDirToExt(e->m_type, e.m_dir),
										  e->m_flavor));
			auto& l = m_labelling[e->m_ID];
			assert(l.size() == 1);
			assert(l.begin()->second == 1 || l.begin()->second == -1);
			numbers.push_back(l.begin()->first);

			//check line direction and "label direction"
			dirs.push_back(e.m_dir == (l.begin()->second == 1));
		}

		//we need to canonically order
		canonRotate(exts, numbers, dirs);

		//get the trees
		auto ret = m_treesPtr->get(exts, numbers);

		//change the sign of incoming externals
		std::vector<std::size_t> changeSign;
		const auto s = dirs.size();
		for (std::size_t i = 0; i != s; ++i) {
			if (!dirs[i]) {
				changeSign.push_back(numbers[i]);
			}
		}
		if (!changeSign.empty()) {
			for (auto& g : ret) {
				g.changeSign(changeSign);
			}
		}

		return ret;
	}

	std::vector<std::vector<labelledGraph> > cut::makeTrees() const {
		std::vector<std::vector<labelledGraph> > ret;
		for (auto& v: m_graph.getVerts()) {
			if (v->m_number == 0) {
				ret.push_back(makeTrees(v.get()));
			}
		}
		return ret;
	}

	bool cut::containsEmpty() const {
		for (auto& v: m_graph.getVerts()) {
			if (v->m_number == 0) {
				if(makeTrees(v.get()).empty()) {
					return true;
				}
			}
		}
		return false;
	}

	spanningCuts::spanningCuts(std::vector<flExt> exts_, const unsigned int& nLoops_,
							   const std::vector<coupling>& couplings_)
		: m_trees(couplings_) {
		//extract all line types with flavors from the couplings
		std::set<flLine_t> lineTypes;
		for (auto& c : couplings_) {
			for (auto& p: c.m_particles) {
				lineTypes.insert(std::make_pair(extToLine(p.first.first),
											   p.first.second));
			}
		}		

		//keep track of partially constructed cuts together with number of loops missing
		std::vector<std::pair<cut, unsigned int> > stack;
		permItRunner pIR(std::make_pair(exts_.begin()+1, exts_.end()));
		do {
			//TODO: more efficient?
			auto pair = std::make_pair(cut(exts_, &m_trees), nLoops_);
			if (std::find(stack.begin(), stack.end(), pair) == stack.end()) {
				stack.push_back(std::move(pair));
			}
		} while (pIR.next());
		
		while (!stack.empty()) {
			auto c = std::move(stack.back());
			assert(c.second > 0);
			stack.pop_back();
			//cut through a blob with a i+1 particle cut (i.e. produce i loops)
			for (unsigned int i = 1; i <= c.second; ++i) {
				//choose line types for each line
				rangeRunner rR(i + 1, (std::size_t) 0, lineTypes.size());

				do {
					//choose direction (if directed)
					//count number of directed types
					std::size_t counter = 0;
					auto& currentTypes = rR.getCurrent();
					std::vector<std::pair<flLine_t, bool> > chosenTypes;
					for (auto& lT : currentTypes) {
						auto it = lineTypes.begin();
						std::advance(it, lT);
						
						if (isDirected(it->first)) {
							++counter;
							chosenTypes.push_back(std::make_pair(*it, true));
						} else {
							chosenTypes.push_back(std::make_pair(*it, false));
						}
					}

					boolRunner bR(counter);
					do {
						//make a copy
						auto chosenTypesTmp = chosenTypes;
						auto currentBoolIt = bR.getCurrent().begin();
						for (auto& t : chosenTypesTmp) {
							if (t.second) {
								if (!*currentBoolIt++) {
									t.second = false;
								}
							}
						}
						
						auto newCs = c.first.cutBlobs(chosenTypesTmp);
						for (auto& cc : newCs) {
							if (i == c.second) {
								//end of recursion
								//canonicalize (special cut version)
								cc.canonicalize();
								//make the labelling
								cc.label();
								//check if there's a blob that doesn't produce
								//any trees
								//the problem is here:
								if (!cc.containsEmpty()) {
									insert(std::move(cc));
								}
							} else {
								stack.push_back(std::make_pair(std::move(cc), c.second-i));
							}
						}
					} while (bR.next());
				} while (rR.next());
			}
		}
   }

	const cut& spanningCuts::operator[](const std::size_t& n_ ) const {
		assert(n_ <= size());
		auto it = begin();
		std::advance(it, n_);
		return *it;
	}

	std::ostream& operator<<(std::ostream& ostream_, const spanningCuts& cuts_) {
		ostream_ << '{';
		bool first = true;
		for (auto& c : cuts_) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << c;
		}
		ostream_ << '}';
		return ostream_;
	}
}
