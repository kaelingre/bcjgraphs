#include "config.h"

#include <iostream>
#include <fstream>

#include "graphLabelling.hpp"
#include "cut.hpp"
#include "eqns.hpp"

using namespace ggraph;


int main(int argc, char* argv[]) {
	std::vector<flExt> exts;
	auto gluon =std::make_pair(ext_t::GLUON,-1);
	exts.push_back(gluon);
	exts.push_back(gluon);
	exts.push_back(gluon);
	exts.push_back(gluon);

	std::fstream fs("/tmp/test.m", std::fstream::out);

	labelledGraphs gs(exts, 2, {coupling::THREEGLUON, coupling::QUARKGLUON});
	labelledGraphs gsNeq4(exts, 2, {coupling::THREEGLUON});

	auto es = eqns::makeJacobiEqns(gs);
	es.add(eqns::makeRevSymEqns(gs));
	es.add(eqns::makeTwoTermEqns(gs));

	std::vector<const graph*> knowns;
	for (auto& g : gsNeq4) {
		es.push_back(eqns::makeNeq4DecompID(g, gsNeq4, gs));
		knowns.push_back(&g.getGraph());
	}
	
	std::vector<const graph*> masters;
	auto it = gs.begin();
	std::advance(it, 151);
	masters.push_back(&it->getGraph());
	std::advance(it, 38);
	masters.push_back(&it->getGraph());
	std::advance(it, 2);
	masters.push_back(&it->getGraph());
	
	auto sol = es.reduce(masters, knowns);
	replaceRepeated(sol);
	//es.replace(sol);

	fs << "solFunctional=" << sol << ';' << std::endl
	   << "consistencyEqns=" << es << ';' << std::endl;

	return 0;
}
