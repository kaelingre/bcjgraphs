#ifndef GKA_CUT_HPP
#define GKA_CUT_HPP

#include "ggraph_config.h"
#include "graphLabelling.hpp"
#include "graphs.hpp"
#include "orderedTrees.hpp"

namespace ggraph {
	class spanningCuts;
	
	class cut : public labelledGraph {
		friend class spanningCuts;
	public:
		//**************************************************
		// constructors
		//**************************************************
		/**
		 * makes a single blob with the given exts
		 */
		cut(const std::vector<flExt>& exts_, orderedTrees* treesPtr_);

		//**************************************************
		// operators
		//**************************************************
		friend bool operator<(const cut& left_, const cut& right_) {
			return ((labelledGraph)left_) < ((labelledGraph)right_);
		}

		friend bool operator==(const cut& left_, const cut& right_) {
			return ((labelledGraph)left_) == ((labelledGraph)right_);
		}
		
		//**************************************************
		// methods
		//**************************************************
		/**
		 * prints in mathematica friendly way
		 * (overwrites!)
		 */
		friend std::ostream& operator<<(std::ostream& ostream_, const cut& c_);

		/**
		 * cuts through all blobs in all possible ways
		 * (leaving always at least two legs on each side)
		 * by inserting the given lines of given type and direction (i.e. the given bool)
		 */
		std::vector<cut> cutBlobs(const std::vector<std::pair<flLine_t, bool> >&
								  chosenTypes_) const;

		/**
		 * cuts through the blob with given ID, such that the edges with given ID's lie on the
		 * left and right on the new "from" and "to" blob
		 */
		void cutBlob(const std::size_t& blobID_, const std::size_t& fromLeft_,
					 const std::size_t& fromRight_, const std::size_t& toLeft_,
					 const std::size_t& toRight_, const flLine_t& lineType_);

		/**
		 * labels the cut (it needs to be canonicalized, i.e. externals first)
		 */
		void label();

		/**
		 * constructs all the graphs contributing to this cut
		 */
		std::vector<labelledGraph> getGraphs() const;

		/**
		 * prints all contributing graphs (that need to be in gs_) in numerator form
		 * as a list
		 */
		void printNums(const std::string& head_, const labelledGraphs& gs_,
					   std::ostream& ostream_ = std::cout) const;
		char* printNumsMMA(const labelledGraphs& gs_, const std::string& head_ = "n") const {
			return printCallable(
				std::bind(&cut::printNums,*this,head_,gs_,std::placeholders::_1));
		}

		/**
		 * prints all contributing graphs (that need to be in gs_) in numerator form
		 * together with their propagator momenta as pairs in a list
		 */
		void printNumsProp(const std::string& head_, const labelledGraphs& gs_,
						   std::ostream& ostream_ = std::cout) const;
		char* printNumsPropMMA(const labelledGraphs& gs_, const std::string& head_ = "n") const {
			return printCallable(
				std::bind(&cut::printNumsProp,*this,head_,gs_,std::placeholders::_1));
		}

		/**
		 * prints the contributing tree specifications (list of list)
		 */
		void printTrees(std::ostream& ostream_ = std::cout) const;
		char* printTreesMMA() const {
			return printCallable(
				std::bind(&cut::printTrees,*this,std::placeholders::_1));
		}

		/**
		 * replaces scalar lines with both directions of
		 * quark lines of the same direction
		 * only keeps cuts that are in cuts_
		 */
		std::vector<cut> decomposeToDirected(const std::set<cut>& cuts_) const;

		orderedTrees* getOrderedTrees() const {
			return m_treesPtr;
		}

		void swapEdges(const std::size_t& vertID_, const std::size_t& edgeID1_, const bool& edgeDir1_,
					   const std::size_t& edgeID2_, const bool& edgeDir2_) {
			return getGraph().swapEdges(vertID_, edgeID1_, edgeDir1_, edgeID2_, edgeDir2_);
		}

	private:
		//**************************************************
		// members
		//**************************************************
		/**
		 * points to a orderedTrees object (of the embedding spanningCut most likely)
		 * that contains all trees of this cut.
		 * (can also be exchanged if the theory changes for example)
		 */
		orderedTrees* m_treesPtr;
		
		//**************************************************
		// methods
		//**************************************************
		/**
		 * canonicalizes the underlying graph for ordered vertices
		 * does NOT canonicalize the labelling (TODO?)
		 */
		std::vector<std::size_t> canonicalize();

		/**
		 * makes the orderedTree amps for a given vertex
		 */
		std::vector<labelledGraph> makeTrees(const graph::vert& v_) const;
		/**
		 * makes all orderedTrees for all vertices
		 */
		std::vector<std::vector<labelledGraph> > makeTrees() const;
		/**
		 * makes all the orderedTree's for each vertex and check if
		 * one of them doesn't produce tree level diagram
		 */
		bool containsEmpty() const;
	};

	//for MMA
	typedef std::vector<cut> cuts;

	class spanningCuts : public std::set<cut> {
	public:
		//**************************************************
		// constructor
		//**************************************************
		spanningCuts(std::vector<flExt> exts_, const unsigned int& nLoops_,
					 const std::vector<coupling>& couplings_);

		//**************************************************
		// operators
		//**************************************************
		const cut& operator[](const std::size_t& n_) const;

		//**************************************************
		// methods
		//**************************************************
		friend std::ostream& operator<<(std::ostream& ostream_, const spanningCuts& cuts_);

		const orderedTrees* getOrderedTrees() const {
			return &m_trees;
		}

		orderedTrees* getOrderedTrees() {
			return &m_trees;
		}

	private:
		//**************************************************
		// members
		//**************************************************
		orderedTrees m_trees;
	};
}

#endif /*GKA_CUT_HPP*/
