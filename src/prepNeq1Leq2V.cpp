#include "ggraph_config.h"

#include <iostream>
#include <fstream>

#include "graphLabelling.hpp"
#include "cut.hpp"
#include "eqns.hpp"

using namespace ggraph;


int main(int argc, char* argv[]) {
	std::vector<flExt> exts;
	auto gluon =std::make_pair(ext_t::GLUON,-1);
	exts.push_back(gluon);
	exts.push_back(gluon);
	exts.push_back(gluon);
	exts.push_back(gluon);

	std::fstream fs("/tmp/test.m", std::fstream::out);

	labelledGraphs gsNeq2(exts, 2, {coupling::THREEGLUON, coupling::QUARKGLUON});
	labelledGraphs gs(exts, 2, {coupling::THREEGLUON, coupling::QUARKGLUON,
								coupling::QUARKGLUON1, coupling::QUARKGLUON2,
								coupling::THREEQUARK, coupling::THREEAQUARK});

	auto es = eqns::makeFlavorSymEqns(gs, {0,1,2},"n",true);
	es.add(eqns::makeTwoTermEqns(gs));
	//es.add(eqns::makeTwoTermFlEqns(gs, {0,1,2}));
	//es.add(eqns::makeJacobiEqns(gs));
	
	std::vector<const graph*> knowns;
	for (auto& g : gsNeq2) {
		es.push_back(eqns::makeNeq2DecompID(g, gsNeq2, gs));
		knowns.push_back(&g.getGraph());
	}
	std::vector<const graph*> masters;
	masters.push_back(&gs[751].getGraph());
	masters.push_back(&gs[753].getGraph());
	masters.push_back(&gs[694].getGraph());
	masters.push_back(&gs[472].getGraph());
	masters.push_back(&gs[700].getGraph());
	masters.push_back(&gs[677].getGraph());
	
	auto sol = es.reduce(masters, knowns, {0,1,2});
	replaceRepeated(sol);

	fs << "solFunctional=" << sol << ';' << std::endl
	   << "consistencyEqns=" << es << ';' << std::endl;

	return 0;
}
