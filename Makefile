all: build/dummy
	meson compile -C build

build/dummy:
	meson setup build
	touch build/dummy

install:
	cd build && sudo meson install

clean:
	rm -rf build
