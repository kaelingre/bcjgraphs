(* ::Package:: *)

Needs["CCompilerDriver`"];


(*compilation*)
libInstallDir = NotebookDirectory[]<>"LibraryResources/"<>$SystemID;
If[!DirectoryQ[libInstallDir],CreateDirectory[libInstallDir,CreateIntermediateDirectories->True]];
fileNames={
	"couplingsRunner.cpp",
	"cut.cpp",
	"eqns.cpp",
	"graph.cpp",
	"graphLabelling.cpp",
	"graphLoopRunner.cpp",
	"graphMap.cpp",
	"graphRunner.cpp",
	"graphs.cpp",
	"libraryLink.cpp",
	"orderedTrees.cpp",
	"tuplesRunner.cpp"};
files=File[(NotebookDirectory[]<>"../src/"<>#)]&/@fileNames;
includes = {NotebookDirectory[]<>"../src/cereal/include/"};
tmpFile=NotebookDirectory[]<>"tmp.txt";
flags="-std=gnu++17 -fopenmp ";
If[Run["pkgconf --cflags --libs graph-canon perm-group > "<>tmpFile]=!=0,
	If[Run["pkg-config --cflags --libs graph-canon perm-group > "<>tmpFile]=!=0,
		Print["Error with pkgconf"];Abort[]
	]
];
flags = flags<>Import[tmpFile];
DeleteFile[tmpFile];
libpath=CreateLibrary[
	files,
	"libggraph",
	"Language"->"C++",
	"TargetDirectory"->libInstallDir,
	"CompileOptions"->flags,
	"IncludeDirectories"->includes,
	"Defines"->{"NDEBUG"}(*,
	"ShellCommandFunction"\[Rule]Print,
	"Debug"\[Rule]True*)
]


(*installation*)
installDir=$UserBaseDirectory<>"/Applications/gGraph/";
If[!DirectoryQ[installDir],CreateDirectory[installDir,CreateIntermediateDirectories->True]];
If[!DirectoryQ[installDir<>"Kernel/"],CreateDirectory[installDir<>"Kernel/",CreateIntermediateDirectories->True]];
If[!DirectoryQ[installDir<>"LibraryResources/"<>$SystemID],CreateDirectory[installDir<>"LibraryResources/"<>$SystemID,CreateIntermediateDirectories->True]];
files = {"gGraph.m","cppLibraryLink.m","source.m","Kernel/init.m","LibraryResources/"<>$SystemID<>"/"<>StringSplit[libpath,"/"][[-1]]};
CopyFile[NotebookDirectory[]<>#,installDir<>#,OverwriteTarget->True]&/@files;


(* ::Section:: *)
(*Useful stuff*)


(*find available compilers*)
CCompilers[]
