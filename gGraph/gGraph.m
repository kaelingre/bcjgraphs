(* ::Package:: *)

(* Wolfram Language Package *)

(* Created by the Wolfram Workbench Jan 22, 2020 *)

BeginPackage["gGraph`",{"gGraph`cppLibraryLink`"}]
Unprotect@@Names["gGraph`*"];
ClearAll["gGraphs`*"];
(* Exported symbols added here with SymbolName::usage *) 

gGraph::libNotFound = 
"C++ graph library could not be loaded.
 Please install the gGraph package in the directory: 
 " <> $UserBaseDirectory <> "/Applications/";

gGraph::srcNotFound = 
"Source file for libggraph could not be loaded.
 Please install the gGraph packages in the directory: 
 " <> $UserBaseDirectory <> "/Applications/";

(*Load shared C++ library*)
If[FindLibrary["libggraph"]===$Failed,Message[gGraph::libNotFound];Abort[]; LibraryLoad["libggraph"];];

drawGraph::usage = "drawGraph[graph_] draws a graph object produceded by the C++ library.
Argument can be a 'printed' graph or a c++ graph.";
drawGraph::author = "Gregor Kaelin";

drawGraphEFT::usage = "drawGraphEFT[graph_] draws a graph object produceded by the C++ library in EFT style.
Argument can be a 'printed' graph or a c++ graph.";
drawGraphEFT::author = "Gregor Kaelin";

drawGraphs::usage = "drawGraphs[graphs_] draws a set of graph objects produced by the C++ library.
Argument can be a 'printed' list of graphs or a C++ set of  graphs.";
drawGraphs::author = "Gregor Kaelin";

drawGraphsEFT::usage = "drawGraphsEFT[graphs_] draws a set of graph objects produced by the C++ library in EFT style.
Argument can be a 'printed' list of graphs or a C++ set of  graphs.";
drawGraphsEFT::author = "Gregor Kaelin";

numsToGraphs::usage = "numsToGraphs[expr_, l_List] prints numerators as graphs. The list is consists of pairs of numerator heads 'n' and graph sets 'gs' such that a numerator of the form n[i][args__] in the expression will be replaced by gs[[i]] with its arguments permutated accordingly.";
numsToGraphs::author = "Gregor Kaelin";  

flExt::usage = "C++ head for flavortexternal pair";
flExt::author = "Gregor Kaelin";

constructFlExt::usage = "constructFlExt[extType_String, flavor_Integer] construct a C++ flExt object";
constructFlExt::author = "Gregor Kaelin";

coupling::usage = "C++ head for coupling";
coupling::author = "Gregor Kaelin";

constructCoupling::usage = "constructCoupling[flExts_List] constructs a C++ coupling objects from the given vector of flExts";
constructCoupling::author = "Gregor Kaelin";

THREEGLUON::usage = "C++ object representing a three-gluon coupling";
THREEGLUON::author = "Gregor Kaelin";

QUARKGLUON::usage = "C++ object representing a quark-antiquark-gluon (flavor 0) coupling";
QUARKGLUON::author = "Gregor Kaelin";

QUARKGLUON1::usage = "C++ object representing a quark-antiquark-gluon (flavor 1) coupling";
QUARKGLUON1::author = "Gregor Kaelin";

QUARKGLUON2::usage = "C++ object representing a quark-antiquark-gluon (flavor 2) coupling";
QUARKGLUON2::author = "Gregor Kaelin";

SCALARGLUON::usage = "C++ object representing a two-scalar-gluons (flavor 0) coupling";
SCALARGLUON::author = "Gregor Kaelin";

SCALARGLUON1::usage = "C++ object representing a two-scalar-gluons (flavor 1) coupling";
SCALARGLUON1::author = "Gregor Kaelin";

SCALARGLUON2::usage = "C++ object representing a two-scalar-gluons (flavor 2) coupling";
SCALARGLUON2::author = "Gregor Kaelin";

THREEQUARK::usage = "C++ object representing a three-quark (flavors 0,1,2) coupling";
THREEQUARK::author = "Gregor Kaelin";

THREEAQUARK::usage = "C++ object representing a three-antiquark (flavors 0,1,2) coupling";
THREEAQUARK::author = "Gregor Kaelin";

THREESCALAR::usage = "C++ object representing a three-scalar (flavors 0,1,2) coupling";
THREESCALAR::author = "Gregor Kaelin";

graph::usage = "C++ head for a graph";
graph::author = "Gregor Kaelin";

labelledGraph::usage = "C++ head for a labelledGraph";
labelledGraph::author = "Gregor Kaelin";

graphs::usage = "C++ head for a set of graphs";
graphs::author = "Gregor Kaelin";

constructGraphs::usage = "constructGraphs[flExts_List, nLoops_Integer, couplings_List] constructs a C++ graphs objects.";
constructGraphs::author = "Gregor Kaelin";

labelledGraphs::usage = "C++ head for a set of labelled graphs";
labelledGraphs::author = "Gregor Kaelin";

constructLabelledGraphs::usage = "constructLabelledGraphs[flExts_List, nLoops_Integer, couplings_List, useExtNumbers_:False (optional), exactNV_Integer:0 (optional)] or
constructLabelledGraphs[graphs_List] constructs a C++ labelledGraphs objects.
  flExts: list of external particles (see constructFlExt)
  nLoops: number of loops
  couplings: list of couplings (see constructCoupling)
  useExtNumbers: specifices if labels 1,...,n should be used or every external is equivalent (i.e. all have label 1)
  exactNV: specifies the exact number of vertices that the graph should have (0=not specified)";
constructLabelledGraphs::author = "Gregor Kaelin";

print::usage = "print[cppObj_] retrieves a C++ object and transforms it into a mathematica readable form."
print::author = "Gregor Kaelin";

stripLineTypes::usage = "stripLineTypes[g_graph] transforms every edge of a c++ graph into a gluon line, i.e. removes all types and flavors.";
stripLineTypes::author = "Gregor Kaelin";

stripMomenta::usage = "stripMomenta[g_labelledGraph] removes all momentum information from a c++ labelled graph, i.e. transform a labelledGraph into a graph.";
stripMomenta::author = "Gregor Kaelin";

printSyms::usage = "printSyms[labelledGraph(s)_, head_String:\"n\"] prints numerator symmetries of given labelled graph(s)";
printSyms::author = "Gregor Kaelin";

printColorFactor::usage "printColorFactor[g_labelledGraph] prints the color factor of the given graph.";
printColorFactor::author = "Gregor Kaelin";

printSymmetryGroup::usage = "printSymmetryGroup[g_(labelled)Graph] prints the symmetry group generators of the given graph.";
printSymmetryGroup::author = "Gregor Kaelin";

symmetryFactor::usage = "symmetryFactor[g_(labelled)Graph] prints the symmetry factor for the given (labelled) graph.";
symmetryFactor::author = "Gregor Kaelin";

containsSimpleTadpole::usage = "containsSimpleTadpole[g_(labelled)Graph] checks if the graph contains an edge that connects to the same vertex on both sides.";
containsSimpleTadpole::author = "Gregor Kaelin";

containsBubble::usage = "containsBubble[g_(labelled)Graph] checks if the graph contains two lines that connect the same two vertices.";
containsBubble::author = "Gregor Kaelin";

containsNull::usage = "containsNull[g_(labelled)Graph] checks if the graph contains an edge that splits the graph into two parts where one side has zero or one external leg.";
containsNull::author = "Gregor Kaelin";

countMultiEdgeTadpoleSyms::usage = "countMultiEdgeTadpoleSyms[g_(labelled)Graph] gives the symmetry factor due to multi-edges and simple tadpoles.";
countMultiEdgeTadpoleSyms::author = "Gregor Kaelin";

countQuarkLoops::usage = "countQuarkLoops[g_(labelled)Graph] counts the number of closed quark loops.";
countQuarkLoops::author = "Gregor Kaelin";

fullSymFactor::usage = "fullSymFactor[g_(labelled)Graph] computes the internal + external symmetry factor of the given (labelled) graph.";
fullSymFactor::author = "Gregor Kaelin";

printJacobies::usage = "printJacobies[gs_labelledGraphs, head_String:\"n\"] prints the numerator jacobies of given labelled graphs";
printJacobies::author = "Gregor Kaelin";

printTwoTerm::usage = "printTwoTerm[gs_labelledGraphs, head_String:\"n\"] prints the numerator two-term identities of given labelled graphs.
This includes only the two-term ids involing two quark pairs of the same flavor.";
printTwoTerm::author = "Gregor Kaelin";

printTwoTermFl::usage = "printTwoTermFl[gs_labelledGraphs, head_String:\"n\", flavors_List:{0,1,2}] prints the numerator  three-flavor two-term identities of given labelled Graphs.
This includes only the two-term ids (which actually consist of three graphs) involing two quark pairs of different flavor.";
printTwoTermFl::author = "Gregor Kaelin";

printScalarTwoTerm::usage = "printScalarTwoTerm[gs_labelledGraphs, head_String:\"n\"] prints the numerator scalar two-term identities of given labelled graphs.
This includes only the two-term ids (which actually consist of three graphs) involing two scalar pairs of the same flavor.";
printSCalarTwoTerm::author = "Gregor Kaelin";

printScalarTwoTermFl::usage = "printScalarTwoTermFl[gs_labelledGraphs, head_String:\"n\", flavors_List:{0,1,2}] prints the numerator  three-flavor scalar two-term identities of given labelled Graphs.
This includes only the two-term ids (which actually consist of three graphs) involing two scalar pairs of different flavor.";
printScalarTwoTermFl::author = "Gregor Kaelin";

printFlavorSyms::usage = "printFlavorSyms[gs_labelledGraphs, head_String:\"n\", flavors_List:{0,1,2}, onlyTruePerms_:True] prints the numerator flavor permutation symmetries involving the given flavors.";
printFlavorSyms::author = "Gregor Kaelin";

printNeq4DecompID::usage = "printNeq4DecompID[gs_labelledGraphs, g_labelledGraph, head_String:\"n\"] prints the sum of diagrams that build up the given N=4 graph.
All of these N=2 graphs need(!) to be part of the given set of graphs gs.";
printNeq4DecompID::author = "Gregor Kaelin";

printNeq2DecompID::usage = "printNeq2DecompID[gs_labelledGraphs, g_labelledGraph, head_String:\"n\"] prints the sum of diagrams that build up the given N=2 graph.
All of these N=1 graphs need(!) to be part of the given set of graphs gs.";
printNeq2DecompID::author = "Gregor Kaelin";

printNeq4DecompIDS::usage = "printNeq4DecompIDS[gs_labelledGraphs, g_labelledGraph, head_String:\"n\"] prints the sum of diagrams that build up the given N=4 graph with undirected (scalar) graphs.
All of these N=2 graphs need(!) to be part of the given set of graphs gs.";
printNeq4DecompIDS::author = "Gregor Kaelin";

printNeq2DecompIDS::usage = "printNeq2DecompIDS[gs_labelledGraphs, g_labelledGraph, head_String:\"n\"] prints the sum of diagrams that build up the given N=2 graph with undirected (scalar) graphs.
All of these N=1 graphs need(!) to be part of the given set of graphs gs.";
printNeq2DecompIDS::author = "Gregor Kaelin";

printReversalSyms::usage = "printReversalSyms[gs_labelledGraphs, head_String:\"n\"] prints the matter reversal symmetries for the given set of graphs."
printReversalSyms::author = "Gregor Kaelin";

printRevAllQuarks::usage = "printRevAllQuarks[gs_labelledGraphs, head_String:\"n\"] prints for each graph the version with all quark lines reversed."
printRevAllQuarks::author = "Gregor Kaelin";

printUndirToDir::usage = "printUndirToDir[gsDir_labelledGraphs, gsUndir_labelledGraphs,couplings_,head_String:\"n\"] decomposes the given set of undirected graphs into directed graphs (i.e. scalar lines go to both directions of quark lines).
The given couplings need to match the couplings given in the construction of the directed graphs. All graphs obtained by this decomposition need to be inside the set of directed graphs."
printUndirToDir::author = "Gregor Kaelin";

printColorFactors::usage = "printColorFactors[gs_labelledGraphs] prints the color factors of all graphs in the set.";
printColorFactors::author = "Gregor Kaelin";

spanningCuts::usage = "C++ head for spanning cuts";
spanningCuts::author = "Gregor Kaelin";

cut::usage = "C++ head for a cut";
cut::author = "Gregor Kaelin";

cuts::usage = "C++ head for a vector of cuts";
cuts::author = "Gregor Kaelin";

constructSpanningCuts::usage = "constructSpanningCuts[flExts_List, nLoops_Integer, couplings_List] constructs a C++ spanningcuts objects.";
constructSpanningCuts::author = "Gregor Kaelin";

printNums::usage = "printNums[c_cut, gs_labelledGraphs, head_String:\"n\"] prints the numerators for the graphs of the set gs appearing in the given cut.";
printNums::author = "Gregor Kaelin";

printNumsProp::usage = "printNumsProp[c_cut, gs_labelledGraphs, head_String:\"n\"] prints the numerators together with their propagator momenta for the graphs of the set gs appearing in the given cut.";
printNumsProp::author = "Gregor Kaelin";

get::usage = "get[cppObj_, n_Integer] extracts the n-1 object in a C++ set object (i.e. labelledGraphs, spanningCuts,...
It is though more convenient to use the Part/[[]] function. Only for positive numbers or ranges. Crashes if out of range!
The length can be obtained via the 'Length' function.";
get::author = "Gregor Kaelin"; 

printTrees::usage = "printTrees[c_cut] prints the tree specifications as a list of lists.";
printTrees::author = "Gregor Kaelin"; 

printProps::usage = "printProps[c_labelledGraph/cut] prints the list of propagator momenta (unsquared!).";
printProps::author = "Gregor Kaelin";

decomposeToDirected::usage = "decomposeToDirected[c_cut, cs_spanningCuts] decomposes all scalar lines in the given cut into quark lines of both directions (of the same flavor).
It returns a list of cuts (i.e. a cuts c++ object) with all the cuts obtained that way thet can be found in cs.";
decomposeToDirected::author = "Gregor Kaelin";

swapEdges::usage = "swapEdges[c_cut, vertID_Integer, edgeID1_Integer, edgeDir1_?BooleanQ, edgeID2_Integer, edgeDir2_?BooleanQ] swapes the position of the two edges at given vertex. IDs need to be subtracted by one to fit the c++ counting starting at zero. The directions need to match (i.e. True=Outgoing, False=Incoming).";
swapEdges::author = "Gregor Kaelin";

makeEFTPMGraphs::usage = "makeEFTPMGraphs[nLoops_Integer] construct the labelled EFT PM graphs at given loop order.";
makeEFTPMGraphs::author = "Gregor Kaelin";

makePMEFTGraphs::usage = "makePMEFTGraphs[order_Integer, nBH_Integer:2, higherWLCoupling_:False] construct the labelled PM PFT graphs with directed graviton lines at given PM order.
Can be used for any number of BHs (nBH) and higher WL couplings can be turned on and off.";
makePMEFTGraphs::author = "Gregor Kaelin";

makePMEFTGraphsInIn::usage = "makePMEFTGraphsInIn[order_Integer, nBH_Integer:2, higherWLCoupling_:False] construct the labelled PM PFT graphs with directed graviton lines at given PM order.
Can be used for any number of BHs (nBH) and higher WL couplings can be turned on and off.";
makePMEFTGraphsInIn::author = "Gregor Kaelin";

makePMEFTRadGraphs::usage = "makePMEFTRadGraphs[order_Integer, nBH_Integer:2, higherWLCoupling_:False] construct the labelled PM PFT graphs with directed graviton lines at given PM order with one external graviton.
Can be used for any number of BHs (nBH) and higher WL couplings can be turned on and off. The external graviton has number nBH+1";
makePMEFTGraphs::author = "Gregor Kaelin";

makeGRWLImpulseGraphs::usage = "makeGRWLImpulseGraphs[orderG_Integer] construct the labelled GR-WL theory graphs for the impluse at given order in G.";
makeGRWLImpulseGraphs::author = "Gregor Kaelin";

makeGRWLSGraphs::usage = "makeGRWLSGraphs[orderG_Integer] construct the labelled GR-WL theory graphs for the classical action at given order in G.";
makeGRWLSGraphs::author = "Gregor Kaelin";

makeCutNumerics::usage = "makeCutNumerics[c_cut,funName_Symbol] defines funName as a function that generates random numerics for the given cut.
This function takes as argument another function which generates a random scalar number when called with [].";
makeCutNumerics::author = "Gregor Kaelin";

getCutPoles::usage = "getCutPoles[c_cut/List] retrieves a list of all (unique) physical poles inside the tree level graphs that make up the given cut";
getCutPoles::author = "Gregor Kaelin";

cutMomConstraints::usage = "cutMomConstraints[c_cut/List] solves the momentum conservation and onshellness conditions for a given cut. Returns a list of replacement rules.";
cutMomConstraints::author = "Gregor Kaelin";

makeCutAnsatz::usage = "makeCutAnsatz[c_cut, gs_labelledGraphs, head_String:\"n\"] writes a cut in terms of the contributing numerators in gs.";
makeCutAnsatz::author = "Gregor Kaelin";

diffExts::usage = "diffExts[gs_(labelled)Graphs] transforms the set of graphs with indistingushable external particles to a set of graphs with distinguishable external particles by giving them explicit labels."
diffExts::author = "Gregor Kaelin";

Begin["`Private`"]
(* Implementation of the package *)

$defaultLineThickness = 0.006;
$defaultWaveLength = 0.08;
$defaultWaveSize = 0.04;

arrowEdgeFunction::usage = "Internal helper for graphEdge";
arrowEdgeFunction::author = "Gregor Kaelin";
arrowEdgeFunction[pts_, edge_] := {Arrowheads[{{Automatic, 0.75}}], Thickness[$defaultLineThickness], Arrow[pts]}

normalizePtDist::usage = "Internal helper for wavedEdgeFunction";
normalizePtDist::author = "Gregor Kaelin";
normalizePtDist[pts_List] := With[{dists=FoldList[Plus,0,Norm/@(pts[[2;;]]-pts[[1;;-2]])]},
	{Function[{x},
		With[{pos=First@FirstPosition[dists,_?(#>x&)]},
			pts[[pos-1]]+(pts[[pos]]-pts[[pos-1]])*(x-dists[[pos-1]])/(dists[[pos]]-dists[[pos-1]])
		]
	],dists[[-1]]}
]

wavePoints::usage = "Internal helper for wavedEdgeFunction";
wavePoints::author = "Gregor Kaelin";
wavePoints[pts_List] := Riffle[pts,Table[pts[[ii]]+1/2 (pts[[ii+1]]-pts[[ii]])+(-1)^ii $defaultWaveSize*RotationMatrix[Pi/2] . Normalize[pts[[ii+1]]-pts[[ii]]],{ii,1,Length[pts]-1}]]

wavedEdgeFunction::usage = "Internal helper for graphEdge";
wavedEdgeFunction::author = "Gregor Kaelin";
wavedEdgeFunction[pts_, edge_] := With[{ptsEquiFun=normalizePtDist[pts]},
	{Arrowheads[{{Automatic, 0.75}}], Thickness[$defaultLineThickness], Arrow[Append[wavePoints[ptsEquiFun[[1]]/@Range[0,ptsEquiFun[[2]],$defaultWaveLength]],pts[[-1]]]]}
]

edgeQuarkColor::usage = "Internal color choser for different flavors of quarks";
edgeQuarkColor::author = "Gregor Kaelin"; 
edgeQuarkColor[i_Integer] := ColorData[1][i+1];
edgeQuarkColor[_] = ColorData[1][1];
edgeScalarColor::usage = "Internal color choser for different flavors of scalars";
edgeScalarColor::author = "Gregor Kaelin"; 
edgeScalarColor[i_Integer] := Darker[ColorData[3][i+2]];
edgeScalarColor[_] = Darker[ColorData[3][2]];


graphEdge::usage = "Internal helper for drawGraph";
graphEdge::author = "Gregor Kaelin";
graphEdge[{"gluon", ID_, from_ -> to_, tooltip_List : {}}] := 
 Tooltip[Labeled[Style[Property[DirectedEdge[from,to],EdgeShapeFunction->wavedEdgeFunction],darkGreen],Style[ID,Gray,Background->White]],tooltip]
graphEdge[{"quark", ID_, from_ -> to_, tooltip_List : {}}] :=
 Tooltip[Labeled[Style[Property[DirectedEdge[from,to],EdgeShapeFunction->arrowEdgeFunction],edgeQuarkColor[tooltip[[1]]]],Style[ID,Gray,Background->White]],tooltip]
graphEdge[{"scalar", ID_, from_ -> to_, tooltip_List : {}}] := 
Tooltip[Labeled[Style[Property[DirectedEdge[from,to],EdgeShapeFunction->arrowEdgeFunction],edgeScalarColor[tooltip[[1]]],Dashing[List[Medium,Small]]],Style[ID,Gray,Background->White]],tooltip]
graphEdge[{"boundary", ID_, from_ -> to_, tooltip_List : {}}] := 
 Tooltip[Labeled[Style[Property[DirectedEdge[from,to],EdgeShapeFunction->arrowEdgeFunction],Blue],Style[ID,Gray,Background->White]],tooltip]
graphEdge[{"none", ID_, from_ -> to_}] := Labeled[Style[DirectedEdge[from,to],Black],Style[ID,Gray,Background->White]]


darkGreen = RGBColor[0, 0.4, 0];
graphVertex::usage = "Internal helper for drawGraph";
graphVertex::author = "Gregor Kaelin";
graphVertex[{ID_Integer}] := Labeled[ID, Style[ID, Gray, Background -> White]]
graphVertex[{ID_Integer, "blob"}] := Property[Labeled[ID, Style[ID, darkGreen, Background -> White]], {VertexSize -> 0.25, VertexStyle -> RGBColor[0, 1, 0, .2]}]
graphVertex[{ID_Integer, number_Integer}] := Labeled[ID, Style[ID, Background -> White]]
graphVertex[{ID_Integer, "blob", {tooltip__}}] := Tooltip[Property[Labeled[ID, Style[ID, darkGreen, Background -> White]], {VertexSize -> 0.25, VertexStyle -> RGBColor[0, 1, 0, .2]}], {tooltip}]
graphVertex[{ID_Integer, number_Integer, {tooltip__}}] := Tooltip[Labeled[ID, Style[Row[{ID, Style["(" <> ToString[number] <> ")", Black]}], Gray, Background -> White]], {tooltip}]
graphVertex[{ID_Integer, {tooltip__}}] := Tooltip[Labeled[ID, Style[ID, Gray, Background -> White]], {tooltip}]


Options[drawGraph] := {"size" -> 250, "dynamic" -> True}
graphComb := System`Graph;(*private variable to have a workaround for combinatorica shadowing*)
drawGraph[graphData_List, OptionsPattern[]] := DynamicModule[{vc, data},
	data = {graphVertex /@ (graphData[[1]]),graphEdge /@ (graphData[[2]])};
	vc = (AbsoluteOptions[graphComb[Sequence @@ (data /. {DirectedEdge -> UndirectedEdge})], VertexCoordinates] /. {HoldPattern[_ -> l_] :> l})[[1]];
	If[
		OptionValue["dynamic"], 
		LocatorPane[Dynamic@vc, Dynamic[Show[graphComb[Sequence @@ data, VertexCoordinates -> vc], ImageSize -> OptionValue["size"]]], Appearance -> {Style["\[Bullet]", Orange, 30]}], 
		Show[graphComb[Sequence @@ data, VertexCoordinates -> vc],ImageSize -> OptionValue["size"]]
	]
]
drawGraph[g_graph, opts:OptionsPattern[]] := drawGraph[print[g], opts]
drawGraph[g_labelledGraph, opts:OptionsPattern[]] := drawGraph[print[g], opts]
drawGraph[g_cut, opts:OptionsPattern[]] := drawGraph[print[g], opts]


Options[drawGraphEFT] := {"size" -> Small, "dynamic" -> True}
drawGraphEFT[graphData_List, OptionsPattern[]] := DynamicModule[{vc, data}, 
	data = {graphVertex /@ (graphData[[1]]),graphEdge /@ (graphData[[2]])};
	vc = (AbsoluteOptions[
		graphComb[Sequence @@ (data /. {DirectedEdge -> UndirectedEdge}),
		VertexCoordinates->((#[[1]]->(If[Length[#]===2,Automatic,{Automatic,-5(Mod[#[[2]],2]-1)}])&/@graphData[[1]])),ImageSize -> OptionValue["size"]], VertexCoordinates] /. {HoldPattern[_ -> l_] :> l})[[1]];
	If[
		OptionValue["dynamic"], 
		LocatorPane[Dynamic@vc, Dynamic[Show[graphComb[Sequence @@ data, VertexCoordinates -> vc], ImageSize -> OptionValue["size"]]], Appearance -> {Style["\[Bullet]", Orange, 30]}], 
		Show[graphComb[Sequence @@ data, VertexCoordinates -> vc],ImageSize -> OptionValue["size"]]
  ]
 ]
drawGraphEFT[g_graph, opts:OptionsPattern[]] := drawGraphEFT[print[g], opts]
drawGraphEFT[g_labelledGraph, opts:OptionsPattern[]] := drawGraphEFT[print[g], opts]


drawGraphs[gs_List, opts:OptionsPattern[]]:=drawGraph[#,opts]&/@gs
drawGraphs[gs_graphs, opts:OptionsPattern[]]:=drawGraph[#,opts]&/@print[gs]
drawGraphs[gs_labelledGraphs, opts:OptionsPattern[]]:=drawGraph[#,opts]&/@print[gs]
drawGraphs[gs_spanningCuts, opts:OptionsPattern[]]:=drawGraph[#,opts]&/@print[gs]


drawGraphsEFT[gs_List, opts:OptionsPattern[]]:=drawGraphEFT[#,opts]&/@gs
drawGraphsEFT[gs_graphs, opts:OptionsPattern[]]:=drawGraphEFT[#,opts]&/@print[gs]
drawGraphsEFT[gs_labelledGraphs, opts:OptionsPattern[]]:=drawGraphEFT[#,opts]&/@print[gs]
drawGraphsEFT[gs_spanningCuts, opts:OptionsPattern[]]:=drawGraphEFT[#,opts]&/@print[gs]


graphs/:Length[gs_graphs]:=size[gs]
labelledGraphs/:Length[gs_labelledGraphs]:=size[gs]
spanningCuts/:Length[gs_spanningCuts]:=size[gs]
cuts/:Length[cs_cuts]:=size[cs]


labelledGraphs/:Part[a_labelledGraphs, i_Integer]/;i>0:=get[a,i-1]
labelledGraphs/:Part[a_labelledGraphs, Span[i_Integer,j_Integer]]/;i>0&&j>0:=get[a,#]&/@Range[i-1,j-1]
graphs/:Part[a_graphs, i_Integer]/;i>0:=get[a,i-1]
graphs/:Part[a_graphs, Span[i_Integer,j_Integer]]/;i>0&&j>0:=get[a,#]&/@Range[i-1,j-1]
spanningCuts/:Part[a_spanningCuts, i_Integer]/;i>0:=get[a,i-1]
spanningCuts/:Part[a_spanningCuts, Span[i_Integer,j_Integer]]/;i>0&&j>0:=get[a,#]&/@Range[i-1,j-1]
cuts/:Part[a_cuts, i_Integer]/;i>0:=get[a,i-1]
cuts/:Part[a_cuts, Span[i_Integer,j_Integer]]/;i>0&&j>0:=get[a,#]&/@Range[i-1,j-1]


computeSymGrpSize::usage = "(Internal) computes the size of the symmetry group as returned by printSymmetryGroup.";
computeSymGrpSize::author = "Gregor Kaelin";
computeSymGrpSize[symGrp_List]:=Block[{els={symGrp[[1]]}},
	Do[
		els=FixedPoint[Union[#,Permute[#,el]&/@#]&,els];
	,{el,(FindPermutation@*Ordering)/@symGrp[[2;;]]}];
	Length[els]
]

fullSymFactor[g_labelledGraph]:=computeSymGrpSize[printSymmetryGroup[g]]*countMultiEdgeTadpoleSyms[g]
fullSymFactor[g_graph]:=computeSymGrpSize[printSymmetryGroup[g]]*countMultiEdgeTadpoleSyms[g]


Options[numToGraphs] := {"size" -> 250}
numToGraph::usage = "Internal helper for numsToGraphs.";
numToGraph::author = "Gregor Kaelin";
numToGraph[_[i_][args__], gs_, opts : OptionsPattern[]] := Block[{argToPattern},
  	argToPattern[{a_, _}] := a;
  	argToPattern[a_] := a;
  	drawGraph[gs[[i]], opts] /. 
   Thread[Array[Symbol["k"], Length[{args}]] -> (argToPattern /@ {args})]
  ]
numsToGraphsRec::usage = "Internal recursive helper of numsToGraphs.";
numsToGraphsRec::author = "Gregor Kaelin";
numsToGraphsRec[eqn_, {n_, gs_}, {}, opts : OptionsPattern[]] := eqn /. {n[i_][args__] :> numToGraph[n[i][args], gs,opts]}
numsToGraphsRec[eqn_, {n_, gs_}, l_List, opts : OptionsPattern[]] := numsToGraphs[eqn /. {n[i_][args__] :> numToGraph[n[i][args], gs,opts]},l,opts]
Options[numsToGraphs] := {"size" -> 250}
numsToGraphs[eqn_, {}, OptionsPattern[]] := eqn
numsToGraphs[eqn_, l_List, opts : OptionsPattern[]] := numsToGraphsRec[eqn, l[[1]], Rest[l], opts] 


getCutBlobs::usage = "getCutBlobs[c_cut/List] extracts momentum numbers of the tree-level blobs of the given cut (either the c++ object or the mathematica representation as a graph/list)";
getCutBlobs::author = "Gregor Kaelin";
getCutBlobs[c_cut] := getCutBlobs[print[c]]
getCutBlobs[c_List] := Block[	
    {getBlobMoms, getBlobLineMom},
    
    (* helper functions *)
	getBlobMoms[blobID_Integer, lineIDs_List] := getBlobLineMom[blobID, #]&/@lineIDs;
	getBlobLineMom[blobID_Integer, lineID_Integer] := Block[
		{line = FirstCase[c[[2]], {_, Abs[lineID], __}]},
		With[{mom = If[MatchQ[line[[4, 2]], -_[_]],-line[[4, 2, 2, 1]],line[[4, 2, 1]]]},
			If[lineID < 0, -mom, mom]
		]
	];
    getBlobMoms[#[[1]], #[[3]]]&/@Cases[c[[1]], {_, "blob", _}]
]

tpauli::author = "Gustav Mogull"
tpauli = {IdentityMatrix[2],-{{0,1},{1,0}},-{{0,-I},{I,0}},-{{1,0},{0,-1}}};

makeCutTwistors::usage = "makeCutTwistors[c_cut/List, funName_Symbol, z_Symbol] defines the given symbol as a function that produces a set of random twistors for the given cut when called with the argument a function that produces a single random scalar number (integer/rational/real/complex). It additionall returns the blobs of the given cut.";
makeCutTwistors::author = "Gregor Kaelin";
makeCutTwistors[c_cut, funName_Symbol, z_Symbol, removeExts_:{}] := makeCutTwistors[print[c], funName, z, removeExts]
makeCutTwistors[c_List, funName_Symbol, z_Symbol, removeExts_:{}] := Block[
	{exts, blobs, getTwistorLines, getTwistorLineExt, getTwistorLineInt, lines, solved, sol = {}, change = True},
	getTwistorLines[es_List, bs_List] := Block[
		{ret, temp, eExts = es/.{i_?Negative :> -i}, bBlobs = bs/.{i_?Negative :> -i}},
		ret = getTwistorLineExt[#, eExts, bBlobs]&/@eExts;
		temp = Gather[Cases[ret, _Integer, Infinity]];
		While[
			Not[And@@(Length[#]==2&/@temp)],
			temp = FirstCase[temp, {_}][[1]];
			AppendTo[ret, getTwistorLineInt[temp, bBlobs, SelectFirst[ret, MemberQ[#,temp]&]]];
			
			temp = Gather[Cases[ret, _Integer, Infinity]];
		];
		ret
	];
	getTwistorLineExt[e_, es_List, bs_List] := Block[
		{current = e, lastBlobPos = -1, pos, ret = {e}},
		While[
			lastBlobPos === - 1 || !MemberQ[es, current],
			If[
				lastBlobPos === - 1,
				(*then: we're still at first external*)
				pos = FirstPosition[bs, current],
				(*else*)
				pos = SelectFirst[Position[bs, current],(#[[1]] != lastBlobPos)&];
			];
			
			lastBlobPos = pos[[1]];
			If[++pos[[2]] > Length[bs[[pos[[1]]]]], pos[[2]] = 1];
			current = bs[[Sequence@@pos]];
			AppendTo[ret, current];
		];
		Sort[ret]
	];
	getTwistorLineInt[i_, bs_List, l_List] := Block[
		{current = i, lastBlobPos = -1, pos, ret = {i}},
		pos = Position[bs, current];
		If[--pos[[1, 2]] == 0, pos[[1, 2]] = Length[bs[[pos[[1, 1]]]]]];
		If[--pos[[2, 2]] == 0, pos[[2, 2]] = Length[bs[[pos[[2, 1]]]]]];
		If[MemberQ[l, bs[[Sequence@@pos[[1]]]]], pos = pos[[2]], pos = pos[[1]]];
		lastBlobPos = pos[[1]];
		current = bs[[Sequence@@pos]];
		While[
			current != i,
			AppendTo[ret, current];
			pos = SelectFirst[Position[bs, current],(#[[1]] != lastBlobPos)&];
			lastBlobPos = pos[[1]];
			If[--pos[[2]] == 0, pos[[2]] = Length[bs[[pos[[1]]]]]];
			current = bs[[Sequence@@pos]];
		];
		Sort[ret]
	];
	
	(* get blobs with their momenta *)
	blobs = getCutBlobs[c];
	
	(* get external momenta *)
	exts = First/@Cases[Tally[Abs/@Join@@blobs],{_,1}];
	
	(* get all the lines in twistor space *)
	lines = getTwistorLines[exts, blobs];
	
	(* remove some of the exts by hand for the case of a tree-level configuration*)
	exts=Complement[exts,removeExts];

	(* select lines with non-trivail information *)
	lines = Select[Select[lines, (Length[#] > 2)&], Function[{x}, Not[And@@((MemberQ[solved,#])&/@x)]]];
	
	(* find unconstraint twistors  and add to ext *)
	exts = DeleteDuplicates[Join[exts, Complement[Union[Abs/@(Join@@blobs)], Union[Abs/@(Join@@lines)]]]]/.{i_?Negative:>-i};
	
	(* solve for each twistor *)
	solved = exts;
	(* solve for points that appear in only a single line *)
	While[change,
		change = False;
		With[{ls = {Select[#, MemberQ[exts,#]&], Select[#, !MemberQ[solved,#]&]}&/@lines},
			With[{possSolve = First/@Select[Tally[Join@@(#[[2]]&/@ls)], #[[2]]==1&]},
				With[{ls2 = Select[ls, Length[First[#]] > 1&]},
					sol = Join[sol, 
						DeleteCases[
							Function[{x},With[{found = SelectFirst[ls2, MemberQ[#[[2]], x]&]},
								If[Head[found]=!=Missing, change = True;AppendTo[solved, x];{x,found[[1,1;;2]]}, Null]
							]]/@possSolve
						,Null]
					];
				];
			];
		];
		lines = Select[lines, Length[Complement[#, solved]] > 0&];
	];

	(* solve for points that appear in several lines but are not yet constrained by more than one of them*)
	change = True;
	While[change,
		change = False;
		With[{unsolved = Complement[Flatten[lines], solved]},
			(*try to find a point that is only constrained by one line*)
			With[{ls = Cases[{Select[#, MemberQ[solved,#]&], Select[#, !MemberQ[solved,#]&]}&/@lines, {_?(Length[#]>=2&),_?(Length[#]>=1&)}]},
				With[{found = Select[Function[{x},Select[ls, MemberQ[#[[2]], x]&]]/@unsolved,Length[#]==1&]},
					If[
						Length[found] > 0, 
						AppendTo[sol, {found[[1,1,2,1]],found[[1,1,1,1;;2]]}];
						AppendTo[solved, found[[1,1,2,1]]];
						change = True;
					];
				];
			];
		];
	];
	
	(*the rest of the lines has to be on the intersection of two or more lines*)
	With[{unsolved = Complement[Flatten[lines], solved]},
		With[{ls = {Select[#, MemberQ[exts,#]&], Select[#, !MemberQ[solved,#]&]}&/@lines},
			Function[{x}, AppendTo[sol, {x, {#,#}}&@First[Intersection@@(First/@Select[ls, MemberQ[#[[2]], x]&])]];]/@unsolved;
		];
	];
	
	Block[{randomFunction},
		With[
			{fExts = exts, fSol = sol},
			Evaluate[funName][randomFunction_] := Fold[Join[#1, {z[#2[[1]]] -> (z[#2[[2, 1]]] + randomFunction[](z[#2[[2, 1]]] - z[#2[[2, 2]]]))/.#1}]&, (z[#] -> Table[randomFunction[], {4}])&/@fExts, fSol]
		];
	];
	blobs
]


eps2::author = "Gustav Mogull";
eps2 = {{0,1},{-1,0}}; (* Taken with raised indices; lowered has opposite signs *)


spAAN::author = "Gustav Mogull";
spAAN[sp1_List,sp2_List] := -sp1 . eps2 . sp2


twistortoSptN::author = "Gustav Mogull";
twistortoSptN[Z1_List,Z2_List,Z3_List] := Module[{sp1=Z1[[;;2]],sp2=Z2[[;;2]],sp3=Z3[[;;2]],mu1=Z1[[3;;]],mu2=Z2[[3;;]],mu3=Z3[[3;;]]},
	(spAAN[sp1,sp2]mu3+spAAN[sp2,sp3]mu1+spAAN[sp3,sp1]mu2)/(spAAN[sp1,sp2]spAAN[sp2,sp3])
]
twistortoSptN[__] := $Failed


makeCutNumerics[c_cut,funName_Symbol]:=makeCutNumerics[print[c],funName]
makeCutNumerics[c_List,funName_Symbol]:=Module[{twistorFun,randomFunction,z},
	With[{blobs=makeCutTwistors[c,twistorFun,z]},
		Evaluate[funName][Pattern[Evaluate[randomFunction],_]]:=Block[{temp,toSpA,toSpB,point=twistorFun[randomFunction]},
			toSpA[list_List]:=(#1[[1;;2]]&)/@list;
			toSpB[list_List]:=(twistortoSptN[list[[Mod[#1-1,Length[list],1]]],list[[#1]],list[[Mod[#1+1,Length[list],1]]]]&)/@Range[Length[list]];
			temp=DeleteDuplicates[Flatten[With[{randZ=(z/@#1/. {z[i_?Negative]:>z[-i]}/. point&)/@blobs},
				(Transpose[{Symbol["k"]/@blobs[[#1]]/. {Symbol["k"][i_?Negative]:>-Symbol["k"][-i]},toSpA[randZ[[#1]]],toSpB[randZ[[#1]]]}]&)
				/@Range[Length[blobs]]]/. {{-Symbol["k"][i_],spa_,spb_}:>{Symbol["k"][i],spa,-spb}},1],#1[[1]]==#2[[1]]&];
			Flatten[({Symbol["spA"][#1[[1]]]->#1[[2]],Symbol["spB"][#1[[1]]]->#1[[3]],#1[[1]]->1/2 Table[#1[[3]] . tpauli[[i]] . #1[[2]],{i,4}]}&)/@temp]
		]
	]
]


getCutLineMom::usage = "getCutLineMom[idx_, c_cut/List] gets the momentum of the line with given index in given cut (either as c++ object or mathematica list)";
getCutLineMom::author = "Gregor Kaelin";
getCutLineMom[idx_,c_cut] := getCutLineMom[idx, print[c]]
getCutLineMom[idx_,c_List] := If[idx < 0, -FirstCase[c[[2]], {_, -idx, ___}][[4,2]], FirstCase[c[[2]], {_, idx, ___}][[4,2]]]


getCutPoles[c_cut]:= getCutPoles[print[c]]
getCutPoles[cut_List]:= 
With[{blobMoms=(getCutLineMom[#,cut]&/@#[[3]])&/@Cases[cut[[1]],{_,"blob",_}]},
	DeleteDuplicates[Flatten[
		Function[{x}, (*special case of 4pt*)(If[Length[#]==4,#[[1;;2]],#])&@(Symbol["dot"][x[[ #[[1]] ]],x[[ #[[2]] ]]]&/@(Partition[Range[Length[x]+1],2,1]/.{Length[x]+1->1}))]/@blobMoms
	]/.{Minus[a_]:>a}]
]


cutMomConstraints[c_cut]:= cutMomConstraints[print[c]]
cutMomConstraints[c_List]:= With[{blobMoms=(getCutLineMom[#,c]&/@#[[3]])&/@Cases[c[[1]],{_,"blob",_}]},
	With[{removedMoms=Solve[#==0&/@(Plus@@@blobMoms)][[1]]},
		Join[Solve[#==0&/@(DeleteCases[Symbol["dot"]/@Variables[blobMoms]/.removedMoms/.{Symbol["dot"][a_,a_]:>0},0])][[1]],removedMoms,{Symbol["dot"][Symbol["k"][a_],Symbol["k"][a_]]:>0}]
	]
]

(* load libgraph source *)
With[
	{file = FileNameJoin[{DirectoryName[$InputFileName], "source.m"}]},
	If[
		FileExistsQ[file],
		(*then*)
		Get[file];,
		(*else*)
		Message[gGraph::srcNotFound];
		Abort[];
	]
]


makeCutAnsatz[c_cut, gs_labelledGraphs, head_String : "n"] := With[
	{grs = printNumsProp[c,gs, head], props = printProps[c]},
	Block[{temp, f},
		f[diagProps_] := Fold[Delete[#1, DeleteCases[{FirstPosition[#1, #2, Missing, 1], FirstPosition[#1//Expand, -#2//Expand, Missing, 1]}, Missing][[1]]]&, diagProps, props];
		temp = {I^Length[props]#[[1]], f[#[[2]]]}&/@grs;(*factors of I come from cut propagator*)
		Plus@@(#[[1]]/Times@@(Symbol["dot"]/@#[[2]])&/@temp)
	]
]

buildCppMathInterface[enums, classes, functions, "libggraph"];

End[]
Protect@@Names["gGraph`*"]
EndPackage[]




